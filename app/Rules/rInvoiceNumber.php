<?php

namespace app\Rules;

use app\Models\mInvoice;
use app\Models\mUser;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use app\Models\mUserRole;
use Session;

class rInvoiceNumber implements Rule
{

    private $error_type;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $invoice = $value;
        $check = mInvoice::where('invoice_number_label', 'like', '%'.$invoice.'%')->count();

        if($check == 0) {
            $this->error_type = NULL;
            return FALSE;
        } else if($check > 1) {
            $this->error_type = 'multiple';
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if($this->error_type == NULL) {
            return 'Nomer Invoice tidak ditemukan';
        } elseif($this->error_type == 'multiple') {
            return 'Nomer Invoice kurang spesifik, Silahkan mengetik kembali';
        } else {
            return 'Nomer Invoice Kosong';
        }
    }
}
