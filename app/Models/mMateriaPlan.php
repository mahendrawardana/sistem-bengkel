<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mMateriaPlan extends Model
{
//    use SoftDeletes;

    protected $table = 'material_plan';
    protected $primaryKey = 'id_material_plan';
    protected $fillable = [
        'id_material_plan',
        'id_order',
        'id_user',
        'mapl_item_name',
        'mapl_item_code',
        'mapl_plan',
        'mapl_actual',
        'mapl_item_price',
        'mapl_price_total',
        'mapl_type',
    ];

    function order() {
        return $this->belongsTo(mOrder::class, 'id_order', 'id_order');
    }

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
