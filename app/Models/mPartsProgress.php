<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPartsProgress extends Model
{
    use SoftDeletes;

    protected $table = 'parts_progress';
    protected $primaryKey = 'id_parts_progress';
    protected $fillable = [
        'id_parts_progress',
        'id_parts_order',
        'id_user',
        'papr_done_estimate',
        'papr_eta',
        'papr_supplier',
        'papr_status',
    ];

    function parts_order() {
        return $this->belongsTo(mPartsOrder::class, 'id_parts_order', 'id_parts_order');
    }

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
