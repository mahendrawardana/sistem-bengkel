<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPartsOrder extends Model
{
    use SoftDeletes;

    protected $table = 'parts_order';
    protected $primaryKey = 'id_parts_order';
    protected $fillable = [
        'id_parts_order',
        'id_order',
        'id_user',
        'id_estimate_parts',
        'paor_price_buy',
        'paor_margin',
    ];

    function order() {
        return $this->belongsTo(mOrder::class, 'id_order', 'id_order');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
