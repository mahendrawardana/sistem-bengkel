<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mQcProcess extends Model
{
    use SoftDeletes;

    protected $table = 'qc_process';
    protected $primaryKey = 'id_qc_process';
    protected $fillable = [
        'id_qc_process',
        'id_order',
        'id_mechanic',
        'id_user',
        'qc_order',
        'qc_notes',
        'qc_date',
        'qc_time'
    ];

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    function order() {
        return $this->belongsTo(mOrder::class, 'id_order', 'id_order');
    }

    function mechanic() {
        return $this->belongsTo(mMechanic::class, 'id_mechanic', 'id_mechanic');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
