<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mOrder extends Model
{
    use SoftDeletes;

    protected $table = 'order';
    protected $primaryKey = 'id_order';
    protected $fillable = [
        'id_order',
        'id_estimate',
        'id_car',
        'id_user',
        'order_number',
        'order_number_label',
        'order_print_date',
        'order_date',
    ];

    function estimate() {
        return $this->belongsTo(mEstimate::class, 'id_estimate', 'id_estimate');
    }

    function car() {
        return $this->belongsTo(mCar::class, 'id_car', 'id_car');
    }

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
