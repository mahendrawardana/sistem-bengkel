<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mCar extends Model
{
    use SoftDeletes;

    protected $table = 'car';
    protected $primaryKey = 'id_car';
    protected $fillable = [
        'id_car',
        'id_car_owner',
        'id_customer',
        'id_brand',
        'id_insurance',
        'id_user',
        'car_brand',
        'car_police_number',
        'car_type',
        'car_color_name',
        'car_color_code',
        'car_production_year',
        'car_machine_number',
        'car_chassis_number',
        'car_stnk_picture',
    ];



    function car() {
        return $this->belongsTo(mCar::class, 'id_car', 'id_car');
    }

    function car_owner() {
        return $this->belongsTo(mCarOwner::class, 'id_car_owner', 'id_car_owner');
    }

    function customer() {
        return $this->belongsTo(mCustomer::class, 'id_customer', 'id_customer');
    }

    function insurance() {
        return $this->belongsTo(mInsurance::class, 'id_insurance', 'id_insurance');
    }

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
