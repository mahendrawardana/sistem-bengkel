<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBilling extends Model
{
    use SoftDeletes;

    protected $table = 'billing';
    protected $primaryKey = 'id_billing';
    protected $fillable = [
        'id_billing',
        'id_order',
        'id_user',
        'billing_invoice',
        'billing_send',
        'billing_pay',
    ];

    function order() {
        return $this->belongsTo(mOrder::class, 'id_order', 'id_order');
    }

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
