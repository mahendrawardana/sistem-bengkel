<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mEstimateServices extends Model
{
    use SoftDeletes;

    protected $table = 'estimate_services';
    protected $primaryKey = 'id_estimate_services';
    protected $fillable = [
        'id_estimate_services',
        'id_estimate',
        'eser_name',
        'eser_status',
        'eser_price_item',
        'eser_discount',
        'eser_price_end',
        'eser_valid',
    ];

    function estimate() {
        return $this->belongsTo(mEstimate::class, 'id_estimate', 'id_estimate');
    }

    function panel() {
        return $this->belongsTo(mPanel::class, 'id_panel', 'id_panel');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
