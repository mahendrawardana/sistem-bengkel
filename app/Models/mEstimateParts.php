<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mEstimateParts extends Model
{
    use SoftDeletes;

    protected $table = 'estimate_parts';
    protected $primaryKey = 'id_estimate_parts';
    protected $fillable = [
        'id_estimate_parts',
        'id_estimate',
        'espa_name',
        'espa_number',
        'espa_qty',
        'espa_price_item',
        'espa_discount',
        'espa_price_end',
        'espa_valid',
    ];

    function estimate() {
        return $this->belongsTo(mEstimate::class, 'id_estimate', 'id_estimate');
    }

    function parts_order() {
        return $this->belongsTo(mPartsOrder::class, 'id_estimate_parts', 'id_estimate_parts');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
