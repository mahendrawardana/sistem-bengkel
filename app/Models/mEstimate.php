<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mEstimate extends Model
{
    use SoftDeletes;

    protected $table = 'estimate';
    protected $primaryKey = 'id_estimate';
    protected $fillable = [
        'id_estimate',
        'id_claim',
        'id_car',
        'id_user',
        'esti_number',
        'esti_number_label',
        'esti_status',
        'esti_price_total_services',
        'esti_price_total_parts',
        'esti_price_total_sublet',
        'esti_price_total',
        'esti_finish',
        'esti_print_date',
        'esti_day'
    ];

    function estimate_parts() {
        return $this->hasMany(mEstimateParts::class, 'id_estimate', 'id_estimate');
    }

    function estimate_services() {
        return $this->hasMany(mEstimateServices::class, 'id_estimate', 'id_estimate');
    }

    function estimate_sublet() {
        return $this->hasMany(mEstimateSublet::class, 'id_estimate', 'id_estimate');
    }

    function claim() {
        return $this->belongsTo(mClaim::class, 'id_claim', 'id_claim');
    }

    function car() {
        return $this->belongsTo(mCar::class, 'id_car', 'id_car');
    }

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
