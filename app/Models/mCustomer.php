<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mCustomer extends Model
{
    use SoftDeletes;

    protected $table = 'customer';
    protected $primaryKey = 'id_customer';
    protected $fillable = [
        'id_customer',
        'id_user',
        'cust_name',
        'cust_address',
        'cust_city',
        'cust_postal_code',
        'cust_phone',
        'cust_birthday',
        'cust_job',
        'cust_hobby',
        'cust_ktp_number',
        'cust_ktp_picture',
    ];

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
