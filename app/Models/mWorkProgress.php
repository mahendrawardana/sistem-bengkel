<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mWorkProgress extends Model
{
    use SoftDeletes;

    protected $table = 'work_progress';
    protected $primaryKey = 'id_work_progress';
    protected $fillable = [
        'id_work_progress',
        'id_order',
        'id_user',
        'id_mechanic',
        'work_progress_date',
        'work_progress_time',
        'work_progress_type'
    ];

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    function mechanic() {
        return $this->belongsTo(mMechanic::class, 'id_mechanic', 'id_mechanic');
    }

    function order() {
        return $this->belongsTo(mOrder::class, 'id_order', 'id_order');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
