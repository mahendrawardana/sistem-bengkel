<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mWorkProgressFile extends Model
{
    use SoftDeletes;

    protected $table = 'work_progress_file';
    protected $primaryKey = 'id_work_progress_file';
    protected $fillable = [
        'id_work_progress_file',
        'id_order',
        'id_user',
        'wopf_parts_lama',
        'wopf_parts_baru',
        'wopf_epoxy',
        'wopf_hasil_pekerjaan',
    ];

    function order() {
        return $this->belongsTo(mOrder::class, 'id_order', 'id_order');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
