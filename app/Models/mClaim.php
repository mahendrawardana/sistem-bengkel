<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mClaim extends Model
{
    use SoftDeletes;

    protected $table = 'claim';
    protected $primaryKey = 'id_claim';
    protected $fillable = [
        'id_claim',
        'id_car',
        'id_user',
        'claim_number',
        'claim_number_label',
        'claim_status',
        'claim_progress',
    ];

    function claim_file() {
        return $this->hasMany(mClaimFile::class, 'id_claim', 'id_claim');
    }

    function car() {
        return $this->belongsTo(mCar::class, 'id_car', 'id_car');
    }

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
