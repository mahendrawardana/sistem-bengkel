<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mInvoice extends Model
{
    use SoftDeletes;

    protected $table = 'invoice';
    protected $primaryKey = 'id_invoice';
    protected $fillable = [
        'id_order',
        'invoice_number',
        'invoice_number_label',
        'parts_number_label',
        'sublet_number_label',
        'invoice_print_date',
        'send_date',
        'payment_date',
    ];

    public function order()
    {
        return $this->belongsTo(mOrder::class, 'id_order');
    }
}
