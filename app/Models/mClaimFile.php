<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mClaimFile extends Model
{
    use SoftDeletes;

    protected $table = 'claim_file';
    protected $primaryKey = 'id_claim_file';
    protected $fillable = [
        'id_claim_file',
        'id_claim',
        'cfile_filename',
        'cfile_type'
    ];

    function claim() {
        return $this->belongsTo(mClaim::class, 'id_claim', 'id_claim');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
