<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mInsurance extends Model
{
    use SoftDeletes;

    protected $table = 'insurance';
    protected $primaryKey = 'id_insurance';
    protected $fillable = [
        'id_insurance',
        'id_user',
        'insu_name',
        'insu_address',
        'insu_city',
        'insu_postal_code',
        'insu_policy_number',
        'insu_type',
        'insu_expire_date',
        'insu_or_value',
        'insu_ktp_picture',
    ];

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
