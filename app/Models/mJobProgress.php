<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mJobProgress extends Model
{
    use SoftDeletes;

    protected $table = 'job_progress';
    protected $primaryKey = 'id_job_progress';
    protected $fillable = [
        'id_job_progress',
        'id_work_plan',
        'id_order',
        'id_mechanic',
        'id_user',
        'job_actual_time',
        'jobp_filename_1',
        'jobp_filename_2',
    ];

    function work_plan() {
        return $this->belongsTo(mWorkPlan::class, 'id_work_plan', 'id_work_plan');
    }

    function order() {
        return $this->belongsTo(mOrder::class, 'id_order', 'id_order');
    }

    function mechanic() {
        return $this->belongsTo(mMechanic::class, 'id_mechanic', 'id_mechanic');
    }

    function user() {
        return $this->belongsTo(mUser::class, 'id_user', 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
