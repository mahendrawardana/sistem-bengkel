<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mEstimateParts;
use app\Models\mEstimateServices;
use app\Models\mEstimateSublet;
use app\Models\mInvoice;
use app\Models\mOrder;
use app\Rules\rInvoiceNumber;
use app\Rules\rPartsNumber;
use app\Rules\rSubletNumber;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class Reprint extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['reprint'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Cetak Ulang',
                'route' => route('reprintPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        return view('bengkel/reprint/reprint_list', $data);
    }

    function invoice(Request $request)
    {
        $request->validate([
            'invoice' => [
                'required',
                new rInvoiceNumber()
            ]
        ]);

        $invoice = $request->input('invoice');
        $id_order = mInvoice::where('invoice_number_label', 'like', '%' . $invoice . '%')->value('id_order');

        return [
            'redirect' => route('orderValidationPrint', ['id_order' => Main::encrypt($id_order)])
        ];
    }

    function parts(Request $request)
    {
        $request->validate([
            'parts' => [
                'required',
                new rPartsNumber()
            ]
        ]);

        $parts = $request->input('parts');
        $id_order = mInvoice::where('parts_number_label', 'like', '%' . $parts . '%')->value('id_order');

        return [
            'redirect' => route('reprintPartsPdf', ['id_order' => Main::encrypt($id_order)])
        ];
    }

    function sublet(Request $request)
    {
        $request->validate([
            'sublet' => [
                'required',
                new rSubletNumber()
            ]
        ]);

        $sublet = $request->input('sublet');
        $id_order = mInvoice::where('sublet_number_label', 'like', '%' . $sublet . '%')->value('id_order');

        return [
            'redirect' => route('reprintSubletPdf', ['id_order' => Main::encrypt($id_order)])
        ];
    }

    function parts_pdf($id_order)
    {
        $id_order = Main::decrypt($id_order);

        $detail = mOrder
            ::leftJoin('invoice', 'invoice.id_order', '=', 'order.id_order')
            ->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $parts = mEstimateParts
            ::where([
                'id_estimate' => $detail->id_estimate,
                'espa_valid' => 'yes'
            ])
            ->get();

        $data = [
            'detail' => $detail,
            'parts' => $parts,
            'parts_total' => 0,
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/reprint/reprint_parts_print', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Slip Parts');
    }

    function sublet_pdf($id_order)
    {
        $id_order = Main::decrypt($id_order);

        $detail = mOrder
            ::leftJoin('invoice', 'invoice.id_order', '=', 'order.id_order')
            ->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $sublet = mEstimateSublet
            ::where([
                'id_estimate' => $detail->id_estimate,
                'esub_valid' => 'yes'
            ])
            ->get();

        $data = [
            'detail' => $detail,
            'sublet' => $sublet,
            'sublet_total' => 0,
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/reprint/reprint_sublet_print', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Slip Parts');
    }
}
