<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mCar;
use app\Models\mClaim;
use app\Models\mClaimFile;
use FontLib\EOT\File;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class ClaimDocument extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['claim_document'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Dokumen Klaim',
                'route' => route('claimDocumentPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "claim_number_label"],
            ["data" => "cust_name"],
            ["data" => "car_police_number"],
            ["data" => "car_production_year"],
            ["data" => "insu_name"],
            ["data" => "insu_policy_number"],
            ["data" => "insu_expire_date"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_claim' => $request->id_claim,
            'cust_name' => $request->cust_name,
            'car_police_number' => $request->car_police_number
        ));
        return view('bengkel/claim_document/claim_document_list', $data);
    }

    function datatable(Request $request)
    {
        $id_claim = $request->id_claim;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'claim.id_claim'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_claim) {
            $id_claim_arr = explode(',', $id_claim);
            $total_data = mClaim::whereIn('id_claim', $id_claim_arr)->count();
        } else {
            $total_data = mClaim
                ::count();
        }

        $data = mClaim
            ::leftJoin('car', 'car.id_car', '=', 'claim.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance');
        if ($id_claim) {
            $id_claim_arr = explode(',', $id_claim);
            $data = $data->whereIn('id_claim', $id_claim_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_car = Main::encrypt($row->id_car);
            $id_claim = Main::encrypt($row->id_claim);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->options = '
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . route('claimDocumentEditPage', ['id_claim' => $id_claim]) . '">
                            <i class="fa fa-bell"></i>
                            Edit 
                        </a>
                        <a class="dropdown-item" href="' . route('claimDocumentDetailPage', ['id_claim' => $id_claim]) . '">
                            <i class="fa fa-bell"></i>
                            Detail
                        </a>
                        <a class="dropdown-item" href="' . route('estimationFormCreate', ['id_car' => $id_car, 'id_claim' => $id_claim]) . '">
                            <i class="fa fa-bell"></i>
                            Buat Estimasi
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-red btn-hapus" 
                           href="#"
                           data-route="' . route('claimDocumentDelete', ['id_claim' => $id_claim]) . '">
                            <i class="fa fa-trash text-red"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {
        $request->validate([
            'cust_name' => 'required_without:car_police_number',
            'car_police_number' => 'required_without:cust_name',
        ]);

        $cust_name = $request->cust_name;
        $car_police_number = $request->car_police_number;
        $check = mClaim
            ::leftJoin('car', 'car.id_car', '=', 'claim.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('cust_name', 'like', '%' . $cust_name . '%');

        if ($request->car_police_number) {
            $check = $check->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($check->count() == 1) {
            $id_claim = $check->value('id_claim');
            $id_claim = Main::encrypt($id_claim);
            $data = [
                'redirect' => route('claimDocumentDetailPage', ['id_claim' => $id_claim])
            ];
        } elseif ($check->count() > 1) {
            $id_claim = [];
            foreach ($check->get() as $row) {
                $id_claim[] = $row->id_claim;
            }

            $id_claim = implode(",", $id_claim);

            $data = [
                'redirect' => route('claimDocumentPage', ['id_claim' => $id_claim, 'cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        } else {
            $data = [
                'redirect' => route('claimDocumentPage', ['cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function create($id_car)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Klaim Baru',
                'route' => route('claimDocumentCreate', ['id_car' => $id_car])
            )
        ));

        $id_car = Main::decrypt($id_car);
        $data = Main::data($breadcrumb, $this->menuActive);
        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();

        $data = array_merge($data, [
            'row' => $row,
            'id_car' => $id_car
        ]);

        return view('bengkel/claim_document/claim_document_create', $data);
    }

    function insert(Request $request)
    {

        DB::beginTransaction();
        try {
            $id_car = $request->input('id_car');
            $id_customer = mCar::where('id_car', $id_car)->value('id_customer');
            $id_user = Session::get('user')['id'];
            $claim_number_data = Main::claim_number();
            $claim_number = $claim_number_data['claim_number'];
            $claim_number_label = $claim_number_data['claim_number_label'];
            $date_modified = date('Y-m-d H:i:S');
            $year = date('Y');
            $month = date('m');
            $time = date('His');
            $prefix = $id_customer . '_' . $year . '-' . $month . '-' . $time;

            $data_claim = [
                'id_car' => $id_car,
                'id_user' => $id_user,
                'claim_number' => $claim_number,
                'claim_number_label' => $claim_number_label,
                'claim_status' => 'add',
                'claim_progress' => '',
            ];

            $id_claim = mClaim::create($data_claim)->id_claim;

            if ($request->hasFile('cfile_filename_policy_insurance')) {
                $data_create = [];
                foreach ($request->file('cfile_filename_policy_insurance') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_policy_insurance', $name);

                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'policy_insurance',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_damage')) {
                $data_create = [];
                foreach ($request->file('cfile_filename_damage') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_damage', $name);

                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'damage',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_drive_license')) {
                $data_create = [];
                foreach ($request->file('cfile_filename_drive_license') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_drive_license', $name);

                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'drive_license',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_police_report')) {
                $data_create = [];
                foreach ($request->file('cfile_filename_police_report') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_police_report', $name);
                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'police_report',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_events_chronology')) {
                $data_create = [];
                foreach ($request->file('cfile_filename_events_chronology') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_events_chronology', $name);
                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'events_chronology',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_loss_report')) {
                $data_create = [];
                foreach ($request->file('cfile_filename_loss_report') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_loss_report', $name);
                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'loss_report',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    function edit($id_claim)
    {
        $id_claim = Main::decrypt($id_claim);
        $claim = mClaim
            ::leftJoin('car', 'car.id_car', '=', 'claim.id_car')
            ->where('id_claim', $id_claim)
            ->first();

        $breadcrumb = array_merge($this->breadcrumb(), [
            [
                'label' => 'Klaim Edit',
                'route' => route('claimDocumentEditPage', ['id_claim' => $id_claim]),
            ],
            [
                'label' => $claim->claim_number_label,
                'route' => ''
            ]
        ]);
        $data = Main::data($breadcrumb, $this->menuActive);
        $id_car = $claim->id_car;

        $claim_file_policy_insurance = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'policy_insurance'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_damage = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'damage'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_drive_license = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'drive_license'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_police_report = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'police_report'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_events_chronology = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'events_chronology'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_loss_report = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'loss_report'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();

        $data = array_merge($data, [
            'row' => $row,
            'id_car' => $id_car,
            'claim' => $claim,
            'claim_file_policy_insurance' => $claim_file_policy_insurance,
            'claim_file_damage' => $claim_file_damage,
            'claim_file_drive_license' => $claim_file_drive_license,
            'claim_file_police_report' => $claim_file_police_report,
            'claim_file_events_chronology' => $claim_file_events_chronology,
            'claim_file_loss_report' => $claim_file_loss_report
        ]);

        return view('bengkel/claim_document/claim_document_edit', $data);
    }


    function update(Request $request, $id_claim)
    {

        DB::beginTransaction();
        try {
            $id_claim = Main::decrypt($id_claim);
            $id_user = Session::get('user')['id'];
            $claim_status = $request->input('claim_status');
            $id_car = mClaim::where('id_claim', $id_claim)->value('id_car');
            $id_customer = mCar::where('id_car', $id_car)->value('id_customer');

            $date_modified = date('Y-m-d H:i:S');
            $year = date('Y');
            $month = date('m');
            $time = date('His');
            $prefix = $id_customer . '_' . $year . '-' . $month . '-' . $time;

            $data_claim = [
                'claim_status' => $claim_status,
            ];

            mClaim::where('id_claim', $id_claim)->update($data_claim);

            if ($request->hasFile('cfile_filename_policy_insurance')) {

                $claim_file = mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'policy_insurance'
                    ])
                    ->get();
                foreach ($claim_file as $row) {
                    $from = 'upload/claim_policy_insurance/' . $row->cfile_filename;
                    \Illuminate\Support\Facades\File::delete($from);
                }

                mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'policy_insurance'
                    ])
                    ->delete();

                $data_create = [];

                foreach ($request->file('cfile_filename_policy_insurance') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_policy_insurance', $name);

                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'policy_insurance',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }
                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_damage')) {
                $claim_file = mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'damage'
                    ])
                    ->get();
                foreach ($claim_file as $row) {
                    $from = 'upload/claim_damage/' . $row->cfile_filename;
                    \Illuminate\Support\Facades\File::delete($from);
                }
                mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'damage'
                    ])
                    ->delete();

                $data_create = [];
                foreach ($request->file('cfile_filename_damage') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_damage', $name);

                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'damage',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_drive_license')) {
                $claim_file = mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'drive_license'
                    ])
                    ->get();
                foreach ($claim_file as $row) {
                    $from = 'upload/claim_drive_license/' . $row->cfile_filename;
                    \Illuminate\Support\Facades\File::delete($from);
                }

                mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'drive_license'
                    ])
                    ->delete();

                $data_create = [];
                foreach ($request->file('cfile_filename_drive_license') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_drive_license', $name);

                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'drive_license',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_police_report')) {
                $claim_file = mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'police_report'
                    ])
                    ->get();
                foreach ($claim_file as $row) {
                    $from = 'upload/claim_police_report/' . $row->cfile_filename;
                    \Illuminate\Support\Facades\File::delete($from);
                }

                mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'police_report'
                    ])
                    ->delete();

                $data_create = [];
                foreach ($request->file('cfile_filename_police_report') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_police_report', $name);
                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'police_report',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_events_chronology')) {
                $claim_file = mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'events_chronology'
                    ])
                    ->get();
                foreach ($claim_file as $row) {
                    $from = 'upload/claim_events_chronology/' . $row->cfile_filename;
                    \Illuminate\Support\Facades\File::delete($from);
                }

                mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'events_chronology'
                    ])
                    ->delete();

                $data_create = [];
                foreach ($request->file('cfile_filename_events_chronology') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_events_chronology', $name);
                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'events_chronology',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            if ($request->hasFile('cfile_filename_loss_report')) {
                $claim_file = mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'loss_report'
                    ])
                    ->get();

                foreach ($claim_file as $row) {
                    $from = 'upload/claim_loss_report/' . $row->cfile_filename;
                    \Illuminate\Support\Facades\File::delete($from);
                }

                mClaimFile
                    ::where([
                        'id_claim' => $id_claim,
                        'cfile_type' => 'loss_report'
                    ])
                    ->delete();

                $data_create = [];
                foreach ($request->file('cfile_filename_loss_report') as $image) {
                    $name = $prefix . '_' . $image->getClientOriginalName();
                    $image->move(public_path() . '/upload/claim_loss_report', $name);
                    $data_create[] = [
                        'id_claim' => $id_claim,
                        'cfile_filename' => $name,
                        'cfile_type' => 'loss_report',
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mClaimFile::insert($data_create);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }


    function detail($id_claim)
    {

        $id_claim = Main::decrypt($id_claim);
        $claim = mClaim
            ::leftJoin('car', 'car.id_car', '=', 'claim.id_car')
            ->where('id_claim', $id_claim)
            ->first();
        $breadcrumb = array_merge($this->breadcrumb(), [
            [
                'label' => 'Detail',
                'route' => route('customerDetailPage', ['id_claim' => $id_claim])
            ],
            [
                'label' => $claim->claim_number_label,
                'route' => ''
            ],
        ]);

        $data = Main::data($breadcrumb, $this->menuActive);
        $id_car = $claim->id_car;

        $claim_file_policy_insurance = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'policy_insurance'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_damage = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'damage'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_drive_license = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'drive_license'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_police_report = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'police_report'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_events_chronology = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'events_chronology'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $claim_file_loss_report = mClaimFile
            ::where([
                'id_claim' => $id_claim,
                'cfile_type' => 'loss_report'
            ])
            ->orderBy('id_claim_file', 'ASC')
            ->get();

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();

        $data = array_merge($data, [
            'row' => $row,
            'id_car' => $id_car,
            'claim' => $claim,
            'claim_file_policy_insurance' => $claim_file_policy_insurance,
            'claim_file_damage' => $claim_file_damage,
            'claim_file_drive_license' => $claim_file_drive_license,
            'claim_file_police_report' => $claim_file_police_report,
            'claim_file_events_chronology' => $claim_file_events_chronology,
            'claim_file_loss_report' => $claim_file_loss_report
        ]);


        return view('bengkel/claim_document/claim_document_detail', $data);
    }

    function delete($id_claim)
    {
        DB::beginTransaction();
        try {
            $id_claim = Main::decrypt($id_claim);
            $claim_file = mClaimFile::where('id_claim', $id_claim)->get();
            foreach ($claim_file as $row) {
                $from = 'upload/claim_' . $row->cfile_type . '/' . $row->cfile_filename;
                \Illuminate\Support\Facades\File::delete($from);
            }

            mClaim::where('id_claim', $id_claim)->delete();
            mClaimFile::where('id_claim', $id_claim)->delete();

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }
}
