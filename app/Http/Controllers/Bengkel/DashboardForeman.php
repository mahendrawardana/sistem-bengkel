<?php

namespace app\Http\Controllers\Bengkel;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class DashboardForeman extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
        $data = $this->data_dashboard_admin($request);
        return view('bengkel/dashboard_foreman/dashboard_foreman', $data);

    }

    function data_dashboard_admin($request)
    {
        $user = Session::get('user');
        $users = mUser::where('username', $user->username)->first();
        $date_range_get = $request->input('date_range');
        $date_range = explode(' - ', $date_range_get);

        $date_start = $date_range_get ? $date_range[0] : date('01-m-Y');
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $date_range_get ? $date_range[1] : date('d-m-Y');
        $date_end_db = Main::format_date_db($date_end);
        $where_date = [$date_start_db, $date_end_db];
        $date_year_now = date('Y', strtotime($date_end_db));
        $data_penjualan = [];

        $data = Main::data($this->breadcrumb);
        $total_hpp = 0;
        $total_biaya = 0;

        $total_nominal_transaksi = 0;
        $gross_profit = $total_nominal_transaksi - $total_hpp;
        $net_profit = $total_nominal_transaksi - $total_hpp - $total_biaya;
        $total_data_transaksi = 0;
        $produk = [];
        $bahan = [];

        foreach ($this->bulan as $index => $month) {
            $total_penjualan = 0;
            $data_penjualan[] = [
                'month' => $month,
                'total_penjualan' => $total_penjualan
            ];
        }

        $data = array_merge($data, array(
            'total_dokumen_klaim' => Main::format_number(104),
            'total_form_estimasi' => Main::format_number(89),
            'total_progress_klaim' => Main::format_number(81),
            'total_form_order' => Main::format_number(200),
            'date_start' => $date_start,
            'date_end' => $date_end
        ));
        return $data;
    }

    function data_dashboard_distributor()
    {

        $data = Main::data($this->breadcrumb);
        $id_distributor = Session::get('user.id');

        $list_piutang_pelanggan = mPiutangPelanggan
            ::where([
                'id_distributor' => $id_distributor,
                'pp_status' => 'belum_bayar'
            ])
            ->orderBy('id', 'ASC')
            ->get();
        $list_piutang_lain = mPiutangLain
            ::where([
                'id_distributor' => $id_distributor,
                'pl_status' => 'belum_bayar'
            ])
            ->orderBy('id_distributor', $id_distributor)
            ->get();
        $list_penjualan = mPenjualan
            ::where([
                'id_distributor' => $id_distributor
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, array(
            'list_piutang_pelanggan' => $list_piutang_pelanggan,
            'list_piutang_lain' => $list_piutang_lain,
            'list_penjualan' => $list_penjualan,

            'no_piutang' => 1,
            'no_history' => 1,
            'piutang_total' => 0,
            'history_total' => 0,
        ));

        return $data;
    }

    function detail_piutang_pelanggan($id)
    {
        $id_piutang_pelanggan = Main::decrypt($id);
        $piutang_pelanggan = mPiutangPelanggan::find($id_piutang_pelanggan);
        $penjualan = mPenjualan
            ::where([
                'id' => $piutang_pelanggan->id_penjualan
            ])
            ->first();
        $penjualan_produk = mPenjualanProduk
            ::with([
                'produk',
                'agencies',
                'stok_produk'
            ])
            ->where([
                'id_penjualan' => $piutang_pelanggan->id_penjualan
            ])
            ->get();

        $data = [
            'piutang_pelanggan' => $piutang_pelanggan,
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'no' => 1
        ];

        return view('dashboard/piutangPelangganDetail', $data);
    }

    function detail_piutang_lain($id)
    {
        $id_piutang_lain = Main::decrypt($id);
        $piutang_lain = mPiutangLain::find($id_piutang_lain);

        $data = [
            'piutang_lain' => $piutang_lain,
            //'piutang_lain' => $piutang_lain
        ];

        return view('dashboard/piutangLainDetail', $data);
    }

    function detail_history_belanja($id)
    {
        $id_penjualan = Main::decrypt($id);
        $penjualan = mPenjualan
            ::where([
                'id' => $id_penjualan
            ])
            ->first();
        $penjualan_produk = mPenjualanProduk
            ::with([
                'produk',
                'agencies',
                'stok_produk'
            ])
            ->where([
                'id_penjualan' => $id_penjualan
            ])
            ->get();

        $data = [
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'no' => 1
        ];

        return view('dashboard/historyBelanjaDetail', $data);
    }
}
