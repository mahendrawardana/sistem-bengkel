<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mEstimateParts;
use app\Models\mEstimateServices;
use app\Models\mEstimateSublet;
use app\Models\mInvoice;
use app\Models\mOrder;
use app\Models\mWorkProgress;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class Billing extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['billing'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Penagihan',
                'route' => route('billingPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "invoice_number_label"],
            ["data" => "car_police_number"],
            ["data" => "car_type"],
            ["data" => "cust_name"],
            ["data" => "insu_name"],
            ["data" => "payment_total"],
            ["data" => "invoice_print_date"],
            ["data" => "send_date"],
            ["data" => "payment_date"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_order' => $request->id_order,
            'car_police_number' => $request->car_police_number,
            'invoice_number_label' => $request->invoice_number_label,
        ));

        return view('bengkel/billing/billing_list', $data);
    }

    function datatable(Request $request)
    {
        $id_order = $request->id_order;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'order.id_order'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $total_data = mOrder::whereIn('id_order', $id_order_arr)->count();
        } else {
            $total_data = mOrder
                ::count();
        }

        $data = mOrder
            ::leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('invoice', 'invoice.id_order', '=', 'order.id_order');
        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $data = $data->whereIn('order.id_order', $id_order_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $eser_sum = mEstimateServices
                ::where([
                    'id_estimate' => $row->id_estimate,
                    'eser_valid' => 'yes'
                ])
                ->sum('eser_price_end');
            $espa_sum = mEstimateParts
                ::where([
                    'id_estimate' => $row->id_estimate,
                    'espa_valid' => 'yes'
                ])
                ->sum('espa_price_end');
            $esub_sum = mEstimateSublet
                ::where([
                    'id_estimate' => $row->id_estimate,
                    'esub_valid' => 'yes'
                ])
                ->sum('esub_price_end');
            $payment_total = $eser_sum + $espa_sum + $esub_sum;
            $send_date = $row->send_date;
            $payment_date = $row->payment_date;

            $row->no = $no;
            $row->invoice_print_date = $row->invoice_print_date ? Main::format_datetime($row->invoice_print_date) : '-';
            $row->payment_total = Main::format_number($payment_total);
            $row->send_date = $row->send_date ? Main::format_datetime($row->send_date) : '-';
            $row->payment_date = $row->payment_date ? Main::format_datetime($row->payment_date) : '-';
            $row->options = '
                <button 
                    type="button" 
                    class="btn btn-success btn-sm btn-modal-billing-update"
                    data-id-order="'.$row->id_order.'"
                    data-send-date="'.$send_date.'"
                    data-payment-date="'.$payment_date.'">
                    <i class="fa fa-check"></i>
                    Update
                </button>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {

        $car_police_number = $request->car_police_number;
        $invoice_number_label = $request->invoice_number_label;

        $check = mOrder::orderBy('order.id_order', 'ASC');

        if ($car_police_number) {
            $check = $check->leftJoin('car', 'car.id_car', '=', 'order.id_car')
                ->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($invoice_number_label) {
            $check = $check
                ->leftJoin('invoice', 'invoice.id_order', '=', 'order.id_order')
                ->where('invoice_number_label', 'like', '%' . $invoice_number_label . '%');
        }

        if ($check->count() > 0) {
            $id_order = [];
            foreach ($check->get() as $row) {
                $id_order[] = $row->id_order;
            }

            $id_order = implode(",", $id_order);

            $data = [
                'redirect' => route('billingPage', [
                    'id_order' => $id_order,
                    'car_police_number' => $car_police_number,
                    'invoice_number_label' => $invoice_number_label,
                ])];
        } else {
            $data = [
                'redirect' => route('billingPage', [
                    'car_police_number' => $car_police_number,
                    'invoice_number_label' => $invoice_number_label
                ])
            ];
        }

        return $data;
    }

    function update(Request $request)
    {
        $id_order = $request->input('id_order');
        $send_date = $request->input('send_date');
        $send_date = $send_date ? Main::format_datetime_db($send_date) : NULL;

        $payment_date = $request->input('payment_date');
        $payment_date = $payment_date ? Main::format_datetime_db($payment_date) : NULL;

        mInvoice
            ::where('id_order', $id_order)
            ->update([
                'send_date' => $send_date,
                'payment_date' => $payment_date
            ]);
    }
}
