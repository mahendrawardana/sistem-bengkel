<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mClaim;
use app\Models\mClaimFile;
use app\Models\mEstimateParts;
use app\Models\mOrder;
use app\Models\mWorkProgress;
use app\Models\mWorkProgressFile;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class DocumentValidation extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['document_validation'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Validasi Dokumen',
                'route' => route('documentValidationPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "order_number_label"],
            ["data" => "car_police_number"],
            ["data" => "car_brand"],
            ["data" => "car_type"],
            ["data" => "car_color_name"],
            ["data" => "cust_name"],
            ["data" => "ready_date"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_order' => $request->id_order,
            'car_police_number' => $request->car_police_number,
            'esti_number_label' => $request->esti_number_label,
            'order_number_label' => $request->order_number_label,
        ));
        return view('bengkel/document_validation/document_validation_list', $data);
    }

    function datatable(Request $request)
    {
        $id_order = $request->id_order;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'order.id_order'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $total_data = mOrder::whereIn('id_order', $id_order_arr)->count();
        } else {
            $total_data = mOrder
                ::count();
        }

        $data = mOrder
            ::leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer');
        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $data = $data->whereIn('id_order', $id_order_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_order = Main::encrypt($row->id_order);
            $work_progress = mWorkProgress
                ::where([
                    'work_progress_type' => 'delivery',
                    'id_order' => $row->id_order
                ])
                ->first();

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->ready_date = $work_progress ? Main::format_datetime($work_progress->work_progress_date.' '.$work_progress->work_progress_time) : '-';
            $row->options = '
                <a class="btn btn-success btn-sm" href="' . route('documentValidationDetail', ['id_order' => $id_order]) . '">
                    <i class="fa fa-file"></i>
                    Daftar Dokumen
                </a>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {

        $car_police_number = $request->car_police_number;
        $esti_number_label = $request->esti_number_label;
        $order_number_label = $request->order_number_label;

        $check = mOrder::orderBy('id_order', 'ASC');

        if ($car_police_number) {
            $check = $check->leftJoin('car', 'car.id_car', '=', 'order.id_car')
                ->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($esti_number_label) {
            $check = $check->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
                ->where('esti_number_label', 'like', '%' . $esti_number_label . '%');
        }

        if ($order_number_label) {
            $check = $check->where('order_number_label', 'like', '%' . $order_number_label . '%');
        }

        if ($check->count() == 1) {
            $id_order = $check->value('id_order');
            $id_order = Main::encrypt($id_order);
            $data = [
                'redirect' => route('workProgressEdit', ['id_order' => $id_order])
            ];
        } elseif ($check->count() > 1) {
            $id_order = [];
            foreach ($check->get() as $row) {
                $id_order[] = $row->id_order;
            }

            $id_order = implode(",", $id_order);

            $data = [
                'redirect' => route('workProgressPage', ['id_order' => $id_order, 'car_police_number' => $car_police_number, 'esti_number_label' => $esti_number_label, 'order_number_label' => $order_number_label])];
        } else {
            $data = [
                'redirect' => route('workProgressPage', [
                    'car_police_number' => $car_police_number,
                    'esti_number_label' => $esti_number_label,
                    'order_number_label' => $order_number_label
                ])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function detail($id_order)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Detail Validation',
                'route' => ''
            )
        ));

        $id_order = Main::decrypt($id_order);
        $data = Main::data($breadcrumb, $this->menuActive);
        $order = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $policy_insurance = mClaimFile
            ::where([
                'cfile_type' => 'policy_insurance',
                'id_claim' => $order->id_claim
            ])
            ->get();
        $damage = mClaimFile
            ::where([
                'cfile_type' => 'damage',
                'id_claim' => $order->id_claim
            ])
            ->get();
        $drive_license = mClaimFile
            ::where([
                'cfile_type' => 'drive_license',
                'id_claim' => $order->id_claim
            ])
            ->get();
        $police_report = mClaimFile
            ::where([
                'cfile_type' => 'police_report',
                'id_claim' => $order->id_claim
            ])
            ->get();
        $events_chronology = mClaimFile
            ::where([
                'cfile_type' => 'events_chronology',
                'id_claim' => $order->id_claim
            ])
            ->get();
        $loss_report = mClaimFile
            ::where([
                'cfile_type' => 'loss_report',
                'id_claim' => $order->id_claim
            ])
            ->get();
        $work_progress_file = mWorkProgressFile
            ::where('id_order', $id_order)
            ->first();

        $data = array_merge($data, [
            'order' => $order,
            'police_insurance' => $policy_insurance,
            'damage' => $damage,
            'drive_license' => $drive_license,
            'police_report' => $police_report,
            'events_chronology' => $events_chronology,
            'loss_report' => $loss_report,
            'work_progress_file' => $work_progress_file
        ]);

        return view('bengkel/document_validation/document_validation_detail', $data);
    }

    function surat_puas($id_order)
    {
        $id_order = Main::decrypt($id_order);

        $order = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $data = [
            'order' => $order
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/document_validation/document_validation_surat_puas', $data);

        return $pdf
            ->setPaper('A4')
            ->stream('Surat Puas Pelanggan');
    }
}
