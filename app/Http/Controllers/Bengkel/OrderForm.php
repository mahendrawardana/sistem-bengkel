<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mCar;
use app\Models\mEstimate;
use app\Models\mOrder;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class OrderForm extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['order_form'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Form Order',
                'route' => route('orderFormPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "claim_number_label"],
            ["data" => "esti_number_label"],
            ["data" => "order_number_label"],
            ["data" => "cust_name"],
            ["data" => "car_police_number"],
            ["data" => "car_production_year"],
            ["data" => "insu_name"],
            ["data" => "insu_policy_number"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_order' => $request->id_order,
            'cust_name' => $request->cust_name,
            'car_police_number' => $request->car_police_number
        ));
        return view('bengkel/order_form/order_form_list', $data);
    }

    function datatable(Request $request)
    {
        $id_order = $request->id_order;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'order.id_order'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $total_data = mOrder::whereIn('id_order', $id_order_arr)->count();
        } else {
            $total_data = mOrder
                ::count();
        }

        $data = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance');
        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $data = $data->whereIn('id_order', $id_order_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_order = Main::encrypt($row->id_order);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->options = '
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . route('orderFormDetail', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Detail Order
                        </a>
                        <a class="dropdown-item" target="_blank" href="' . route('orderFormPrint', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Cetak Order
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-red btn-hapus" 
                           href="#"
                           data-route="' . route('orderFormDelete', ['id_order' => $id_order]) . '">
                            <i class="fa fa-trash text-red"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {
        $request->validate([
            'cust_name' => 'required_without:car_police_number',
            'car_police_number' => 'required_without:cust_name',
        ]);

        $cust_name = $request->cust_name;
        $car_police_number = $request->car_police_number;
        $check = mOrder
            ::leftJoin('car', 'car.id_car', '=', 'estimate.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('cust_name', 'like', '%' . $cust_name . '%');

        if ($request->car_police_number) {
            $check = $check->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($check->count() == 1) {
            $id_order = $check->value('id_order');
            $id_order = Main::encrypt($id_order);
            $data = [
                'redirect' => route('orderFormDetail', ['id_order' => $id_order])
            ];
        } elseif ($check->count() > 1) {
            $id_order = [];
            foreach ($check->get() as $row) {
                $id_order[] = $row->id_order;
            }

            $id_order = implode(",", $id_order);

            $data = [
                'redirect' => route('orderFormPage', ['id_order' => $id_order, 'cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        } else {
            $data = [
                'redirect' => route('orderFormPage', ['cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function detail($id_order)
    {
        $id_order = Main::decrypt($id_order);
        $order = mOrder::where('id_order', $id_order)->first();
        $id_estimate = mOrder::where('id_order', $id_order)->value('id_estimate');
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Detail Form Order',
                'route' => '',
            ),
            array(
                'label' => $order->order_number_label,
                'route' => ''
            )
        ));
        $data = Main::data($breadcrumb, $this->menuActive);
        $estimate = mEstimate
            ::select([
                'estimate.*',
                'claim.claim_number_label',
                'claim.claim_status'
            ])
            ->leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->where('id_estimate', $id_estimate)
            ->first();

        $id_car = $estimate->id_car;

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();


        $data = array_merge($data, [
            'estimate' => $estimate,
            'id_car' => $id_car,
            'row' => $row,
            'order' => $order
        ]);


        return view('bengkel/order_form/order_form_detail', $data);
    }

    function print_from_estimate($id_estimate)
    {
        $id_estimate = Main::decrypt($id_estimate);
        $check_order = mOrder::where('id_estimate', $id_estimate)->count();
        if ($check_order == 0) {
            $id_car = mEstimate::where('id_estimate', $id_estimate)->value('id_car');
            $id_user = Session::get('user')['id'];
            $order_number_generate = Main::order_number();
            $order_number = $order_number_generate['order_number'];
            $order_number_label = $order_number_generate['order_number_label'];
            $order_data = [
                'id_estimate' => $id_estimate,
                'id_car' => $id_car,
                'id_user' => $id_user,
                'order_number' => $order_number,
                'order_number_label' => $order_number_label,
                'order_print_date' => date('Y-m-d H:i:s'),
                'order_date' => date('Y-m-d H:i:s')
            ];
            mOrder::create($order_data);
        }

        $estimate = mEstimate
            ::select([
                'estimate.*',
                'order.order_number_label',
                'claim.claim_number_label',
                'claim.claim_status'
            ])
            ->leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->leftJoin('order', 'order.id_estimate', '=', 'estimate.id_estimate')
            ->where('estimate.id_estimate', $id_estimate)
            ->first();

        $id_car = $estimate->id_car;

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();


        $data = [
            'estimate' => $estimate,
            'id_car' => $id_car,
            'row' => $row
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/order_form/order_form_print', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }

    function print_from_order($id_order)
    {
        $id_order = Main::decrypt($id_order);
        $id_estimate = mOrder::where('id_order', $id_order)->value('id_estimate');

        $estimate = mEstimate
            ::select([
                'estimate.*',
                'order.order_number_label',
                'claim.claim_number_label',
                'claim.claim_status'
            ])
            ->leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->leftJoin('order', 'order.id_estimate', '=', 'estimate.id_estimate')
            ->where('estimate.id_estimate', $id_estimate)
            ->first();

        $id_car = $estimate->id_car;

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();


        $data = [
            'estimate' => $estimate,
            'id_car' => $id_car,
            'row' => $row
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/order_form/order_form_print', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }

    function delete($id_order)
    {
        $id_order = Main::decrypt($id_order);
        mOrder::where('id_order', $id_order)->delete();
    }
}
