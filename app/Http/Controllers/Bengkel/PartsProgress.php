<?php

namespace app\Http\Controllers\Bengkel;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class PartsProgress extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['parts_progress'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Parts Progress',
                'route' => route('partsProgressPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        return view('bengkel/parts_progress/parts_progress_list', $data);
    }

    function edit(Request $request)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Edit Parts Progress',
                'route' => route('partsProgressEditPage')
            )
        ));

        $data = Main::data($breadcrumb, $this->menuActive);
        return view('bengkel/parts_progress/parts_progress_edit', $data);
    }

    function print(Request $request)
    {
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/parts_order/parts_order_print');

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }
}
