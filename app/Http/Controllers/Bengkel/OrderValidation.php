<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mCar;
use app\Models\mEstimate;
use app\Models\mEstimateParts;
use app\Models\mEstimateServices;
use app\Models\mEstimateSublet;
use app\Models\mInvoice;
use app\Models\mOrder;
use app\Models\mWorkProgress;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class OrderValidation extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['order_validation'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Validasi Order',
                'route' => route('documentValidationPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "order_number_label"],
            ["data" => "car_police_number"],
            ["data" => "car_brand"],
            ["data" => "car_type"],
            ["data" => "car_color_name"],
            ["data" => "cust_name"],
            ["data" => "ready_date"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_order' => $request->id_order,
            'car_police_number' => $request->car_police_number,
            'esti_number_label' => $request->esti_number_label,
            'order_number_label' => $request->order_number_label,
        ));

        return view('bengkel/order_validation/order_validation_list', $data);
    }

    function datatable(Request $request)
    {
        $id_order = $request->id_order;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'order.id_order'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $total_data = mOrder::whereIn('id_order', $id_order_arr)->count();
        } else {
            $total_data = mOrder
                ::count();
        }

        $data = mOrder
            ::leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer');
        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $data = $data->whereIn('id_order', $id_order_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_order = Main::encrypt($row->id_order);
            $work_progress = mWorkProgress
                ::where([
                    'work_progress_type' => 'delivery',
                    'id_order' => $row->id_order
                ])
                ->first();

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->ready_date = $work_progress ? Main::format_datetime($work_progress->work_progress_date . ' ' . $work_progress->work_progress_time) : '-';
            $row->options = '
                <a class="btn btn-success btn-sm" href="' . route('orderValidationDetail', ['id_order' => $id_order]) . '">
                    <i class="fa fa-check"></i>
                    Validasi
                </a>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {

        $car_police_number = $request->car_police_number;
        $esti_number_label = $request->esti_number_label;
        $order_number_label = $request->order_number_label;

        $check = mOrder::orderBy('id_order', 'ASC');

        if ($car_police_number) {
            $check = $check->leftJoin('car', 'car.id_car', '=', 'order.id_car')
                ->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($esti_number_label) {
            $check = $check->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
                ->where('esti_number_label', 'like', '%' . $esti_number_label . '%');
        }

        if ($order_number_label) {
            $check = $check->where('order_number_label', 'like', '%' . $order_number_label . '%');
        }

        if ($check->count() == 1) {
            $id_order = $check->value('id_order');
            $id_order = Main::encrypt($id_order);
            $data = [
                'redirect' => route('orderValidationDetail', ['id_order' => $id_order])
            ];
        } elseif ($check->count() > 1) {
            $id_order = [];
            foreach ($check->get() as $row) {
                $id_order[] = $row->id_order;
            }

            $id_order = implode(",", $id_order);

            $data = [
                'redirect' => route('orderValidationPage', ['id_order' => $id_order, 'car_police_number' => $car_police_number, 'esti_number_label' => $esti_number_label, 'order_number_label' => $order_number_label])];
        } else {
            $data = [
                'redirect' => route('orderValidationPage', [
                    'car_police_number' => $car_police_number,
                    'esti_number_label' => $esti_number_label,
                    'order_number_label' => $order_number_label
                ])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function detail($id_order)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Validasi Order',
                'route' => ''
            )
        ));

        $data = Main::data($breadcrumb, $this->menuActive);
        $id_order = Main::decrypt($id_order);
        $order = mOrder::where('id_order', $id_order)->first();
        $id_estimate = mOrder::where('id_order', $id_order)->value('id_estimate');
        $estimate = mEstimate
            ::select([
                'estimate.*',
                'claim.claim_number_label',
                'claim.claim_status'
            ])
            ->leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->where('id_estimate', $id_estimate)
            ->first();

        $id_car = $estimate->id_car;

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();

        $data = array_merge($data, [
            'estimate' => $estimate,
            'id_car' => $id_car,
            'row' => $row,
            'order' => $order
        ]);


        return view('bengkel/order_validation/order_validation_detail', $data);
    }

    function update(Request $request, $id_order)
    {
        $id_order = Main::decrypt($id_order);
        $id_estimate_services_arr = $request->input('id_estimate_services');
        $id_estimate_parts_arr = $request->input('id_estimate_parts');
        $id_estimate_sublet_arr = $request->input('id_estimate_sublet');
        $eser_valid_arr = $request->input('eser_valid');
        $espa_valid_arr = $request->input('espa_valid');
        $esub_valid_arr = $request->input('esub_valid');
        $order_validation_cetak = $request->input('order_validation_cetak');

        foreach ($id_estimate_services_arr as $id_estimate_services) {
            $eser_valid = isset($eser_valid_arr[$id_estimate_services]) && $eser_valid_arr[$id_estimate_services] == 'yes' ? $eser_valid_arr[$id_estimate_services] : 'no';

            mEstimateServices
                ::where('id_estimate_services', $id_estimate_services)
                ->update([
                    'eser_valid' => $eser_valid
                ]);
        }

        foreach ($id_estimate_parts_arr as $id_estimate_parts) {
            $espa_valid = isset($espa_valid_arr[$id_estimate_parts]) && $espa_valid_arr[$id_estimate_parts] == 'yes' ? $espa_valid_arr[$id_estimate_parts] : 'no';

            mEstimateParts
                ::where('id_estimate_parts', $id_estimate_parts)
                ->update([
                    'espa_valid' => $espa_valid
                ]);
        }

        foreach ($id_estimate_sublet_arr as $id_estimate_sublet) {
            $esub_valid = isset($esub_valid_arr[$id_estimate_sublet]) && $esub_valid_arr[$id_estimate_sublet] == 'yes' ? $esub_valid_arr[$id_estimate_sublet] : 'no';

            mEstimateSublet
                ::where('id_estimate_sublet', $id_estimate_sublet)
                ->update([
                    'esub_valid' => $esub_valid
                ]);
        }

        if ($order_validation_cetak == 'yes') {
            return [
                'redirect' => route('orderValidationPrint', ['id_order' => Main::encrypt($id_order)])
            ];
        }
    }

    function print_invoice($id_order)
    {
        $id_order = Main::decrypt($id_order);
        $check = mInvoice::where('id_order', $id_order)->count();

        if ($check == 0) {
            $number = Main::invoice_number();
            $data_insert = [
                'id_order' => $id_order,
                'invoice_number' => $number['invoice_number'],
                'invoice_number_label' => $number['invoice_number_label'],
                'parts_number' => $number['invoice_number'],
                'parts_number_label' => $number['parts_number_label'],
                'sublet_number' => $number['invoice_number'],
                'sublet_number_label' => $number['sublet_number_label'],
                'invoice_print_date' => date('Y-m-d H:i:s')
            ];
            mInvoice::create($data_insert);
        } else {
            $data_update = [
                'invoice_print_date' => date('Y-m-d H:i:s')
            ];
            mInvoice::where('id_order', $id_order)->update($data_update);
        }

        $detail = mOrder
            ::leftJoin('invoice', 'invoice.id_order', '=', 'order.id_order')
            ->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();
        $services = mEstimateServices
            ::where([
                'id_estimate' => $detail->id_estimate,
                'eser_valid' => 'yes'
            ])
            ->get();
        $parts = mEstimateParts
            ::where([
                'id_estimate' => $detail->id_estimate,
                'espa_valid' => 'yes'
            ])
            ->get();
        $sublet = mEstimateSublet
            ::where([
                'id_estimate' => $detail->id_estimate,
                'esub_valid' => 'yes'
            ])
            ->get();

        $data = [
            'detail' => $detail,
            'services' => $services,
            'services_total' => 0,
            'parts' => $parts,
            'parts_total' => 0,
            'sublet' => $sublet,
            'sublet_total' => 0,
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/order_validation/order_validation_invoice', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }


    function jasa(Request $request)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Biaya Jasa - Validasi Order',
                'route' => route('orderValidationJasaPage')
            )
        ));

        $data = Main::data($breadcrumb, $this->menuActive);
        return view('bengkel/order_validation/order_validation_jasa', $data);
    }

    function jasa_print(Request $request)
    {
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/order_validation/order_validation_jasa_print');

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }

    function parts(Request $request)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Biaya Parts - Validasi Order',
                'route' => route('orderValidationPartsPage')
            )
        ));

        $data = Main::data($breadcrumb, $this->menuActive);
        return view('bengkel/order_validation/order_validation_parts', $data);
    }

    function parts_print(Request $request)
    {
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/order_validation/order_validation_parts_print');

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }

    function sublet(Request $request)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Biaya Sublet - Validasi Order',
                'route' => route('orderValidationSubletPage')
            )
        ));

        $data = Main::data($breadcrumb, $this->menuActive);
        return view('bengkel/order_validation/order_validation_sublet', $data);
    }

    function sublet_print(Request $request)
    {
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/order_validation/order_validation_sublet_print');

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }
}
