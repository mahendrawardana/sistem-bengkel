<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mCar;
use app\Models\mClaim;
use app\Models\mEstimate;
use app\Models\mEstimateParts;
use app\Models\mEstimateServices;
use app\Models\mEstimateSublet;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class EstimationForm extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['estimation_form'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Dokumen Klaim',
                'route' => route('claimDocumentPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "claim_number_label"],
            ["data" => "esti_number_label"],
            ["data" => "cust_name"],
            ["data" => "car_police_number"],
            ["data" => "car_production_year"],
            ["data" => "insu_name"],
            ["data" => "insu_policy_number"],
            ["data" => "insu_expire_date"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_estimate' => $request->id_estimate,
            'cust_name' => $request->cust_name,
            'car_police_number' => $request->car_police_number
        ));
        return view('bengkel/estimation_form/estimation_form_list', $data);
    }

    function datatable(Request $request)
    {
        $id_estimate = $request->id_estimate;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'estimate.id_estimate'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_estimate) {
            $id_estimate_arr = explode(',', $id_estimate);
            $total_data = mEstimate::whereIn('id_estimate', $id_estimate_arr)->count();
        } else {
            $total_data = mEstimate
                ::count();
        }

        $data = mEstimate
            ::leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->leftJoin('car', 'car.id_car', '=', 'estimate.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance');
        if ($id_estimate) {
            $id_estimate_arr = explode(',', $id_estimate);
            $data = $data->whereIn('id_estimate', $id_estimate_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_estimate = Main::encrypt($row->id_estimate);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->options = '
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . route('estimationFormEdit', ['id_estimate' => $id_estimate]) . '">
                            <i class="fa fa-bell"></i>
                            Edit 
                        </a>
                        <a class="dropdown-item" href="' . route('estimationFormDetail', ['id_estimate' => $id_estimate]) . '">
                            <i class="fa fa-bell"></i>
                            Detail
                        </a>
                        <a class="dropdown-item" target="_blank" href="' . route('estimationFormPrint', ['id_estimate' => $id_estimate]) . '">
                            <i class="fa fa-bell"></i>
                            Cetak Estimasi
                        </a>
                        <a class="dropdown-item" target="_blank" href="' . route('orderFormEstimatePrint', ['id_estimate' => $id_estimate]) . '">
                            <i class="fa fa-bell"></i>
                            Cetak Order
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-red btn-hapus" 
                           href="#"
                           data-route="' . route('estimationFormDelete', ['id_estimate' => $id_estimate]) . '">
                            <i class="fa fa-trash text-red"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {
        $request->validate([
            'cust_name' => 'required_without:car_police_number',
            'car_police_number' => 'required_without:cust_name',
        ]);

        $cust_name = $request->cust_name;
        $car_police_number = $request->car_police_number;
        $check = mEstimate
            ::leftJoin('car', 'car.id_car', '=', 'estimate.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('cust_name', 'like', '%' . $cust_name . '%');

        if ($request->car_police_number) {
            $check = $check->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($check->count() == 1) {
            $id_estimate = $check->value('id_estimate');
            $id_estimate = Main::encrypt($id_estimate);
            $data = [
                'redirect' => route('estimationFormDetail', ['id_estimate' => $id_estimate])
            ];
        } elseif ($check->count() > 1) {
            $id_estimate = [];
            foreach ($check->get() as $row) {
                $id_estimate[] = $row->id_estimate;
            }

            $id_estimate = implode(",", $id_estimate);

            $data = [
                'redirect' => route('estimationFormPage', ['id_estimate' => $id_estimate, 'cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        } else {
            $data = [
                'redirect' => route('estimationFormPage', ['cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function create($id_car, $id_claim = NULL)
    {
        $id_car = Main::decrypt($id_car);
        $id_claim_route = $id_claim;
        $id_claim = $id_claim ? Main::decrypt($id_claim) : $id_claim;
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Form Estimasi Baru',
                'route' => route('estimationFormCreate', ['id_car' => Main::encrypt($id_car), 'id_claim' => $id_claim_route])
            )
        ));
        $data = Main::data($breadcrumb, $this->menuActive);
        $claim = mClaim::where('id_claim', $id_claim)->first();
        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();


        $data = array_merge($data, [
            'claim' => $claim,
            'id_car' => $id_car,
            'id_claim' => $id_claim,
            'row' => $row
        ]);
        return view('bengkel/estimation_form/estimation_form_create', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'eser_price_end' => 'required',
            'eser_price_end.*' => 'required',
            'espa_price_end' => 'required',
            'espa_price_end.*' => 'required',
            'esub_price_end' => 'required',
            'esub_price_end.*' => 'required',
            'esti_status' => 'required',
        ]);

        $estimate_cetak = $request->input('estimate_cetak');
        $order_cetak = $request->input('order_cetak');
        $id_car = $request->input('id_car');
        $id_claim = $request->input('id_claim');
        $id_user = Session::get('user')['id'];
        $estimate_number = Main::esti_number();
        $esti_number = $estimate_number['esti_number'];
        $esti_number_label = $estimate_number['esti_number_label'];
        $esti_price_total_services = $request->input('esti_price_total_services');
        $esti_price_total_parts = $request->input('esti_price_total_parts');
        $esti_price_total_sublet = $request->input('esti_price_total_sublet');
        $esti_price_total = $request->input('esti_price_total');
        $esti_status = $request->input('esti_status');
        $eser_price_end_arr = $request->input('eser_price_end');
        $eser_name_arr = $request->input('eser_name');
        $eser_status_arr = $request->input('eser_status');
        $eser_price_item_arr = $request->input('eser_price_item');
        $eser_discount_arr = $request->input('eser_discount');
        $espa_price_end_arr = $request->input('espa_price_end');
        $espa_name_arr = $request->input('espa_name');
        $espa_number_arr = $request->input('espa_number');
        $espa_qty_arr = $request->input('espa_qty');
        $espa_price_item_arr = $request->input('espa_price_item');
        $espa_discount_arr = $request->input('espa_discount');
        $esub_price_end_arr = $request->input('esub_price_end');
        $esub_name_arr = $request->input('esub_name');
        $esub_qty_arr = $request->input('esub_qty');
        $esub_price_item_arr = $request->input('esub_price_item');
        $esub_discount_arr = $request->input('esub_discount');
        $esti_day = $request->input('esti_day');
        $date_modified = date('Y-m-d H:i:s');

        DB::beginTransaction();
        try {
            $estimate_data = [
                'id_claim' => $id_claim,
                'id_car' => $id_car,
                'id_user' => $id_user,
                'esti_number' => $esti_number,
                'esti_number_label' => $esti_number_label,
                'esti_status' => $esti_status,
                'esti_price_total_services' => $esti_price_total_services,
                'esti_price_total_parts' => $esti_price_total_parts,
                'esti_price_total_sublet' => $esti_price_total_sublet,
                'esti_price_total' => $esti_price_total,
                'esti_day' => $esti_day
            ];

            $id_estimate = mEstimate::create($estimate_data)->id_estimate;

            $estimate_services_data = [];
            foreach ($eser_price_end_arr as $key => $eser_price_end) {
                $eser_name = $eser_name_arr[$key];
                $eser_status = $eser_status_arr[$key];
                $eser_price_item = Main::format_number_db($eser_price_item_arr[$key]);
                $eser_discount = Main::format_number_db($eser_discount_arr[$key]);

                $estimate_services_data[] = [
                    'id_estimate' => $id_estimate,
                    'eser_name' => $eser_name,
                    'eser_status' => $eser_status,
                    'eser_price_item' => $eser_price_item,
                    'eser_discount' => $eser_discount,
                    'eser_price_end' => $eser_price_end,
                    'created_at' => $date_modified,
                    'updated_at' => $date_modified,
                ];
            }
            mEstimateServices::insert($estimate_services_data);

            $estimate_parts_data = [];
            foreach ($espa_price_end_arr as $key => $espa_price_end) {
                $espa_name = $espa_name_arr[$key];
                $espa_number = $espa_number_arr[$key];
                $espa_qty = Main::format_number_db($espa_qty_arr[$key]);
                $espa_price_item = Main::format_number_db($espa_price_item_arr[$key]);
                $espa_discount = Main::format_number_db($espa_discount_arr[$key]);

                $estimate_parts_data[] = [
                    'id_estimate' => $id_estimate,
                    'espa_name' => $espa_name,
                    'espa_number' => $espa_number,
                    'espa_qty' => $espa_qty,
                    'espa_price_item' => $espa_price_item,
                    'espa_discount' => $espa_discount,
                    'espa_price_end' => $espa_price_end,
                    'created_at' => $date_modified,
                    'updated_at' => $date_modified,
                ];
            }
            mEstimateParts::insert($estimate_parts_data);

            $estimate_sublet_data = [];
            foreach ($esub_price_end_arr as $key => $esub_price_end) {
                $esub_name = $esub_name_arr[$key];
                $esub_qty = Main::format_number_db($esub_qty_arr[$key]);
                $esub_price_item = Main::format_number_db($esub_price_item_arr[$key]);
                $esub_discount = Main::format_number_db($esub_discount_arr[$key]);

                $estimate_sublet_data[] = [
                    'id_estimate' => $id_estimate,
                    'esub_name' => $esub_name,
                    'esub_qty' => $esub_qty,
                    'esub_price_item' => $esub_price_item,
                    'esub_discount' => $esub_discount,
                    'esub_price_end' => $esub_price_end,
                    'created_at' => $date_modified,
                    'updated_at' => $date_modified,
                ];
            }
            mEstimateSublet::insert($estimate_sublet_data);

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }

        if ($estimate_cetak == 'yes') {
            return [
                'redirect' => route('estimationFormPrint', ['id_estimate' => Main::encrypt($id_estimate)])
            ];
        }

        if ($order_cetak == 'yes') {
            return [
                'redirect' => route('orderFormEstimatePrint', ['id_estimate' => Main::encrypt($id_estimate)])
            ];
        }
    }

    function edit($id_estimate)
    {
        $id_estimate = Main::decrypt($id_estimate);
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Form Estimasi Edit',
                'route' => ''
            )
        ));
        $data = Main::data($breadcrumb, $this->menuActive);
        $estimate = mEstimate
            ::select([
                'estimate.*',
                'claim.claim_number',
                'claim.claim_number_label',
                'claim.claim_status',
            ])
            ->leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->where('id_estimate', $id_estimate)
            ->first();

        $id_car = $estimate->id_car;

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();


        $data = array_merge($data, [
            'estimate' => $estimate,
            'id_car' => $id_car,
            'row' => $row
        ]);

        return view('bengkel/estimation_form/estimation_form_edit', $data);
    }

    function update(Request $request, $id_estimate)
    {
        $request->validate([
            'eser_price_end' => 'required',
            'eser_price_end.*' => 'required',
            'espa_price_end' => 'required',
            'espa_price_end.*' => 'required',
            'esub_price_end' => 'required',
            'esub_price_end.*' => 'required',
        ]);

        $id_estimate = Main::decrypt($id_estimate);

        $esti_price_total_services = $request->input('esti_price_total_services');
        $esti_price_total_parts = $request->input('esti_price_total_parts');
        $esti_price_total_sublet = $request->input('esti_price_total_sublet');
        $esti_price_total = $request->input('esti_price_total');
        $esti_status = $request->input('esti_status');
        $eser_price_end_arr = $request->input('eser_price_end');
        $eser_name_arr = $request->input('eser_name');
        $eser_status_arr = $request->input('eser_status');
        $eser_price_item_arr = $request->input('eser_price_item');
        $eser_discount_arr = $request->input('eser_discount');
        $espa_price_end_arr = $request->input('espa_price_end');
        $espa_name_arr = $request->input('espa_name');
        $espa_number_arr = $request->input('espa_number');
        $espa_qty_arr = $request->input('espa_qty');
        $espa_price_item_arr = $request->input('espa_price_item');
        $espa_discount_arr = $request->input('espa_discount');
        $esub_price_end_arr = $request->input('esub_price_end');
        $esub_name_arr = $request->input('esub_name');
        $esub_qty_arr = $request->input('esub_qty');
        $esub_price_item_arr = $request->input('esub_price_item');
        $esub_discount_arr = $request->input('esub_discount');
        $esti_day = $request->input('esti_day');
        $date_modified = date('Y-m-d H:i:s');

        DB::beginTransaction();
        try {
            $estimate_data = [
                'esti_status' => $esti_status,
                'esti_price_total_services' => $esti_price_total_services,
                'esti_price_total_parts' => $esti_price_total_parts,
                'esti_price_total_sublet' => $esti_price_total_sublet,
                'esti_price_total' => $esti_price_total,
                'esti_day' => $esti_day
            ];

            mEstimate::where('id_estimate', $id_estimate)->update($estimate_data);
            mEstimateServices::where('id_estimate', $id_estimate)->delete();
            mEstimateParts::where('id_estimate', $id_estimate)->delete();
            mEstimateSublet::where('id_estimate', $id_estimate)->delete();

            $estimate_services_data = [];
            foreach ($eser_price_end_arr as $key => $eser_price_end) {
                $eser_name = $eser_name_arr[$key];
                $eser_status = $eser_status_arr[$key];
                $eser_price_item = Main::format_number_db($eser_price_item_arr[$key]);
                $eser_discount = Main::format_number_db($eser_discount_arr[$key]);

                $estimate_services_data[] = [
                    'id_estimate' => $id_estimate,
                    'eser_name' => $eser_name,
                    'eser_status' => $eser_status,
                    'eser_price_item' => $eser_price_item,
                    'eser_discount' => $eser_discount,
                    'eser_price_end' => $eser_price_end,
                    'created_at' => $date_modified,
                    'updated_at' => $date_modified,
                ];
            }
            mEstimateServices::insert($estimate_services_data);

            $estimate_parts_data = [];
            foreach ($espa_price_end_arr as $key => $espa_price_end) {
                $espa_name = $espa_name_arr[$key];
                $espa_number = $espa_number_arr[$key];
                $espa_qty = Main::format_number_db($espa_qty_arr[$key]);
                $espa_price_item = Main::format_number_db($espa_price_item_arr[$key]);
                $espa_discount = Main::format_number_db($espa_discount_arr[$key]);

                $estimate_parts_data[] = [
                    'id_estimate' => $id_estimate,
                    'espa_name' => $espa_name,
                    'espa_number' => $espa_number,
                    'espa_qty' => $espa_qty,
                    'espa_price_item' => $espa_price_item,
                    'espa_discount' => $espa_discount,
                    'espa_price_end' => $espa_price_end,
                    'created_at' => $date_modified,
                    'updated_at' => $date_modified,
                ];
            }
            mEstimateParts::insert($estimate_parts_data);

            $estimate_sublet_data = [];
            foreach ($esub_price_end_arr as $key => $esub_price_end) {
                $esub_name = $esub_name_arr[$key];
                $esub_qty = Main::format_number_db($esub_qty_arr[$key]);
                $esub_price_item = Main::format_number_db($esub_price_item_arr[$key]);
                $esub_discount = Main::format_number_db($esub_discount_arr[$key]);

                $estimate_sublet_data[] = [
                    'id_estimate' => $id_estimate,
                    'esub_name' => $esub_name,
                    'esub_qty' => $esub_qty,
                    'esub_price_item' => $esub_price_item,
                    'esub_discount' => $esub_discount,
                    'esub_price_end' => $esub_price_end,
                    'created_at' => $date_modified,
                    'updated_at' => $date_modified,
                ];
            }
            mEstimateSublet::insert($estimate_sublet_data);

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function detail($id_estimate)
    {
        $id_estimate = Main::decrypt($id_estimate);
        $estimate = mEstimate
            ::select([
                'estimate.*',
                'claim.claim_number_label',
                'claim.claim_status'
            ])
            ->leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->where('id_estimate', $id_estimate)
            ->first();

        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Form Estimasi Detail',
                'route' => ''
            ),
            array(
                'label' => $estimate->esti_number_label,
                'route' => ''
            )
        ));
        $data = Main::data($breadcrumb, $this->menuActive);

        $id_car = $estimate->id_car;

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();


        $data = array_merge($data, [
            'estimate' => $estimate,
            'id_car' => $id_car,
            'row' => $row
        ]);

        return view('bengkel/estimation_form/estimation_form_detail', $data);
    }

    function delete($id_estimate)
    {
        $id_estimate = Main::decrypt($id_estimate);
        mEstimate::where('id_estimate', $id_estimate)->delete();
        mEstimateServices::where('id_estimate', $id_estimate)->delete();
        mEstimateParts::where('id_estimate', $id_estimate)->delete();
        mEstimateSublet::where('id_estimate', $id_estimate)->delete();
    }

    function print($id_estimate)
    {
        $id_estimate = Main::decrypt($id_estimate);
        $estimate = mEstimate
            ::select([
                'estimate.*',
                'claim.claim_number_label',
                'claim.claim_status'
            ])
            ->leftJoin('claim', 'claim.id_claim', '=', 'estimate.id_claim')
            ->where('id_estimate', $id_estimate)
            ->first();

        $id_car = $estimate->id_car;

        $row = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();


        $data = [
            'estimate' => $estimate,
            'id_car' => $id_car,
            'row' => $row
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/estimation_form/estimation_form_print', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi');
    }
}
