<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mCar;
use app\Models\mCarOwner;
use app\Models\mCustomer;
use app\Models\mInsurance;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class CustomerData extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['customer_data'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Data Pelanggan',
                'route' => route('customerDataPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "car_police_number"],
            ["data" => "car_brand"],
            ["data" => "car_type"],
            ["data" => "cust_name"],
            ["data" => "cust_address"],
            ["data" => "cust_phone"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_car' => $request->id_car,
            'cust_name' => $request->cust_name,
            'car_police_number' => $request->car_police_number
        ));

        return view('bengkel/customer_data/customer_list', $data);
    }

    function datatable(Request $request)
    {
        $id_car = $request->id_car;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'customer.id_customer'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_car) {
            $id_car_arr = explode(',', $id_car);
            $total_data = mCar::whereIn('id_car', $id_car_arr)->count();
        } else {
            $total_data = mCar
                ::count();
        }

        $data = mCar
            ::leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer');
        if ($id_car) {
            $id_car_arr = explode(',', $id_car);
            $data = $data->whereIn('id_car', $id_car_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_car = Main::encrypt($row->id_car);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->options = '
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . route('customerEditPage', ['id_car' => $id_car]) . '">
                            <i class="fa fa-bell"></i>
                            Edit 
                        </a>
                        <a class="dropdown-item" href="' . route('customerExistPage', ['id_car' => $id_car]) . '">
                            <i class="fa fa-bell"></i>
                            Detail
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-red btn-hapus" 
                           href="#"
                           data-route="' . route('customerDelete', ['id_car' => $id_car]) . '">
                            <i class="fa fa-trash text-red"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {
        $request->validate([
            'cust_name' => 'required_without:car_police_number',
            'car_police_number' => 'required_without:cust_name',
        ]);

        $cust_name = $request->cust_name;
        $car_police_number = $request->car_police_number;

        $check = mCar
            ::leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('cust_name', 'like', '%' . $cust_name . '%');
        if ($request->car_police_number) {
            $check = $check->orWhere('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($check->count() == 1) {
            $id_car = $check->value('id_car');
            $id_car = Main::encrypt($id_car);
            $data = [
                'redirect' => route('customerExistPage', ['id_car' => $id_car])
            ];
        } elseif ($check->count() > 1) {
            $id_car = [];
            foreach ($check->get() as $row) {
                $id_car[] = $row->id_car;
            }

            $id_car = implode(",", $id_car);

            $data = [
                'redirect' => route('customerDataPage', ['id_car' => $id_car, 'cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        } else {
            $data = [
                'redirect' => route('customerCreatePage')
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function create()
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Baru',
                'route' => route('customerCreatePage')
            )
        ));

        $data = Main::data($breadcrumb, $this->menuActive);
        return view('bengkel/customer_data/customer_create', $data);
    }

    function insert(Request $request)
    {
//        return Response::json($request->all(), 404);

        $request->validate([
            'caro_name' => 'required',
            'caro_address' => 'required',
            'caro_city' => 'required',
            'caro_postal_code' => 'required',
            'caro_phone' => 'required',
            'caro_birthday' => 'required|date_format:d-m-Y',
            'caro_job' => 'required',
            'caro_hobby' => 'required',
            'caro_ktp_number' => 'required',
            'caro_ktp_picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'cust_name' => 'required',
            'cust_address' => 'required',
            'cust_city' => 'required',
            'cust_postal_code' => 'required',
            'cust_phone' => 'required',
            'cust_birthday' => 'required|date_format:d-m-Y',
            'cust_job' => 'required',
            'cust_hobby' => 'required',
            'cust_ktp_number' => 'required',
            'cust_ktp_picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'car_police_number' => 'required',
            'car_brand' => 'required',
            'car_type' => 'required',
            'car_color_name' => 'required',
            'car_color_code' => 'required',
            'car_production_year' => 'required',
            'car_machine_number' => 'required',
            'car_chassis_number' => 'required',
            'car_stnk_picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'insu_name' => 'required',
            'insu_address' => 'required',
            'insu_city' => 'required',
            'insu_postal_code' => 'required',
            'insu_policy_number' => 'required',
            'insu_type' => 'required',
            'insu_expire_date' => 'required|date_format:d-m-Y',
            'insu_or_value' => 'required',
            'insu_ktp_picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $caro_name = $request->caro_name;
        $caro_address = $request->caro_address;
        $caro_city = $request->caro_city;
        $caro_postal_code = $request->caro_postal_code;
        $caro_phone = $request->caro_phone;
        $caro_birthday = $request->caro_birthday;
        $caro_job = $request->caro_job;
        $caro_hobby = $request->caro_hobby;
        $caro_ktp_number = $request->caro_ktp_number;
        $caro_ktp_picture_data = $request->file('caro_ktp_picture');

        $cust_name = $request->cust_name;
        $cust_address = $request->cust_address;
        $cust_city = $request->cust_city;
        $cust_postal_code = $request->cust_postal_code;
        $cust_phone = $request->cust_phone;
        $cust_birthday = $request->cust_birthday;
        $cust_job = $request->cust_job;
        $cust_hobby = $request->cust_hobby;
        $cust_ktp_number = $request->cust_ktp_number;
        $cust_ktp_picture_data = $request->file('cust_ktp_picture');

        $car_police_number = $request->car_police_number;
        $car_brand = $request->car_brand;
        $car_type = $request->car_type;
        $car_color_name = $request->car_color_name;
        $car_color_code = $request->car_color_code;
        $car_production_year = $request->car_production_year;
        $car_machine_number = $request->car_machine_number;
        $car_chassis_number = $request->car_chassis_number;
        $car_stnk_picture_data = $request->file('car_stnk_picture');

        $insu_name = $request->insu_name;
        $insu_address = $request->insu_address;
        $insu_city = $request->insu_city;
        $insu_postal_code = $request->insu_postal_code;
        $insu_policy_number = $request->insu_policy_number;
        $insu_type = $request->insu_type;
        $insu_expire_date = $request->insu_expire_date;
        $insu_or_value = $request->insu_or_value;
        $insu_ktp_picture_data = $request->file('insu_ktp_picture');

        $id_user = Session::get('user')['id'];

        DB::beginTransaction();
        try {

            $caro_ktp_picture = Main::file_rename($caro_ktp_picture_data->getClientOriginalName());
            $caro_ktp_picture_data->move(Main::$path_file_car_owner, $caro_ktp_picture);

            $cust_ktp_picture = Main::file_rename($cust_ktp_picture_data->getClientOriginalName());
            $cust_ktp_picture_data->move(Main::$path_file_customer, $cust_ktp_picture);

            $car_stnk_picture = Main::file_rename($car_stnk_picture_data->getClientOriginalName());
            $car_stnk_picture_data->move(Main::$path_file_car, $car_stnk_picture);

            $insu_ktp_picture = Main::file_rename($insu_ktp_picture_data->getClientOriginalName());
            $insu_ktp_picture_data->move(Main::$path_file_insurance, $insu_ktp_picture);

            $data_car_owner = [
                'id_user' => $id_user,
                'caro_name' => $caro_name,
                'caro_address' => $caro_address,
                'caro_city' => $caro_city,
                'caro_postal_code' => $caro_postal_code,
                'caro_phone' => $caro_phone,
                'caro_birthday' => Main::format_date_db($caro_birthday),
                'caro_job' => $caro_job,
                'caro_hobby' => $caro_hobby,
                'caro_ktp_number' => $caro_ktp_number,
                'caro_ktp_picture' => $caro_ktp_picture,
            ];

            $id_car_owner = mCarOwner::create($data_car_owner)->id_car_owner;

            $data_insurance = [
                'id_user' => $id_user,
                'insu_name' => $insu_name,
                'insu_address' => $insu_address,
                'insu_city' => $insu_city,
                'insu_postal_code' => $insu_postal_code,
                'insu_policy_number' => $insu_policy_number,
                'insu_type' => $insu_type,
                'insu_expire_date' => Main::format_date_db($insu_expire_date),
                'insu_or_value' => $insu_or_value,
                'insu_ktp_picture' => $insu_ktp_picture,
            ];

            $id_insurance = mInsurance::create($data_insurance)->id_insurance;

            $data_customer = [
                'id_user' => $id_user,
                'cust_name' => $cust_name,
                'cust_address' => $cust_address,
                'cust_city' => $cust_city,
                'cust_postal_code' => $cust_postal_code,
                'cust_phone' => $cust_phone,
                'cust_birthday' => Main::format_date_db($cust_birthday),
                'cust_job' => $cust_job,
                'cust_hobby' => $cust_hobby,
                'cust_ktp_number' => $cust_ktp_number,
                'cust_ktp_picture' => $cust_ktp_picture,
            ];

            $id_customer = mCustomer::create($data_customer)->id_customer;

            $data_car = [
                'id_user' => $id_user,
                'id_car_owner' => $id_car_owner,
                'id_customer' => $id_customer,
                'id_insurance' => $id_insurance,
                'car_police_number' => $car_police_number,
                'car_brand' => $car_brand,
                'car_type' => $car_type,
                'car_color_name' => $car_color_name,
                'car_color_code' => $car_color_code,
                'car_production_year' => $car_production_year,
                'car_machine_number' => $car_machine_number,
                'car_chassis_number' => $car_chassis_number,
                'car_stnk_picture' => $car_stnk_picture,
            ];

            mCar::create($data_car);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function edit($id_car)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Edit',
                'route' => route('customerEditPage', ['id_car' => $id_car])

            )
        ));

        $id_car = Main::decrypt($id_car);
        $data = Main::data($breadcrumb, $this->menuActive);
        $car = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();

        $data = array_merge($data, [
            'car' => $car,
            'id_car' => Main::encrypt($car->id_car)
        ]);

        return view('bengkel/customer_data/customer_edit', $data);
    }

    function update(Request $request, $id_car)
    {
        $request->validate([
            'caro_name' => 'required',
            'caro_address' => 'required',
            'caro_city' => 'required',
            'caro_postal_code' => 'required',
            'caro_phone' => 'required',
            'caro_birthday' => 'required|date_format:d-m-Y',
            'caro_job' => 'required',
            'caro_hobby' => 'required',
            'caro_ktp_number' => 'required',
            'caro_ktp_picture' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'cust_name' => 'required',
            'cust_address' => 'required',
            'cust_city' => 'required',
            'cust_postal_code' => 'required',
            'cust_phone' => 'required',
            'cust_birthday' => 'required|date_format:d-m-Y',
            'cust_job' => 'required',
            'cust_hobby' => 'required',
            'cust_ktp_number' => 'required',
            'cust_ktp_picture' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'car_police_number' => 'required',
            'car_brand' => 'required',
            'car_type' => 'required',
            'car_color_name' => 'required',
            'car_color_code' => 'required',
            'car_production_year' => 'required',
            'car_machine_number' => 'required',
            'car_chassis_number' => 'required',
            'car_stnk_picture' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'insu_name' => 'required',
            'insu_address' => 'required',
            'insu_city' => 'required',
            'insu_postal_code' => 'required',
            'insu_policy_number' => 'required',
            'insu_type' => 'required',
            'insu_expire_date' => 'required|date_format:d-m-Y',
            'insu_or_value' => 'required',
            'insu_ktp_picture' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $caro_name = $request->caro_name;
        $caro_address = $request->caro_address;
        $caro_city = $request->caro_city;
        $caro_postal_code = $request->caro_postal_code;
        $caro_phone = $request->caro_phone;
        $caro_birthday = $request->caro_birthday;
        $caro_job = $request->caro_job;
        $caro_hobby = $request->caro_hobby;
        $caro_ktp_number = $request->caro_ktp_number;
        $caro_ktp_picture_data = $request->file('caro_ktp_picture');

        $cust_name = $request->cust_name;
        $cust_address = $request->cust_address;
        $cust_city = $request->cust_city;
        $cust_postal_code = $request->cust_postal_code;
        $cust_phone = $request->cust_phone;
        $cust_birthday = $request->cust_birthday;
        $cust_job = $request->cust_job;
        $cust_hobby = $request->cust_hobby;
        $cust_ktp_number = $request->cust_ktp_number;
        $cust_ktp_picture_data = $request->file('cust_ktp_picture');

        $car_police_number = $request->car_police_number;
        $car_brand = $request->car_brand;
        $car_type = $request->car_type;
        $car_color_name = $request->car_color_name;
        $car_color_code = $request->car_color_code;
        $car_production_year = $request->car_production_year;
        $car_machine_number = $request->car_machine_number;
        $car_chassis_number = $request->car_chassis_number;
        $car_stnk_picture_data = $request->file('car_stnk_picture');

        $insu_name = $request->insu_name;
        $insu_address = $request->insu_address;
        $insu_city = $request->insu_city;
        $insu_postal_code = $request->insu_postal_code;
        $insu_policy_number = $request->insu_policy_number;
        $insu_type = $request->insu_type;
        $insu_expire_date = $request->insu_expire_date;
        $insu_or_value = $request->insu_or_value;
        $insu_ktp_picture_data = $request->file('insu_ktp_picture');

        $id_car = Main::decrypt($id_car);
        $car = mCar::where('id_car', $id_car)->first();

        DB::beginTransaction();
        try {

            $data_car_owner = [
                'caro_name' => $caro_name,
                'caro_address' => $caro_address,
                'caro_city' => $caro_city,
                'caro_postal_code' => $caro_postal_code,
                'caro_phone' => $caro_phone,
                'caro_birthday' => Main::format_date_db($caro_birthday),
                'caro_job' => $caro_job,
                'caro_hobby' => $caro_hobby,
                'caro_ktp_number' => $caro_ktp_number,
            ];

            if ($request->hasFile('caro_ktp_picture')) {
                $caro_ktp_picture = Main::file_rename($caro_ktp_picture_data->getClientOriginalName());
                $caro_ktp_picture_data->move(Main::$path_file_car_owner, $caro_ktp_picture);

                $data_car_owner['caro_ktp_picture'] = $caro_ktp_picture;
            }

            mCarOwner
                ::where(['id_car_owner' => $car->id_car_owner])
                ->update($data_car_owner);

            $data_insurance = [
                'insu_name' => $insu_name,
                'insu_address' => $insu_address,
                'insu_city' => $insu_city,
                'insu_postal_code' => $insu_postal_code,
                'insu_policy_number' => $insu_policy_number,
                'insu_type' => $insu_type,
                'insu_expire_date' => Main::format_date_db($insu_expire_date),
                'insu_or_value' => $insu_or_value,
            ];

            if ($request->hasFile('insu_ktp_picture')) {
                $insu_ktp_picture = Main::file_rename($insu_ktp_picture_data->getClientOriginalName());
                $insu_ktp_picture_data->move(Main::$path_file_insurance, $insu_ktp_picture);

                $data_insurance['insu_ktp_picture'] = $insu_ktp_picture;
            }

            mInsurance
                ::where([
                    'id_insurance' => $car->id_insurance
                ])
                ->update($data_insurance);

            $data_customer = [
                'cust_name' => $cust_name,
                'cust_address' => $cust_address,
                'cust_city' => $cust_city,
                'cust_postal_code' => $cust_postal_code,
                'cust_phone' => $cust_phone,
                'cust_birthday' => Main::format_date_db($cust_birthday),
                'cust_job' => $cust_job,
                'cust_hobby' => $cust_hobby,
                'cust_ktp_number' => $cust_ktp_number,
            ];


            if ($request->hasFile('cust_ktp_picture')) {
                $cust_ktp_picture = Main::file_rename($cust_ktp_picture_data->getClientOriginalName());
                $cust_ktp_picture_data->move(Main::$path_file_customer, $cust_ktp_picture);

                $data_customer['cust_ktp_picture'] = $cust_ktp_picture;
            }

            mCustomer
                ::where(['id_customer' => $car->id_customer])
                ->update($data_customer);

            $data_car = [
                'car_police_number' => $car_police_number,
                'car_brand' => $car_brand,
                'car_type' => $car_type,
                'car_color_name' => $car_color_name,
                'car_color_code' => $car_color_code,
                'car_production_year' => $car_production_year,
                'car_machine_number' => $car_machine_number,
                'car_chassis_number' => $car_chassis_number,
            ];

            if ($request->hasFile('car_stnk_picture')) {
                $car_stnk_picture = Main::file_rename($car_stnk_picture_data->getClientOriginalName());
                $car_stnk_picture_data->move(Main::$path_file_car, $car_stnk_picture);

                $data_car['car_stnk_picture'] = $car_stnk_picture;
            }

            mCar
                ::where(['id_car' => $car->id_car])
                ->update($data_car);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    function exist($id_car)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Tersedia',
                'route' => route('customerExistPage', ['id_car' => $id_car])
            )
        ));

        $id_car = Main::decrypt($id_car);
        $data = Main::data($breadcrumb, $this->menuActive);
        $car = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();

        $data = array_merge($data, [
            'car' => $car,
            'id_car' => Main::encrypt($car->id_car)
        ]);

        return view('bengkel/customer_data/customer_exist', $data);
    }

    function detail($id_car)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Detail',
                'route' => route('customerDetailPage', ['id_car' => $id_car])
            )
        ));

        $id_car = Main::decrypt($id_car);
        $data = Main::data($breadcrumb, $this->menuActive);
        $car = mCar
            ::with([
                'customer',
                'insurance',
                'car_owner'
            ])
            ->where('id_car', $id_car)
            ->first();

        $data = array_merge($data, [
            'car' => $car,
            'id_car' => Main::encrypt($car->id_car)
        ]);

        return view('bengkel/customer_data/customer_detail', $data);
    }

    function delete($id_car)
    {
        $id_car = Main::decrypt($id_car);
        $car = mCar::where('id_car', $id_car)->first();

        mCar::where('id_car', $id_car)->delete();
        mCarOwner::where('id_car_owner', $car->id_car_owner)->delete();
        mInsurance::where('id_insurance', $car->id_insurance)->delete();
        mCustomer::where('id_customer', $car->id_customer)->delete();
    }
}
