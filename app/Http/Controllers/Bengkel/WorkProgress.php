<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mCar;
use app\Models\mClaimFile;
use app\Models\mEstimateParts;
use app\Models\mMechanic;
use app\Models\mOrder;
use app\Models\mQcProcess;
use app\Models\mWorkPlan;
use app\Models\mWorkProgress;
use app\Models\mWorkProgressFile;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class WorkProgress extends Controller
{
    private $menuActive;
    private $work_plan_type_arr;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['work_progress'];
        $this->work_plan_type_arr = [
            'panel_repair',
            'pendempulan',
            'surfacer',
            'spraying',
            'polishing',
            'finishing',
            'delivery',
        ];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Progress Pekerjaan',
                'route' => route('workProgressPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "order_number_label"],
            ["data" => "car_police_number"],
            ["data" => "car_brand"],
            ["data" => "car_type"],
            ["data" => "car_color_name"],
            ["data" => "cust_name"],
            ["data" => "parts_count"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_order' => $request->id_order,
            'car_police_number' => $request->car_police_number,
            'esti_number_label' => $request->esti_number_label,
            'order_number_label' => $request->order_number_label,
        ));
        return view('bengkel/work_progress/work_progress_list', $data);
    }

    function datatable(Request $request)
    {
        $id_order = $request->id_order;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'order.id_order'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $total_data = mOrder::whereIn('id_order', $id_order_arr)->count();
        } else {
            $total_data = mOrder
                ::count();
        }

        $data = mOrder
            ::leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer');
        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $data = $data->whereIn('id_order', $id_order_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_order = Main::encrypt($row->id_order);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->parts_count = mEstimateParts::where('id_estimate', $row->id_estimate)->sum('espa_qty');
            $row->options = '
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . route('workProgressEdit', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Edit
                        </a>
                        <a class="dropdown-item" target="_blank" href="' . route('workProgressPrint', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Cetak
                        </a>
                    </div>
                </div>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {

        $car_police_number = $request->car_police_number;
        $esti_number_label = $request->esti_number_label;
        $order_number_label = $request->order_number_label;

        $check = mOrder::orderBy('id_order', 'ASC');

        if ($car_police_number) {
            $check = $check->leftJoin('car', 'car.id_car', '=', 'order.id_car')
                ->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($esti_number_label) {
            $check = $check->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
                ->where('esti_number_label', 'like', '%' . $esti_number_label . '%');
        }

        if ($order_number_label) {
            $check = $check->where('order_number_label', 'like', '%' . $order_number_label . '%');
        }

        if ($check->count() == 1) {
            $id_order = $check->value('id_order');
            $id_order = Main::encrypt($id_order);
            $data = [
                'redirect' => route('workProgressEdit', ['id_order' => $id_order])
            ];
        } elseif ($check->count() > 1) {
            $id_order = [];
            foreach ($check->get() as $row) {
                $id_order[] = $row->id_order;
            }

            $id_order = implode(",", $id_order);

            $data = [
                'redirect' => route('workProgressPage', ['id_order' => $id_order, 'car_police_number' => $car_police_number, 'esti_number_label' => $esti_number_label, 'order_number_label' => $order_number_label])];
        } else {
            $data = [
                'redirect' => route('workProgressPage', [
                    'car_police_number' => $car_police_number,
                    'esti_number_label' => $esti_number_label,
                    'order_number_label' => $order_number_label
                ])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function edit($id_order)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Edit Work Plan',
                'route' => ''
            )
        ));

        $data = Main::data($breadcrumb, $this->menuActive);

        $id_order = Main::decrypt($id_order);

        $order = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();
        $mechanic = mMechanic::orderBy('mechanic_name', 'ASC')->get();
        $work_progress_file = mWorkProgressFile::where('id_order', $id_order)->first();
        $work_plan_data = mWorkPlan
            ::leftJoin('mechanic', 'mechanic.id_mechanic', '=', 'work_plan.id_mechanic')
            ->where('id_order', $id_order)
            ->get();
        $work_plan = [];
        foreach ($work_plan_data as $row) {
            $work_plan[$row->work_plan_type] = $row;
        }

        foreach ($this->work_plan_type_arr as $type) {
            $data['work_progress'][$type] = mWorkProgress
                ::where(['id_order' => $id_order, 'work_progress_type' => $type])
                ->first();
        }

        $data = array_merge($data, [
            'mechanic' => $mechanic,
            'order' => $order,
            'type_list' => $this->work_plan_type_arr,
            'work_plan' => $work_plan,
            'work_progress_file' => $work_progress_file
        ]);

        return view('bengkel/work_progress/work_progress_edit', $data);
    }

    function update(Request $request, $id_order)
    {
        $request->validate([
            'work_progress_date' => 'required',
            'work_progress_date.*' => 'required',
            'work_progress_time' => 'required',
            'work_progress_time.*' => 'required',
            'id_mechanic' => 'required',
            'id_mechanic.*' => 'required',
        ]);

        $id_order = Main::decrypt($id_order);
        $work_progress_date = $request->input('work_progress_date');
        $work_progress_time = $request->input('work_progress_time');
        $id_mechanic = $request->input('id_mechanic');
        $id_car = $request->input('id_car');
        $work_progress_cetak = $request->input('work_progress_cetak');
        $id_customer = mCar::where('id_car', $id_car)->value('id_customer');
        $id_user = Session::get('user')['id'];
        $date_modified = date('Y-m-d H:i:s');
        $year = date('Y');
        $month = date('m');
        $time = date('His');
        $prefix = $id_customer . '_' . $year . '-' . $month . '-' . $time;;

        DB::beginTransaction();
        try {

            $check_work_progress = mWorkProgress::where('id_order', $id_order)->count();
            $check_work_progress_file = mWorkProgressFile::where('id_order', $id_order)->count();

            if ($check_work_progress > 0) {
                foreach ($this->work_plan_type_arr as $type) {
                    $work_progress_data = [
                        'id_mechanic' => $id_mechanic[$type],
                        'work_progress_date' => Main::format_date_db($work_progress_date[$type]),
                        'work_progress_time' => Main::format_time_db($work_progress_time[$type])
                    ];

                    mWorkProgress
                        ::where(['id_order' => $id_order, 'work_progress_type' => $type])
                        ->update($work_progress_data);
                }
            } else {
                $work_progress_data = [];
                foreach ($this->work_plan_type_arr as $type) {
                    $work_progress_data[] = [
                        'id_order' => $id_order,
                        'id_user' => $id_user,
                        'id_mechanic' => $id_mechanic[$type],
                        'work_progress_date' => Main::format_date_db($work_progress_date[$type]),
                        'work_progress_time' => Main::format_time_db($work_progress_time[$type]),
                        'work_progress_type' => $type,
                        'created_at' => $date_modified,
                        'updated_at' => $date_modified
                    ];
                }

                mWorkProgress::insert($work_progress_data);
            }

            if ($check_work_progress_file == 0) {
                mWorkProgressFile::create([
                    'id_order' => $id_order,
                    'id_user' => $id_user
                ]);
            }

            $work_progress_file = mWorkProgressFile::where('id_order', $id_order)->first();

            if ($request->hasFile('wopf_parts_lama')) {
                $image = $request->file('wopf_parts_lama') ;
                $name = $prefix. '_' . $image->getClientOriginalName();
                $image->move(public_path() . '/upload/wopf_parts_lama', $name);

                $from = 'upload/wopf_parts_lama/' . $work_progress_file->wopf_parts_lama;
                \Illuminate\Support\Facades\File::delete($from);

                $data_update = [
                    'wopf_parts_lama' => $name,
                ];

                mWorkProgressFile::where('id_order', $id_order)->update($data_update);
            }

            if ($request->hasFile('wopf_parts_baru')) {
                $image = $request->file('wopf_parts_baru') ;
                $name = $prefix. '_' . $image->getClientOriginalName();
                $image->move(public_path() . '/upload/wopf_parts_baru', $name);

                $from = 'upload/wopf_parts_baru/' . $work_progress_file->wopf_parts_baru;
                \Illuminate\Support\Facades\File::delete($from);


                $data_update = [
                    'wopf_parts_baru' => $name,
                ];

                mWorkProgressFile::where('id_order', $id_order)->update($data_update);
            }

            if ($request->hasFile('wopf_epoxy')) {
                $image = $request->file('wopf_epoxy') ;
                $name = $prefix. '_' . $image->getClientOriginalName();
                $image->move(public_path() . '/upload/wopf_epoxy', $name);

                $from = 'upload/wopf_epoxy/' . $work_progress_file->wopf_epoxy;
                \Illuminate\Support\Facades\File::delete($from);

                $data_update = [
                    'wopf_epoxy' => $name,
                ];

                mWorkProgressFile::where('id_order', $id_order)->update($data_update);
            }

            if ($request->hasFile('wopf_hasil_pekerjaan')) {
                $image = $request->file('wopf_hasil_pekerjaan') ;
                $name = $prefix. '_' . $image->getClientOriginalName();
                $image->move(public_path() . '/upload/wopf_hasil_pekerjaan', $name);

                $from = 'upload/wopf_hasil_pekerjaan/' . $work_progress_file->wopf_hasil_pekerjaan;
                \Illuminate\Support\Facades\File::delete($from);

                $data_update = [
                    'wopf_hasil_pekerjaan' => $name,
                ];

                mWorkProgressFile::where('id_order', $id_order)->update($data_update);
            }

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();

        }

        if ($work_progress_cetak == 'yes') {
            return [
                'redirect' => route('workProgressPrint', ['id_order' => Main::encrypt($id_order)])
            ];
        }

    }

    function print($id_order)
    {
        $id_order = Main::decrypt($id_order);
        $data['work_plan_type_arr'] = $this->work_plan_type_arr;
        $data['order'] = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $data['work_progress_file'] = mWorkProgressFile::where('id_order', $id_order)->first();
        $data['work_plan_data'] = mWorkPlan
            ::leftJoin('mechanic', 'mechanic.id_mechanic', '=', 'work_plan.id_mechanic')
            ->where('id_order', $id_order)
            ->get();
        $data['type_list'] = $this->work_plan_type_arr;
        $data['work_plan'] = [];
        foreach ($data['work_plan_data'] as $row) {
            $data['work_plan'][$row->work_plan_type] = $row;
        }

        foreach ($this->work_plan_type_arr as $type) {
            $data['work_progress'][$type] = mWorkProgress
                ::leftJoin('mechanic', 'mechanic.id_mechanic', '=', 'work_progress.id_mechanic')
                ->where(['work_progress.id_order' => $id_order, 'work_progress_type' => $type])
                ->first();
        }

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/work_progress/work_progress_print', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Form Estimasi atau Order');
    }
}
