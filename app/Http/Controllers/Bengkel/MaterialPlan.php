<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mEstimateParts;
use app\Models\mMateriaPlan;
use app\Models\mMechanic;
use app\Models\mOrder;
use app\Models\mWorkPlan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class MaterialPlan extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['material_plan'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Material Plan',
                'route' => route('materialPlanPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "car_police_number"],
            ["data" => "car_brand"],
            ["data" => "car_type"],
            ["data" => "car_color_name"],
            ["data" => "cust_name"],
            ["data" => "order_number_label"],
            ["data" => "parts_count"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_order' => $request->id_order,
            'car_police_number' => $request->car_police_number,
            'esti_number_label' => $request->esti_number_label,
            'order_number_label' => $request->order_number_label,
        ));
        return view('bengkel/material_plan/material_plan_list', $data);
    }

    function datatable(Request $request)
    {
        $id_order = $request->id_order;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'order.id_order'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $total_data = mOrder::whereIn('id_order', $id_order_arr)->count();
        } else {
            $total_data = mOrder
                ::count();
        }

        $data = mOrder
            ::leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer');
        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $data = $data->whereIn('id_order', $id_order_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_order = Main::encrypt($row->id_order);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->parts_count = mEstimateParts::where('id_estimate', $row->id_estimate)->sum('espa_qty');
            $row->options = '
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . route('materialPlanEdit', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Edit
                        </a>
                        <a class="dropdown-item" target="_blank" href="' . route('materialPlanPrint', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Cetak
                        </a>
                    </div>
                </div>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {
//        $request->validate([
//            'car_police_number' => 'required_without:esti_number_label,order_number_label',
//            'esti_number_label' => 'required_without:car_police_number,order_number_label',
//            'order_number_label' => 'required_without:car_police_number,esti_number_label',
//        ]);

        $car_police_number = $request->car_police_number;
        $esti_number_label = $request->esti_number_label;
        $order_number_label = $request->order_number_label;

        $check = mOrder::orderBy('id_order', 'ASC');

        if ($car_police_number) {
            $check = $check->leftJoin('car', 'car.id_car', '=', 'order.id_car')
                ->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($esti_number_label) {
            $check = $check->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
                ->where('esti_number_label', 'like', '%' . $esti_number_label . '%');
        }

        if ($order_number_label) {
            $check = $check->where('order_number_label', 'like', '%' . $order_number_label . '%');
        }

        if ($check->count() == 1) {
            $id_order = $check->value('id_order');
            $id_order = Main::encrypt($id_order);
            $data = [
                'redirect' => route('materialPlanEdit', ['id_order' => $id_order])
            ];
        } elseif ($check->count() > 1) {
            $id_order = [];
            foreach ($check->get() as $row) {
                $id_order[] = $row->id_order;
            }

            $id_order = implode(",", $id_order);

            $data = [
                'redirect' => route('materialPlanPage', ['id_order' => $id_order, 'car_police_number' => $car_police_number, 'esti_number_label' => $esti_number_label, 'order_number_label' => $order_number_label])];
        } else {
            $data = [
                'redirect' => route('materialPlanPage', [
                    'car_police_number' => $car_police_number,
                    'esti_number_label' => $esti_number_label,
                    'order_number_label' => $order_number_label
                ])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function edit($id_order)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Edit Material Plan',
                'route' => ''
            )
        ));

        $data = Main::data($breadcrumb, $this->menuActive);
        $id_order = Main::decrypt($id_order);

        $order = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $work_plan_non_paint = mMateriaPlan
            ::where([
                'id_order'=>$id_order,
                'mapl_type' => 'non_paint'
            ])
            ->orderBy('id_material_plan', 'ASC')
            ->get();
        $work_plan_paint = mMateriaPlan
            ::where([
                'id_order'=>$id_order,
                'mapl_type' => 'paint'
            ])
            ->orderBy('id_material_plan', 'ASC')
            ->get();

        $data = array_merge($data, [
            'order' => $order,
            'work_plan_non_paint' => $work_plan_non_paint,
            'work_plan_paint' => $work_plan_paint
        ]);

        return view('bengkel/material_plan/material_plan_edit', $data);
    }

    function update(Request $request, $id_order)
    {
        $request->validate([
            'mapl_item_name' => 'required',
            'mapl_item_name.non_paint' => 'required',
            'mapl_item_name.non_paint.*' => 'required',
            'mapl_item_name.paint' => 'required',
            'mapl_item_name.paint.*' => 'required',

            'mapl_item_code' => 'required',
            'mapl_item_code.non_paint' => 'required',
            'mapl_item_code.non_paint.*' => 'required',
            'mapl_item_code.paint' => 'required',
            'mapl_item_code.paint.*' => 'required',

            'mapl_actual' => 'required',
            'mapl_actual.non_paint' => 'required',
            'mapl_actual.non_paint.*' => 'required',
            'mapl_actual.paint' => 'required',
            'mapl_actual.paint.*' => 'required',


            'mapl_item_price' => 'required',
            'mapl_item_price.non_paint' => 'required',
            'mapl_item_price.non_paint.*' => 'required',
            'mapl_item_price.paint' => 'required',
            'mapl_item_price.paint.*' => 'required',

        ]);

        $id_order = Main::decrypt($id_order);
        $id_user = Session::get('user')['id'];
        $mapl_item_name_arr = $request->input('mapl_item_name');
        $mapl_item_code_arr = $request->input('mapl_item_code');
        $mapl_plan_arr = $request->input('mapl_plan');
        $mapl_actual_arr = $request->input('mapl_actual');
        $mapl_item_price_arr = $request->input('mapl_item_price');
        $mapl_price_total_arr = $request->input('mapl_price_total');
        $date_modified = date('Y-m-d H:i:s');
        $material_plan_cetak = $request->input('material_plan_cetak');

        mMateriaPlan::where('id_order', $id_order)->delete();

        $material_plan_data = [];
        $type_arr = ['non_paint', 'paint'];
        foreach ($type_arr as $type) {
            foreach ($mapl_item_name_arr[$type] as $key => $mapl_item_name) {
                $mapl_item_code = $mapl_item_code_arr[$type][$key];
                $mapl_plan = $mapl_plan_arr[$type][$key];
                $mapl_actual = $mapl_actual_arr[$type][$key];
                $mapl_item_price = $mapl_item_price_arr[$type][$key];
                $mapl_price_total = $mapl_price_total_arr[$type][$key];

                $material_plan_data[] = [
                    'id_order' => $id_order,
                    'id_user' => $id_user,
                    'mapl_item_name' => $mapl_item_name,
                    'mapl_item_code' => $mapl_item_code,
                    'mapl_plan' => $mapl_plan,
                    'mapl_actual' => $mapl_actual,
                    'mapl_item_price' => $mapl_item_price,
                    'mapl_price_total' => $mapl_price_total,
                    'mapl_type' => $type,
                    'created_at' => $date_modified,
                    'updated_at' => $date_modified
                ];
            }
        }

        mMateriaPlan::insert($material_plan_data);

        if($material_plan_cetak == 'yes') {
            return [
                'redirect' => route('materialPlanPrint', ['id_order' => Main::encrypt($id_order)])
            ];
        }

    }

    function print($id_order)
    {
        $id_order = Main::decrypt($id_order);
        $data['order'] = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $data['material_plan_non_paint'] = mMateriaPlan
            ::where([
                'id_order' => $id_order,
                'mapl_type' => 'non_paint'
            ])
            ->orderBy('id_material_plan', 'ASC')
            ->get();

        $data['material_plan_paint'] = mMateriaPlan
            ::where([
                'id_order' => $id_order,
                'mapl_type' => 'paint'
            ])
            ->orderBy('id_material_plan', 'ASC')
            ->get();


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/material_plan/material_plan_print', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }
}
