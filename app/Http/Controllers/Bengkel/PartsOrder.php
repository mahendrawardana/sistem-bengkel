<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mEstimateParts;
use app\Models\mOrder;
use app\Models\mPartsOrder;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class PartsOrder extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['parts_order'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Parts Order',
                'route' => route('partsOrderPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "order_number_label"],
            ["data" => "car_police_number"],
            ["data" => "car_brand"],
            ["data" => "car_type"],
            ["data" => "car_color_name"],
            ["data" => "cust_name"],
            ["data" => "parts_count"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_order' => $request->id_order,
            'car_police_number' => $request->car_police_number,
            'esti_number_label' => $request->esti_number_label,
            'order_number_label' => $request->order_number_label,
        ));
        return view('bengkel/parts_order/parts_order_list', $data);
    }

    function datatable(Request $request)
    {
        $id_order = $request->id_order;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'order.id_order'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $total_data = mOrder::whereIn('id_order', $id_order_arr)->count();
        } else {
            $total_data = mOrder
                ::count();
        }

        $data = mOrder
            ::leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer');
        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $data = $data->whereIn('id_order', $id_order_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_order = Main::encrypt($row->id_order);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->parts_count = mEstimateParts::where('id_estimate', $row->id_estimate)->sum('espa_qty');
            $row->options = '
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . route('partsOrder', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Parts
                        </a>
                        <a class="dropdown-item" target="_blank" href="' . route('partsOrderPrint', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Cetak
                        </a>
                    </div>
                </div>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {
//        $request->validate([
//            'car_police_number' => 'required_without:esti_number_label,order_number_label',
//            'esti_number_label' => 'required_without:car_police_number,order_number_label',
//            'order_number_label' => 'required_without:car_police_number,esti_number_label',
//        ]);

        $car_police_number = $request->car_police_number;
        $esti_number_label = $request->esti_number_label;
        $order_number_label = $request->order_number_label;

        $check = mOrder::orderBy('id_order', 'ASC');

        if ($car_police_number) {
            $check = $check->leftJoin('car', 'car.id_car', '=', 'order.id_car')
                ->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($esti_number_label) {
            $check = $check->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
                ->where('esti_number_label', 'like', '%' . $esti_number_label . '%');
        }

        if ($order_number_label) {
            $check = $check->where('order_number_label', 'like', '%' . $order_number_label . '%');
        }

        if ($check->count() == 1) {
            $id_order = $check->value('id_order');
            $id_order = Main::encrypt($id_order);
            $data = [
                'redirect' => route('partsOrder', ['id_order' => $id_order])
            ];
        } elseif ($check->count() > 1) {
            $id_order = [];
            foreach ($check->get() as $row) {
                $id_order[] = $row->id_order;
            }

            $id_order = implode(",", $id_order);

            $data = [
                'redirect' => route('partsOrderPage', ['id_order' => $id_order, 'car_police_number' => $car_police_number, 'esti_number_label' => $esti_number_label, 'order_number_label' => $order_number_label])];
        } else {
            $data = [
                'redirect' => route('materialPlanPage', [
                    'car_police_number' => $car_police_number,
                    'esti_number_label' => $esti_number_label,
                    'order_number_label' => $order_number_label
                ])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function edit($id_order)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Parts Order',
                'route' => ''
            )
        ));
        $data = Main::data($breadcrumb, $this->menuActive);
        $id_order = Main::decrypt($id_order);

        $order = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();


        $parts = mEstimateParts
            ::select([
                'parts_order.*',
                'estimate_parts.*',
                'estimate_parts.id_estimate_parts AS id_estimate_parts_from'
            ])
            ->leftJoin('parts_order', 'parts_order.id_estimate_parts', '=', 'estimate_parts.id_estimate_parts')
            ->where('estimate_parts.id_estimate', $order->id_estimate)
            ->orderBy('estimate_parts.id_estimate_parts', 'ASC')
            ->get();

        $data = array_merge($data, [
            'order' => $order,
            'parts' => $parts
        ]);

        return view('bengkel/parts_order/parts_order_edit', $data);
    }

    function update(Request $request, $id_order)
    {
        $request->validate([
            'paor_price_buy' => 'required',
            'paor_price_buy.*' => 'required',
        ]);
        $id_order = Main::decrypt($id_order);
        $paor_margin_arr = $request->input('paor_margin');
        $paor_price_buy_arr = $request->input('paor_price_buy');
        $id_user = Session::get('user')['id'];
        $parts_order_cetak = $request->input('parts_order_cetak');

        foreach ($paor_margin_arr as $id_estimate_parts => $paor_margin) {

            $check = mPartsOrder::where('id_estimate_parts', $id_estimate_parts)->count();
            $paor_price_buy = Main::format_number_db($paor_price_buy_arr[$id_estimate_parts]);
            $paor_margin = Main::format_number_db($paor_margin);

            if ($check == 0) {
                $data_insert = [
                    'id_order' => $id_order,
                    'id_user' => $id_user,
                    'id_estimate_parts' => $id_estimate_parts,
                    'paor_price_buy' => $paor_price_buy,
                    'paor_margin' => $paor_margin
                ];
                mPartsOrder::create($data_insert);

            } else {
                $data_update = [
                    'paor_price_buy' => $paor_price_buy,
                    'paor_margin' => $paor_margin
                ];

                mPartsOrder::where('id_estimate_parts', $id_estimate_parts)->update($data_update);
            }
        }

        if ($parts_order_cetak == 'yes') {
            return [
                'redirect' => route('partsOrderPrint', ['id_order' => Main::encrypt($id_order)])
            ];
        }
    }

    function print($id_order)
    {
        $id_order = Main::decrypt($id_order);

        $data['order'] = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $data['parts'] = mEstimateParts
            ::leftJoin('parts_order', 'parts_order.id_estimate_parts', '=', 'estimate_parts.id_estimate_parts')
            ->where('estimate_parts.id_estimate', $data['order']->id_estimate)
            ->orderBy('estimate_parts.id_estimate_parts', 'ASC')
            ->get();

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/parts_order/parts_order_print', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Form Estimasi atau Order');
    }
}
