<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mEstimateParts;
use app\Models\mMechanic;
use app\Models\mOrder;
use app\Models\mQcProcess;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class QcProcess extends Controller
{
    private $menuActive;
    private $step;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['qc_process'];
        $this->step = [
            'qc1', 'qc2', 'qc3', 'qc4', 'qc5', 'qc6', 'qc7', 'qc8', 'qc9',
        ];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Qc Process',
                'route' => route('qcProcessPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "order_number_label"],
            ["data" => "car_police_number"],
            ["data" => "car_brand"],
            ["data" => "car_type"],
            ["data" => "car_color_name"],
            ["data" => "cust_name"],
            ["data" => "parts_count"],
            ["data" => "options"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_order' => $request->id_order,
            'car_police_number' => $request->car_police_number,
            'esti_number_label' => $request->esti_number_label,
            'order_number_label' => $request->order_number_label,
        ));
        return view('bengkel/qc_process/qc_process_list', $data);
    }

    function datatable(Request $request)
    {
        $id_order = $request->id_order;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'order.id_order'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $total_data = mOrder::whereIn('id_order', $id_order_arr)->count();
        } else {
            $total_data = mOrder
                ::count();
        }

        $data = mOrder
            ::leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer');
        if ($id_order) {
            $id_order_arr = explode(',', $id_order);
            $data = $data->whereIn('id_order', $id_order_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;
            $id_order = Main::encrypt($row->id_order);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $row->no = $no;
            $row->parts_count = mEstimateParts::where('id_estimate', $row->id_estimate)->sum('espa_qty');
            $row->options = '
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . route('qcProcessEdit', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Edit
                        </a>
                        <a class="dropdown-item" target="_blank" href="' . route('qcProcessPrint', ['id_order' => $id_order]) . '">
                            <i class="fa fa-bell"></i>
                            Cetak
                        </a>
                    </div>
                </div>
            ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {

        $car_police_number = $request->car_police_number;
        $esti_number_label = $request->esti_number_label;
        $order_number_label = $request->order_number_label;

        $check = mOrder::orderBy('id_order', 'ASC');

        if ($car_police_number) {
            $check = $check->leftJoin('car', 'car.id_car', '=', 'order.id_car')
                ->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($esti_number_label) {
            $check = $check->leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
                ->where('esti_number_label', 'like', '%' . $esti_number_label . '%');
        }

        if ($order_number_label) {
            $check = $check->where('order_number_label', 'like', '%' . $order_number_label . '%');
        }

        if ($check->count() == 1) {
            $id_order = $check->value('id_order');
            $id_order = Main::encrypt($id_order);
            $data = [
                'redirect' => route('qcProcessEdit', ['id_order' => $id_order])
            ];
        } elseif ($check->count() > 1) {
            $id_order = [];
            foreach ($check->get() as $row) {
                $id_order[] = $row->id_order;
            }

            $id_order = implode(",", $id_order);

            $data = [
                'redirect' => route('qcProcessPage', ['id_order' => $id_order, 'car_police_number' => $car_police_number, 'esti_number_label' => $esti_number_label, 'order_number_label' => $order_number_label])];
        } else {
            $data = [
                'redirect' => route('qcProcessPage', [
                    'car_police_number' => $car_police_number,
                    'esti_number_label' => $esti_number_label,
                    'order_number_label' => $order_number_label
                ])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function edit($id_order)
    {
        $breadcrumb = array_merge($this->breadcrumb(), array(
            array(
                'label' => 'Edit QC Process',
                'route' => ''
            )
        ));
        $data = Main::data($breadcrumb, $this->menuActive);

        $id_order = Main::decrypt($id_order);

        $order = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();
        $mechanic = mMechanic::orderBy('mechanic_name', 'ASC')->get();
        $step = $this->step;

        $qc_process = [];
        foreach($step as $langkah) {
            $qc_process[$langkah] = mQcProcess
                ::where('id_order', $id_order)
                ->where('qc_order', $langkah)
                ->first();
        }

        $data = array_merge($data, [
            'mechanic' => $mechanic,
            'step' => $step,
            'order' => $order,
            'qc_process' => $qc_process
        ]);

        return view('bengkel/qc_process/qc_process_edit', $data);
    }

    function update(Request $request, $id_order)
    {
        $request->validate([
            'qc_date' => 'required',
            'qc_date.*' => 'required',
            'qc_time' => 'required',
            'qc_time.*' => 'required',
            'id_mechanic' => 'required',
            'id_mechanic.*' => 'required',
            'qc_notes' => 'required',
        ]);

        $id_order = Main::decrypt($id_order);
        $id_user = Session::get('user')['id'];
        $date_modified = date('Y-m-d H:i:s');
        $id_mechanic_arr = $request->input('id_mechanic');
        $qc_notes = $request->input('qc_notes');
        $qc_date_arr = $request->input('qc_date');
        $qc_time_arr = $request->input('qc_time');
        $qc_process_cetak = $request->input('qc_process_cetak');

        $check = mQcProcess::where('id_order', $id_order)->count();

        if($check == 0) {
            $qc_data = [];
            foreach ($id_mechanic_arr as $qc_order => $id_mechanic) {

                $qc_date = Main::format_date_db($qc_date_arr[$qc_order]);
                $qc_time = $qc_time_arr[$qc_order];

                $qc_data[] = [
                    'id_order' => $id_order,
                    'id_mechanic' => $id_mechanic,
                    'id_user' => $id_user,
                    'qc_order' => $qc_order,
                    'qc_notes' => $qc_notes,
                    'qc_date' => $qc_date,
                    'qc_time' => $qc_time,
                    'created_at' => $date_modified,
                    'updated_at' => $date_modified
                ];
            }

            mQcProcess::insert($qc_data);

        } else {
            foreach ($id_mechanic_arr as $qc_order => $id_mechanic) {

                $qc_date = Main::format_date_db($qc_date_arr[$qc_order]);
                $qc_time = $qc_time_arr[$qc_order];

                $qc_data = [
                    'id_mechanic' => $id_mechanic,
                    'qc_notes' => $qc_notes,
                    'qc_date' => $qc_date,
                    'qc_time' => $qc_time
                ];

                mQcProcess
                    ::where([
                        'qc_order'=>$qc_order,
                        'id_order' => $id_order
                    ])
                    ->update($qc_data);
            }
        }

        if($qc_process_cetak == 'yes') {
            return [
                'redirect' => route('qcProcessPrint', ['id_order' => Main::encrypt($id_order)])
            ];
        }

    }

    function print($id_order)
    {
        $id_order = Main::decrypt($id_order);
        $qc_process = [];
        $data['step'] = $this->step;
        foreach($this->step as $langkah) {
            $qc_process[$langkah] = mQcProcess
                ::leftJoin('mechanic', 'mechanic.id_mechanic', '=', 'qc_process.id_mechanic')
                ->where('qc_process.id_order', $id_order)
                ->where('qc_process.qc_order', $langkah)
                ->first();
        }

        $data['qc_process'] = $qc_process;
        $data['order'] = mOrder
            ::leftJoin('estimate', 'estimate.id_estimate', '=', 'order.id_estimate')
            ->leftJoin('car', 'car.id_car', '=', 'order.id_car')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('order.id_order', $id_order)
            ->first();

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('bengkel/qc_process/qc_process_print', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Form Estimasi atau Order');
    }
}
