<?php

namespace app\Http\Controllers\Bengkel;

use app\Models\mClaim;
use app\Models\mEstimate;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class ClaimProgress extends Controller
{
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['claim_progress'];
    }

    function breadcrumb()
    {
        return [
            [
                'label' => 'Progress Klaim',
                'route' => route('claimProgressPage')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb());
        $datatable_column = [
            ["data" => "no"],
            ["data" => "claim_number_label"],
            ["data" => "car_police_number"],
            ["data" => "car_brand"],
            ["data" => "car_type"],
            ["data" => "cust_name"],
            ["data" => "insu_name"],
            ["data" => "progress"],
            ["data" => "keterangan"],
        ];

        $data = array_merge($data, array(
            'datatable_column' => $datatable_column,
            'id_claim' => $request->id_claim,
            'cust_name' => $request->cust_name,
            'car_police_number' => $request->car_police_number
        ));
        return view('bengkel/claim_progress/claim_progress_list', $data);
    }

    function datatable(Request $request)
    {
        $id_claim = $request->id_claim;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'claim.id_claim'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($id_claim) {
            $id_claim_arr = explode(',', $id_claim);
            $total_data = mClaim::whereIn('id_claim', $id_claim_arr)->count();
        } else {
            $total_data = mClaim
                ::count();
        }

        $data = mClaim
            ::leftJoin('car', 'car.id_car', '=', 'claim.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->leftJoin('insurance', 'insurance.id_insurance', '=', 'car.id_insurance');
        if ($id_claim) {
            $id_claim_arr = explode(',', $id_claim);
            $data = $data->whereIn('id_claim', $id_claim_arr);
        }
        $data = $data
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        foreach ($data as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $progress = 'Kurang Dokumen';

            $check_estimate = mEstimate::where('id_claim', $row->id_claim)->count();
            if ($check_estimate > 0) {
                $id_estimate = mEstimate::where('id_claim', $row->id_claim)->value('id_estimate');
                $progress = 'Estimasi';
                $check_order = mOrder::where('id_estimate', $id_estimate)->count();
                if ($check_order > 0) {
                    $progress = 'Order';
                }
            }


            $row->no = $no;
            $row->progress = $progress;
            $row->keterangan = '
                <textarea class="form-control" name="claim_progress" placeholder="Ketik disini ...">'.$row->claim_progress.'</textarea>
                <div class="text-right">
                    <button type="button" class="btn btn-sm btn-success btn-order-progress" data-id-claim="' . $row->id_claim . '">Simpan</button>
                </div>
                ';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function search(Request $request)
    {
        $request->validate([
            'cust_name' => 'required_without:car_police_number',
            'car_police_number' => 'required_without:cust_name',
        ]);

        $cust_name = $request->cust_name;
        $car_police_number = $request->car_police_number;
        $check = mClaim
            ::leftJoin('car', 'car.id_car', '=', 'claim.id_car')
            ->leftJoin('customer', 'customer.id_customer', '=', 'car.id_customer')
            ->where('cust_name', 'like', '%' . $cust_name . '%');

        if ($request->car_police_number) {
            $check = $check->where('car_police_number', 'like', '%' . $car_police_number . '%');
        }

        if ($check->count() > 0) {
            $id_claim = [];
            foreach ($check->get() as $row) {
                $id_claim[] = $row->id_claim;
            }

            $id_claim = implode(",", $id_claim);

            $data = [
                'redirect' => route('claimProgressPage', ['id_claim' => $id_claim, 'cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        } else {
            $data = [
                'redirect' => route('claimProgressPage', ['cust_name' => $cust_name, 'car_police_number' => $car_police_number])
            ];
        }

//        return Response::json($data, 404);

        return $data;
    }

    function claim_progress_update(Request $request)
    {
        $id_claim = $request->input('id_claim');
        $claim_progress = $request->input('claim_progress');

        mClaim::where('id_claim', $id_claim)->update(['claim_progress'=>$claim_progress]);
    }

}
