<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>{{ $pageTitle }}</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

@include('general/modal_password')
@include('general/modal_roles_user')

<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login">
        <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
            <div class="m-login__container">
                <div class="m-portlet m-portlet-login">
                    <div class="m-login__logo">
                        <a href="#">
                            <h1>SISTEM BENGKEL</h1>
                        </a>
                    </div>
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h3 class="m-login__title">Isi form dibawah untuk masuk ke halaman Level User</h3>
{{--                            <br />--}}
{{--                            <h5 align="center">Peran Komputer : <span class="role-name"></span></h5>--}}
{{--                            <h3 class="m-login__title">Did you <a href="javascript;;">forget your password ?</a></h3>--}}
                        </div>
                        <form class="m-login__form m-form form-send" action="{{ route('loginDo') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group m-form__group">
                                <input class="form-control m-input" type="text" placeholder="Username" name="username" autocomplete="off">
                            </div>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input" type="password" placeholder="Password" name="password">
                            </div>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input" type="hidden" placeholder="Roles User" name="id_user_role">
                            </div>
                            <div class="m-login__form-action">
                                <button type="submit" id="m_login_signin_submit" class="btn m-btn m-btn--custom m-btn--login">Sign In</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


{{--<button class="btn-login-roles" data-toggle="modal" data-target="#modal-roles-login"><i class="fa fa-users"></i></button>--}}


<!-- end:: Page -->

<!--begin::Global Theme Bundle -->
<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Scripts -->
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>
