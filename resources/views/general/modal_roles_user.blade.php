<div class="modal" id="modal-roles-list" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pilih Roles Dibawah untuk merubah </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group">
                        <label class="form-control-label required">Roles User</label>
                        <select class="form-control m-select2" name="id_user_role_list">
                            @foreach($roles as $row)
                                <option value="{{ $row->id }}">{{ $row->role_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-user-role-change">Ganti Roles Komputer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
