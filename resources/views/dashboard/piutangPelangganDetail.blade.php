<h4>Detail Data Hutang</h4>
<div class="m-form__section m-form__section--first">
    <div class="form-group m-form__group row">
        <label class="col-lg-3 col-form-label">No Hutang Pelanggan</label>
        <div class="col-lg-3 col-form-label">
            : {{ $piutang_pelanggan->no_piutang_pelanggan }}
        </div>
        <label class="col-lg-3 col-form-label">Jatuh Tempo</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_date($piutang_pelanggan->pp_jatuh_tempo) }}
        </div>
        <label class="col-lg-3 col-form-label">No Faktur Pembelian</label>
        <div class="col-lg-3 col-form-label">
            : {{ $piutang_pelanggan->pp_no_faktur }}
        </div>
        <label class="col-lg-3 col-form-label">Jumlah</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_money($piutang_pelanggan->pp_amount) }}
        </div>
        <label class="col-lg-3 col-form-label">Tanggal Piutang</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_date($piutang_pelanggan->tanggal_piutang) }}
        </div>
        <label class="col-lg-3 col-form-label">Keterangan</label>
        <div class="col-lg-3 col-form-label">
            : {{ $piutang_pelanggan->pp_keterangan }}
        </div>
    </div>
</div>
<br/>
<hr/>
<br/>
<h4>Detail Data Pembelian Produk</h4>
<div class="m-form__section m-form__section--first">
    <div class="form-group m-form__group row">
        <label class="col-lg-3 col-form-label">No Faktur Pembelian</label>
        <div class="col-lg-3 col-form-label">
            : {{ $penjualan->no_faktur }}
        </div>
        <label class="col-lg-3 col-form-label">Tanggal Pembelian</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_date($penjualan->tanggal) }}
        </div>
        <label class="col-lg-3 col-form-label">Pengiriman Produk</label>
        <div class="col-lg-3 col-form-label">
            : {!! Main::distributor_pengiriman($penjualan->pengiriman) !!}
        </div>
        <label class="col-lg-3 col-form-label">Jenis Ekspedisi</label>
        <div class="col-lg-3 col-form-label">
            : {{ $penjualan->ekspedisi['nama_ekspedisi'] }}
        </div>
        <label class="col-lg-3 col-form-label">Total Pembelian</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_money($penjualan->total) }}
        </div>
        <label class="col-lg-3 col-form-label">Biaya Tambahan</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_money($penjualan->biaya_tambahan) }}
        </div>
        <label class="col-lg-3 col-form-label">Potongan Akhir</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_money($penjualan->potongan_akhir) }}
        </div>
        <label class="col-lg-3 col-form-label">Grand Total</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_money($penjualan->grand_total) }}
        </div>
        <label class="col-lg-3 col-form-label">Terbayar</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_money($penjualan->terbayar) }}
        </div>
        <label class="col-lg-3 col-form-label">Sisa Pembayaran</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_money($penjualan->sisa_pembayaran) }}
        </div>
        <label class="col-lg-3 col-form-label">Kembalian</label>
        <div class="col-lg-3 col-form-label">
            : {{ Main::format_money($penjualan->kembalian) }}
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-bordered m-table m-table--head-bg-success datatable-no-order ">
        <thead>
        <tr>
            <th>No</th>
            <th>Nama Produk</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Potongan</th>
            <th>PPN Persentase</th>
            <th>PPN Nominal</th>
            <th>Harga Net</th>
            <th>Sub Total</th>
        </tr>
        </thead>
        <tbody>
        @php
            $total_qty = 0;
            $total_harga = 0;
            $total_potongan = 0;
            $total_ppn = 0;
            $total_harga_net = 0;
            $total_sub_total = 0;
        @endphp
        @foreach($penjualan_produk as $r)
            @php
                $total_qty += $r->qty;
                $total_harga += $r->harga;
                $total_potongan += $r->potongan;
                $total_ppn += $r->ppn_nominal;
                $total_harga_net += $r->harga_net;
                $total_sub_total += $r->sub_total;
            @endphp
            <tr>
                <th scope="row">{{ $no++ }}</th>
                <td>{{ '('.$r->produk->kode_produk.') '.$r->produk->nama_produk }}</td>
                <td align="right">{{ Main::format_number($r->qty) }}</td>
                <td align="right">{{ Main::format_money($r->harga) }}</td>
                <td align="right">{{ Main::format_money($r->potongan) }}</td>
                <td align="center">{{ ($r->ppn_persen*100).'%' }}</td>
                <td align="right">{{ Main::format_money($r->ppn_nominal) }}</td>
                <td align="right">{{ Main::format_money($r->harga_net) }}</td>
                <td align="right">{{ Main::format_money($r->sub_total) }}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="2" class="text-center">
                    <strong>Total</strong>
                </th>
                <th class="text-right">{{ Main::format_number($total_qty) }}</th>
                <th class="text-right">{{ Main::format_money($total_harga) }}</th>
                <th class="text-right">{{ Main::format_money($total_potongan) }}</th>
                <th class="text-right"></th>
                <th class="text-right">{{ Main::format_money($total_ppn) }}</th>
                <th class="text-right">{{ Main::format_money($total_harga_net) }}</th>
                <th class="text-right">{{ Main::format_money($total_sub_total) }}</th>
            </tr>
        </tfoot>
    </table>
</div>