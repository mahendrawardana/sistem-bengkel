@extends('../general/index')

@section('css')

@endsection

@section('js')

@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">

                    <div class="row detail-info">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">
                                    Nama Pelanggan
                                </label>
                                <div class="col-9">
                                    {{ $car->customer->cust_name }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">
                                    Alamat Pelanggan
                                </label>
                                <div class="col-9">
                                    {{ $car->customer->cust_address }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">
                                    No Telp Pelanggan
                                </label>
                                <div class="col-9">
                                    {{ $car->customer->cust_phone }}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">
                                    No Polisi
                                </label>
                                <div class="col-8">
                                    {{ $car->car_police_number }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">
                                    Tipe Mobil
                                </label>
                                <div class="col-8">
                                    {{ $car->car_type }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">
                                    Warna
                                </label>
                                <div class="col-8">
                                    {{ $car->car_color_name }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">
                                    Tahun Produksi
                                </label>
                                <div class="col-8">
                                    {{ $car->car_production_year }}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">
                                    Nama Asuransi
                                </label>
                                <div class="col-8">
                                    {{ $car->insurance->insu_name }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">
                                    No Polis
                                </label>
                                <div class="col-8">
                                    {{ $car->insurance->insu_name }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">
                                    Tipe
                                </label>
                                <div class="col-8">
                                    {{ $car->insurance->insu_type }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">
                                    Expire Date
                                </label>
                                <div class="col-8">
                                    {{ $car->insurance->insu_expire_date }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <hr/>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 text-center">
                            <a href="{{ route("customerDetailPage", ['id_car'=>Main::encrypt($car->id_car)]) }}"
                               class="btn-produk-add btn btn-primary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-eye"></i>
                                        <span>Edit/View</span>
                                    </span>
                            </a>
                            <a href="{{ route("claimDocumentCreate", ['id_car'=>Main::encrypt($car->id_car)]) }}"
                               class="btn-produk-add btn btn-success m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-file-text"></i>
                                        <span>Buat Klaim Asuransi</span>
                                    </span>
                            </a>
                            <a href="{{ route("estimationFormCreate", ['id_car' => Main::encrypt($car->id_car)]) }}"
                               class="btn-produk-add btn btn-info m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-check"></i>
                                        <span>Buat Estimasi</span>
                                    </span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection