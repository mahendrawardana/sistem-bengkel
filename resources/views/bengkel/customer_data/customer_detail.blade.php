@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/produksi.js') }}"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#" data-target="#kt_tabs_1_1"><h5>1. Data
                                Pemilik</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_1_2"><h5>2. Data
                                Pelanggan</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3"><h5>3. Data Mobil</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_4"><h5>4. Data Asuransi</h5></a>
                    </li>
                </ul>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Nama Pemilik
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_owner->caro_name }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Alamat Pemilik
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_owner->caro_address }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Kota Pemilik
                                            </label>
                                            <div class="col-4">
                                                {{ $car->car_owner->caro_city }}
                                            </div>
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Kode POS
                                            </label>
                                            <div class="col-3">
                                                {{ $car->car_owner->caro_postal_code }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                No Telp Pemilik
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_owner->caro_phone }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">

                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Tanggal Lahir
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_owner->caro_birthday }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Pekerjaan
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_owner->caro_job }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Hobi
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_owner->caro_hobby }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                No KTP
                                            </label>
                                            <div class="col-9">

                                                {{ $car->car_owner->caro_ktp_number }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Foto KTP
                                            </label>
                                            <div class="col-9">
                                                <img src="{{ Main::file_url('car_owner', $car->car_owner->caro_ktp_picture) }}" width="300">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">

                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Nama Pelanggan
                                            </label>
                                            <div class="col-9">
                                                {{ $car->customer->cust_name }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Alamat Pelanggan
                                            </label>
                                            <div class="col-9">
                                                {{ $car->customer->cust_address }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Kota Pelanggan
                                            </label>
                                            <div class="col-4">
                                                {{ $car->customer->cust_city }}
                                            </div>
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Kode POS
                                            </label>
                                            <div class="col-3">
                                                {{ $car->customer->cust_postal_code }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                No Telp
                                            </label>
                                            <div class="col-9">
                                                {{ $car->customer->cust_phone }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">

                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Tanggal Lahir
                                            </label>
                                            <div class="col-9">
                                                {{ $car->customer->cust_birthday }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Pekerjaan
                                            </label>
                                            <div class="col-9">
                                                {{ $car->customer->cust_job }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Hobi
                                            </label>
                                            <div class="col-9">
                                                {{ $car->customer->cust_hobby }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                No KTP
                                            </label>
                                            <div class="col-9">
                                                {{ $car->customer->cust_ktp_number }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Foto KTP
                                            </label>
                                            <div class="col-9">
                                                <img src="{{ Main::file_url('customer', $car->customer->cust_ktp_picture) }}" width="300">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_3" role="tabpanel">
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Merk
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_brand }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Tipe
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_type }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Warna
                                            </label>
                                            <div class="col-4">
                                                {{ $car->car_color_name }}
                                            </div>
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Kode Warna
                                            </label>
                                            <div class="col-3">
                                                {{ $car->car_color_code }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Tahun Produksi
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_production_year }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">

                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Nomer Mesin
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_machine_number }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Nomer Rangka
                                            </label>
                                            <div class="col-9">
                                                {{ $car->car_chassis_number }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Foto STNK
                                            </label>
                                            <div class="col-9">
                                                <img src="{{ Main::file_url('car', $car->car_stnk_picture) }}" width="300">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_4" role="tabpanel">
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Nama Asuransi
                                            </label>
                                            <div class="col-9">
                                                {{ $car->insurance->insu_name }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Alamat Asuransi
                                            </label>
                                            <div class="col-9">
                                                {{ $car->insurance->insu_address }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Kota
                                            </label>
                                            <div class="col-4">
                                                {{ $car->insurance->insu_city }}
                                            </div>
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Kode POS
                                            </label>
                                            <div class="col-3">
                                                {{ $car->insurance->insu_postal_code }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                No Polis
                                            </label>
                                            <div class="col-9">
                                                {{ $car->insurance->insu_policy_number }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Tipe
                                            </label>
                                            <div class="col-9">
                                                {{ $car->insurance->insu_type }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Expire Date
                                            </label>
                                            <div class="col-9">
                                                {{ $car->insurance->insu_expire_date }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Nilai O.R
                                            </label>
                                            <div class="col-9">
                                                {{ $car->insurance->insu_or_value }}
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label">
                                                Foto KTP
                                            </label>
                                            <div class="col-9">
                                                <img src="{{ Main::file_url('insurance', $car->insurance->insu_ktp_picture) }}" width="300">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <hr/>
                        <br/>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 text-center">
                                <a href="{{ route("customerEditPage", ['id_car'=>Main::encrypt($car->id_car)]) }}"
                                   class="btn-produk-add btn btn-primary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-edit"></i>
                                        <span>Edit</span>
                                    </span>
                                </a>

                                <a href="{{ route("customerExistPage", ['id_car' => Main::encrypt($car->id_car)]) }}"
                                   class="btn-produk-add btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-angle-double-left"></i>
                                        <span>Kembali</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

@endsection