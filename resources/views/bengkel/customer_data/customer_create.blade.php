@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('customerInsert') }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('customerDataPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true"
            enctype="multipart/form-data">
                {{ csrf_field() }}
                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#" data-target="#kt_tabs_1_1">
                            <h5>1. Data Pemilik</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_1_2">
                            <h5>2. Data Pelanggan</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3">
                            <h5>3. Data Mobil</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_4">
                            <h5>4. Data Asuransi</h5>
                        </a>
                    </li>
                </ul>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Nama
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="caro_name">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Alamat
                                            </label>
                                            <div class="col-9">
                                                <textarea class="form-control" rows="3" name="caro_address"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Kota
                                            </label>
                                            <div class="col-4">
                                                <input type="text" class="form-control" name="caro_city">
                                            </div>
                                            <label for="example-text-input" class="col-2 col-form-label font-weight-bold required">
                                                Kode POS
                                            </label>
                                            <div class="col-3">
                                                <input type="text" class="form-control" name="caro_postal_code">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                No Telp
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="caro_phone">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">

                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Tanggal Lahir
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control m_datepicker_1_modal" name="caro_birthday">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Pekerjaan
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="caro_job">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Hobi
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="caro_hobby">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                No KTP
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="caro_ktp_number">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Foto KTP
                                            </label>
                                            <div class="col-9">
                                                <div></div>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input image-preview-browse" name="caro_ktp_picture" id="customFile">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                                <br />
                                                <br />
                                                <img src="" class="image-preview" width="300">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Nama
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="cust_name">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Alamat
                                            </label>
                                            <div class="col-9">
                                                <textarea class="form-control" rows="3" name="cust_address"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Kota
                                            </label>
                                            <div class="col-4">
                                                <input type="text" class="form-control" name="cust_city">
                                            </div>
                                            <label for="example-text-input" class="col-2 col-form-label font-weight-bold required">
                                                Kode POS
                                            </label>
                                            <div class="col-3">
                                                <input type="text" class="form-control" name="cust_postal_code">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                No Telp
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="cust_phone">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">

                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Tanggal Lahir
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control m_datepicker_1_modal" name="cust_birthday">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Pekerjaan
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="cust_job">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Hobi
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="cust_hobby">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                No KTP
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="cust_ktp_number">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Foto KTP
                                            </label>
                                            <div class="col-9">
                                                <div></div>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input image-preview-browse" name="cust_ktp_picture" id="customFile">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                                <br />
                                                <br />
                                                <img src="" class="image-preview" width="300">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_3" role="tabpanel">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Nomer Polisi
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="car_police_number">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Merk
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="car_brand">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Tipe
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="car_type">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Warna
                                            </label>
                                            <div class="col-4">
                                                <input type="text" class="form-control" name="car_color_name">
                                            </div>
                                            <label for="example-text-input" class="col-2 col-form-label font-weight-bold required">
                                                Kode Warna
                                            </label>
                                            <div class="col-3">
                                                <input type="text" class="form-control" name="car_color_code">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Tahun Produksi
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="car_production_year">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Nomer Mesin
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="car_machine_number">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Nomer Rangka
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="car_chassis_number">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Foto STNK
                                            </label>
                                            <div class="col-9">
                                                <div></div>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input image-preview-browse" name="car_stnk_picture" id="customFile">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                                <br />
                                                <br />
                                                <img src="" class="image-preview" width="300">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_4" role="tabpanel">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Nama Asuransi
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="insu_name">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Alamat Asuransi
                                            </label>
                                            <div class="col-9">
                                                <textarea class="form-control" rows="3" name="insu_address"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Kota
                                            </label>
                                            <div class="col-4">
                                                <input type="text" class="form-control" name="insu_city">
                                            </div>
                                            <label for="example-text-input" class="col-2 col-form-label font-weight-bold required">
                                                Kode POS
                                            </label>
                                            <div class="col-3">
                                                <input type="text" class="form-control" name="insu_postal_code">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                No Polis
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="insu_policy_number">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Tipe
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="insu_type">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Expire Date
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control m_datepicker_1_modal" name="insu_expire_date">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Nilai O.R
                                            </label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="insu_or_value">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-3 col-form-label required">
                                                Foto KTP
                                            </label>
                                            <div class="col-9">
                                                <div></div>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input image-preview-browse" name="insu_ktp_picture" id="customFile">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                                <br />
                                                <br />
                                                <img src="" class="image-preview" width="300">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </button>

                    <a href="{{ route("customerDataPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Data Pelanggan</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection