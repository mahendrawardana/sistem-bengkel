@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">


            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" action=""
                      class="form-send1 m-form m-form--fit m-form--label-align-right">
                    {{ csrf_field() }}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-1 col-sm-12">Nomor Polisi</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="car_police_number"
                                       value="{{ $car_police_number }}">
                            </div>
                            <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Nomor Estimasi</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="esti_number_label"
                                       value="{{ $esti_number_label }}">
                            </div>
                            <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Nomor Order</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="order_number_label"
                                       value="{{ $order_number_label }}">
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <button type="submit" class="btn btn-accent btn-sm btn-search">
                            <i class="la la-search"></i> Cari Data
                        </button>
                        <a href="{{ route('jobProgressPage') }}" class="btn btn-info btn-sm btn-search">
                            <i class="la la-random"></i> Reset
                        </a>
                        <button type="button" class="btn-job-progress-print btn btn-primary btn-sm">
                            <i class="la la-print"></i> Cetak Job Progress Board
                        </button>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive table-job-progress">
                        <table class="datatable table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>No Polisi</th>
                                <th>Type</th>
                                <th>Warna</th>
                                <th></th>
                                @foreach($type_arr as $row)
                                    <th class="text-center font-weight-bold text-uppercase">{{ str_replace('_', ' ', $row) }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order as $key => $row)
                                @php
                                    $no = $key + 1;
                                    $plan_data = \app\Models\mWorkPlan
                                    ::where('id_order', $row->id_order)
                                    ->whereIn('work_plan_type', $type_arr)
                                    ->get();
                                    $progress_data = \app\Models\mWorkProgress
                                    ::where('id_order', $row->id_order)
                                    ->whereIn('work_progress_type', $type_arr)
                                    ->get();
                                    $plan_arr = [];
                                    $progress_arr = [];

                                    foreach($type_arr as $type) {
                                        $plan_arr[$type] = ['work_plan_date'=>''];
                                        $progress_arr[$type] = ['work_progress_date'=>''];
                                    }
                                    foreach($plan_data as $plan) {
                                        $plan_arr[$plan->work_plan_type] = $plan;
                                    }
                                    foreach($progress_data as $plan) {
                                        $progress_arr[$plan->work_progress_type] = $plan;
                                    }

                                @endphp
                                <tr>
                                    <td rowspan="2">{{ $no }}.</td>
                                    <td rowspan="2">{{ $row->car_police_number }}</td>
                                    <td rowspan="2">{{ $row->car_type }}</td>
                                    <td rowspan="2">{{ $row->car_color_name }}</td>
                                    <td class="font-weight-bold text-uppercase text-center">Plan</td>
                                    @foreach($type_arr as $type)
                                        <td class="text-center font-weight-bold text-uppercase">
                                            {!! $plan_arr[$type]['work_plan_date']  ? '<img src="'.asset('images/check.png').'">':'' !!}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td class="font-weight-bold text-uppercase text-center"><strong>Actual</strong></td>
                                    @foreach($type_arr as $type)
                                        <td class="text-center font-weight-bold text-uppercase">
                                            {!! $progress_arr[$type]['work_progress_date']  ? '<img src="'.asset('images/check.png').'">':'' !!}
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <form class="form-job-progress-cetak m--hide" method="post" action="{{ route('jobProgressPrint') }}"
          target="_blank">
        {{ csrf_field() }}
        <textarea name="content"></textarea>
    </form>

@endsection
