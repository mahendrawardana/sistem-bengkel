@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/produksi.js') }}"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('produksiInsert') }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('produksiPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Nomer Dokumen
                                    </label>
                                    <div class="col-8">
                                        100-DOK-01
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        100-ORD-01
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <br />

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Nama
                                    </label>
                                    <div class="col-8">
                                        DWI APRIANTO W.Y.
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Alamat
                                    </label>
                                    <div class="col-8">
                                        PERUM GRAHA DEWATA IRAWADI NO. 6 JL. TUKAD IRAWADI, PANJER, DENPASAR
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        No Telp
                                    </label>
                                    <div class="col-8">
                                        +6281 5791 4082
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        DK 1234 ABC
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        S-CROSS
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        Putih
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        2014
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        PAG
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        No Polis
                                    </label>
                                    <div class="col-8">
                                        918-28371
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Tipe
                                    </label>
                                    <div class="col-8">
                                        BTC
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label required">
                                        Expire Date
                                    </label>
                                    <div class="col-8">
                                        2021
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#" data-target="#kt_tabs_1_1"><h5>1. Biaya Jasa</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_1_2"><h5>2. Biaya Parts</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3"><h5>3. Sublet</h5></a>
                    </li>
                </ul>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Panel</th>
                                        <th>Repaire/Replace</th>
                                        <th>Price List</th>
                                        <th>Disc</th>
                                        <th>Harga</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>BUMPER DEPAN</td>
                                        <td>Repair</td>
                                        <td>800,000</td>
                                        <td>50,000</td>
                                        <td>750,000</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>FENDER DEPAN</td>
                                        <td>Replace</td>
                                        <td>400,000</td>
                                        <td>50,000</td>
                                        <td>350,000</td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="3" class="text-center">Total Jasa</th>
                                        <th>1,200,000</th>
                                        <th>100,000</th>
                                        <th>1,100,000</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
                                <div class="table-responsive">
                                <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Parts</th>
                                        <th>Nomer Parts</th>
                                        <th>Jumlah</th>
                                        <th>Price List</th>
                                        <th>Discount</th>
                                        <th>Harga</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Kopling Set Kampas</td>
                                        <td>PRT-10001</td>
                                        <td>1</td>
                                        <td>620,000</td>
                                        <td>20,000</td>
                                        <td>600,000</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Remote Steer </td>
                                        <td>PRT-10002</td>
                                        <td>1</td>
                                        <td>750,000</td>
                                        <td>30,000</td>
                                        <td>720,000</td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="3" class="text-center">Total Parts</th>
                                        <th>2</th>
                                        <th>1,370,000</th>
                                        <th>50,000</th>
                                        <th>1,320,000</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_3" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Pekerjaan</th>
                                        <th>Jumlah</th>
                                        <th>Price List</th>
                                        <th>Discount</th>
                                        <th>Harga</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Perbaikan Kaki Mobil</td>
                                        <td>1</td>
                                        <td>350,000</td>
                                        <td>10000</td>
                                        <td>340,000</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Perbaikan Mesin</td>
                                        <td>1</td>
                                        <td>1,150,000</td>
                                        <td>50,000</td>
                                        <td>1,100,000</td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="2" class="text-center">Total Sublet</th>
                                        <th>2</th>
                                        <th>1,500,000</th>
                                        <th>60,000</th>
                                        <th>1,440,000</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-4 offset-md-8">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-5 col-form-label required">
                                                Total Estimasi Biaya
                                            </label>
                                            <div class="col-7">
                                                3,860,000
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-5 col-form-label required">
                                                Estimasi Selesai
                                            </label>
                                            <div class="col-7">
                                                10 Hari
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <a href="{{ route('orderFormPage') }}"
                       class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </a>

                    <a href="{{ route("orderFormPrint") }}" target="_blank"
                       class="btn-produk-add btn btn-info btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Order</span>
                        </span>
                    </a>

                    <a href="{{ route("estimationFormPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar Data</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection