@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/estimation.js') }}"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <form action="{{ route('orderValidationUpdate', ['id_order' => Main::encrypt($order->id_order)]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('orderValidationPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">

                {{ csrf_field() }}
                <input type="hidden" name="order_validation_cetak" value="no">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                @if($estimate->id_claim)
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label ">
                                            Nomer Klaim
                                        </label>
                                        <div class="col-8">
                                            {{ $estimate->claim_number_label }}
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Nomer Estimasi
                                    </label>
                                    <div class="col-8">
                                        {{ $estimate->esti_number_label }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        {{ $order->order_number_label }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Alamat Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_address }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Telp Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_phone }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_color_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polis
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_policy_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tipe
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Expire Date
                                    </label>
                                    <div class="col-8">
                                        {{ Main::format_date($row->insurance->insu_expire_date) }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#" data-target="#kt_tabs_1_1"><h5>1. Biaya
                                Jasa</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_1_2"><h5>2. Biaya
                                Parts</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3"><h5>3. Sublet</h5></a>
                    </li>
                </ul>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-jasa">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Panel</th>
                                        <th>Repaire/Replace</th>
                                        <th>Price List</th>
                                        <th>Diskon</th>
                                        <th>Harga</th>
                                        <th class="text-center">Valid</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $eser_price_item_total = 0;
                                        $eser_discount_total = 0;
                                    @endphp
                                    @foreach($estimate->estimate_services as $key => $row)
                                        @php
                                            $no = $key + 1;
                                            $eser_price_item_total += $row->eser_price_item;
                                            $eser_discount_total += $row->eser_discount;
                                        @endphp
                                        <input type="hidden" name="id_estimate_services[]" value="{{ $row->id_estimate_services }}">
                                        <tr>
                                            <td>{{ $no }}.</td>
                                            <td>{{ $row->eser_name }}</td>
                                            <td>{{ $row->eser_status }}</td>
                                            <td>{{ Main::format_number($row->eser_price_item) }}</td>
                                            <td>{{ Main::format_number($row->eser_discount) }}</td>
                                            <td>{{ Main::format_number($row->eser_price_end) }}</td>
                                            <td class="text-center">
                                            <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                                <label>
                                                    <input
                                                            type="checkbox"
                                                            name="eser_valid[{{ $row->id_estimate_services }}]"
                                                            value="yes"
                                                            data-on-text="normal"
                                                            data-off-text="abnormal"
                                                            {{ $row->eser_valid == 'yes' ? 'checked':'' }}
                                                    >
                                                    <span></span>
                                                </label>
                                            </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="3" class="text-center">Total Jasa</th>
                                        <th>{{ Main::format_number($eser_price_item_total) }}</th>
                                        <th>{{ Main::format_number($eser_discount_total) }}</th>
                                        <th>{{ Main::format_number($estimate->esti_price_total_services) }}</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
                                <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-parts">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Parts</th>
                                        <th>Nomer Parts</th>
                                        <th>Jumlah</th>
                                        <th>Price List</th>
                                        <th>Discount</th>
                                        <th>Harga</th>
                                        <th class="text-center">Valid</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $espa_qty_total = 0;
                                    @endphp
                                    @foreach($estimate->estimate_parts as $key => $row)
                                        @php
                                            $no = $key + 1;
                                            $espa_qty_total += $row->espa_qty;
                                        @endphp
                                        <input type="hidden" name="id_estimate_parts[]" value="{{ $row->id_estimate_parts }}">
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $row->espa_name }}</td>
                                            <td>{{ $row->espa_number }}</td>
                                            <td>{{ Main::format_number($row->espa_qty) }}</td>
                                            <td>{{ Main::format_number($row->espa_price_item) }}</td>
                                            <td>{{ Main::format_number($row->espa_discount) }}</td>
                                            <td>{{ Main::format_number($row->espa_price_end) }}</td>
                                            <td class="text-center">
                                            <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                                <label>
                                                    <input
                                                            type="checkbox"
                                                            name="espa_valid[{{ $row->id_estimate_parts }}]"
                                                            value="yes"
                                                            data-on-text="normal"
                                                            data-off-text="abnormal"
                                                            {{ $row->espa_valid == 'yes' ? 'checked':'' }}
                                                    >
                                                    <span></span>
                                                </label>
                                            </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="3" class="text-center">Total Parts</th>
                                        <th>{{ Main::format_number($espa_qty_total) }}</th>
                                        <th></th>
                                        <th></th>
                                        <th>{{ Main::format_number($estimate->esti_price_total_parts) }}</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_3" role="tabpanel">
                                <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-sublet">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Pekerjaan</th>
                                        <th>Jumlah</th>
                                        <th>Price List</th>
                                        <th>Discount</th>
                                        <th>Harga</th>
                                        <th class="text-center">Valid</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $esub_qty_total = 0;
                                    @endphp
                                    @foreach($estimate->estimate_sublet as $key => $row)
                                        @php
                                            $no = $key + 1;
                                            $esub_qty_total += $row->esub_qty;
                                        @endphp
                                        <input type="hidden" name="id_estimate_sublet[]" value="{{ $row->id_estimate_sublet }}">
                                        <tr class="sublet-row">
                                            <td>{{ $no }}.</td>
                                            <td>{{ $row->esub_name }}</td>
                                            <td>{{ Main::format_number($row->esub_qty) }}</td>
                                            <td>{{ Main::format_number($row->esub_price_item) }}</td>
                                            <td>{{ Main::format_number($row->esub_discount) }}</td>
                                            <td>{{ Main::format_number($row->esub_price_end) }}</td>
                                            <td class="text-center">
                                            <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                                <label>
                                                    <input
                                                            type="checkbox"
                                                            name="esub_valid[{{ $row->id_estimate_sublet }}]"
                                                            value="yes"
                                                            data-on-text="normal"
                                                            data-off-text="abnormal"
                                                            {{ $row->esub_valid == 'yes' ? 'checked':'' }}>
                                                    <span></span>
                                                </label>
                                            </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="2" class="text-center">Total Sublet</th>
                                        <th>{{ Main::format_number($esub_qty_total) }}</th>
                                        <th></th>
                                        <th></th>
                                        <th>{{ Main::format_number($estimate->esti_price_total_sublet) }}</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-4 offset-md-8">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-5 col-form-label ">
                                                Total Estimasi Biaya
                                            </label>
                                            <div class="col-7">
                                                {{ Main::format_number($estimate->esti_price_total) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-5 col-form-label ">
                                                Estimasi Selesai
                                            </label>
                                            <div class="col-7">
                                                {{ $estimate->esti_day }} Hari
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </button>
                    <button type="submit"
                       class="btn-submit-cetak-order-validation btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Invoice</span>
                        </span>
                    </button>
                    <a href="{{ route("orderValidationPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection