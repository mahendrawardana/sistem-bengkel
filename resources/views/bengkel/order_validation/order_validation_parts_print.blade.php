<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }
        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }
        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }

        .total-biaya {
            font-size: 12px;
            font-weight: bold;
        }


    </style>
    <?php
        $width_label = '64';
    ?>

</head>
<body>
<h1 align="center">FORM PARTS SLIP</h1>
<hr/>

<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Parts Slip</td>
            <td width="4">:</td>
            <td>192381-PRT-001</td>
        </tr>
        <tr>
            <td>Nama Pemilik</td>
            <td>:</td>
            <td>DWI APRIANTO W.Y.</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td>PERUM GRAHA DEWATA IRAWADI NO. 6 JL. TUKAD IRAWADI, PANJER, DENPASAR</td>
        </tr>
        <tr>
            <td>No Telp</td>
            <td>:</td>
            <td>+6281 5791 4082</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Invoice</td>
            <td width="4">:</td>
            <td>192381-INV-001</td>
        </tr>
        <tr>
            <td>No. Polisi</td>
            <td>:</td>
            <td>DK 123 ABC</td>
        </tr>
        <tr>
            <td>Type</td>
            <td>:</td>
            <td>S-CROSS</td>
        </tr>
        <tr>
            <td>Warna</td>
            <td>:</td>
            <td>Putih</td>
        </tr>
        <tr>
            <td>Tahun Produksi</td>
            <td>:</td>
            <td>2015</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Order</td>
            <td width="4">:</td>
            <td>100-ORD-01</td>
        </tr>
        <tr>
            <td>Nama Asuransi</td>
            <td>:</td>
            <td>PAG</td>
        </tr>
        <tr>
            <td>No Polis</td>
            <td>:</td>
            <td>918-28371</td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td>:</td>
            <td>BTC</td>
        </tr>
        <tr>
            <td>Expire Date</td>
            <td>:</td>
            <td>2021</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>

<br />
<br />

<h2 style="margin-bottom: 0">Biaya Parts</h2>
<table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
    <thead>
    <tr>
        <th width="10">No</th>
        <th width="150">Nama Parts</th>
        <th>Nomer Parts</th>
        <th>Jumlah</th>
        <th>Price List</th>
        <th>Discount</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="text-center">1</td>
        <td>Kopling Set Kampas</td>
        <td>PRT-10001</td>
        <td class="text-right">1</td>
        <td class="text-right">620,000</td>
        <td class="text-right">20,000</td>
        <td class="text-right">600,000</td>
    </tr>
    <tr>
        <td class="text-center">2</td>
        <td>Remote Steer</td>
        <td>PRT-10002</td>
        <td class="text-right">1</td>
        <td class="text-right">750,000</td>
        <td class="text-right">30,000</td>
        <td class="text-right">720,000</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="3" class="text-center">Total Spare Parts</th>
        <th class="text-right">2</th>
        <th class="text-right">1,370,000</th>
        <th class="text-right">50,000</th>
        <th class="text-right">1,320,000</th>
    </tr>
    </tfoot>
</table>
<br />
<br />
<br />
<div class="div-2"></div>
<div class="div-2 text-center">
    Denpasar, <?php echo date('d') ?> Agustus 2020
    <br />
    <br />
    <br />
    <br />
    <br />
    <u>{{ Session::get('user')['karyawan']['nama_karyawan'] }}</u><br />
    Kepala Bengkel
</div>
<div class="clearfix"></div>