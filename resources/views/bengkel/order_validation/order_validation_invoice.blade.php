<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }
        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }
        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }

        .total-biaya {
            font-size: 12px;
            font-weight: bold;
        }


    </style>
    <?php
        $width_label = '64';
    ?>

</head>
<body>
<h1 align="center">FORM INVOICE</h1>
<hr/>

<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Invoice</td>
            <td width="4">:</td>
            <td>{{ $detail->invoice_number_label }}</td>
        </tr>
        <tr>
            <td>Nama Pelanggan</td>
            <td>:</td>
            <td>{{ $detail->cust_name }}</td>
        </tr>
        <tr>
            <td>Alamat Pelanggan</td>
            <td>:</td>
            <td>{{ $detail->cust_address }}</td>
        </tr>
        <tr>
            <td>No Telp</td>
            <td>:</td>
            <td>{{ $detail->cust_phone }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Order</td>
            <td width="4">:</td>
            <td>{{ $detail->order_number_label }}</td>
        </tr>
        <tr>
            <td>No. Polisi</td>
            <td>:</td>
            <td>{{ $detail->car_police_number }}</td>
        </tr>
        <tr>
            <td>Type</td>
            <td>:</td>
            <td>{{ $detail->car_type }}</td>
        </tr>
        <tr>
            <td>Warna</td>
            <td>:</td>
            <td>{{ $detail->car_color_name }}</td>
        </tr>
        <tr>
            <td>Tahun Produksi</td>
            <td>:</td>
            <td>{{ $detail->car_production_year }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">&nbsp;</td>
            <td width="4"></td>
            <td></td>
        </tr>
        <tr>
            <td>Nama Asuransi</td>
            <td>:</td>
            <td>{{ $detail->insu_name }}</td>
        </tr>
        <tr>
            <td>No Polis</td>
            <td>:</td>
            <td>{{ $detail->insu_policy_name }}</td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td>:</td>
            <td>{{ $detail->insu_type }}</td>
        </tr>
        <tr>
            <td>Expire Date</td>
            <td>:</td>
            <td>{{ $detail->insu_expire_date }}</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>

<br />
<br />

<h2>1. Biaya Jasa</h2>
<table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
    <thead>
    <tr>
        <th width="10">No</th>
        <th>Nama Panel</th>
        <th>Repaire/Replace</th>
        <th>Price List</th>
        <th>Disc</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @foreach($services as $key => $row)
        @php
            $services_total += $row->eser_price_end;
        @endphp
    <tr>
        <td class="text-center">{{ ++$key }}.</td>
        <td>{{ $row->eser_name }}</td>
        <td class="text-center">{{ ucwords($row->eser_status) }}</td>
        <td class="text-right">{{ Main::format_number($row->eser_price_item) }}</td>
        <td class="text-right">{{ Main::format_number($row->eser_discount) }}</td>
        <td class="text-right">{{ Main::format_number($row->eser_price_end) }}</td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="5" class="text-center">TOTAL JASA</th>
        <th class="text-right">{{ Main::format_number($services_total) }}</th>
    </tr>
    </tfoot>
</table>
<h2 style="margin-bottom: 0">2. Biaya Parts</h2>
<strong>No. Parts Slip</strong> : {{ $detail->parts_number_label }}
<table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
    <thead>
    <tr>
        <th width="10">No</th>
        <th width="150">Nama Parts</th>
        <th>Nomer Parts</th>
        <th>Jumlah</th>
        <th>Price List</th>
        <th>Discount</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @foreach($parts as $key => $row)
        @php
            $parts_total += $row->espa_price_end;
        @endphp
    <tr>
        <td class="text-center">{{ ++$key }}.</td>
        <td>{{ $row->espa_name }}</td>
        <td>{{ $row->espa_number }}</td>
        <td class="text-center">{{ Main::format_number($row->espa_qty) }}</td>
        <td class="text-right">{{ Main::format_number($row->espa_price_item) }}</td>
        <td class="text-right">{{ Main::format_number($row->espa_discount) }}</td>
        <td class="text-right">{{ Main::format_number($row->espa_price_end) }}</td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="6" class="text-center">TOTAL SPARE PARTS</th>
        <th class="text-right">{{ Main::format_number($parts_total) }}</th>
    </tr>
    </tfoot>
</table>
<h2 style="margin-bottom: 0">3. Biaya Sublet</h2>
<strong>No. Sublet Slip</strong> : {{ $detail->sublet_number_label }}
<table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
    <thead>
    <tr>
        <th width="10">No</th>
        <th>Nama Pekerjaan</th>
        <th>Jumlah</th>
        <th>Price List</th>
        <th>Discount</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sublet as $key => $row)
        @php
            $sublet_total += $row->esub_price_end;
        @endphp
    <tr>
        <td class="text-center">{{ ++$key }}.</td>
        <td>{{ $row->esub_name }}</td>
        <td class="text-center">{{ $row->esub_qty }}</td>
        <td class="text-right">{{ Main::format_number($row->esub_price_item) }}</td>
        <td class="text-right">{{ Main::format_number($row->esub_discount) }}</td>
        <td class="text-right">{{ Main::format_number($row->esub_price_end) }}</td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="5" class="text-center">TOTAL SUBLET</th>
        <th class="text-right">{{ Main::format_number($sublet_total) }}</th>
    </tr>
    </tfoot>
</table>
<br />
<table width="100%" class="total-biaya">
    <tr>
        <td width="50%"></td>
        <td width="30%"><strong>Total Biaya Jasa</strong></td>
        <td width="10">:</td>
        <td width="20%" class="text-right">{{ Main::format_number($services_total) }}</td>
    </tr>
    <tr>
        <td></td>
        <td><strong>Total Biaya Parts</strong></td>
        <td>:</td>
        <td class="text-right">{{ Main::format_number($parts_total) }}</td>
    </tr>
    <tr>
        <td></td>
        <td><strong>Total Biaya Sublet</strong></td>
        <td>:</td>
        <td class="text-right">{{ Main::format_number($sublet_total) }}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><strong>Total Biaya Perbaikan</strong></td>
        <td>:</td>
        <td class="text-right"><strong>{{ Main::format_number($services_total + $parts_total + $sublet_total) }}</strong></td>
    </tr>
</table>

<div class="clearfix"></div>
<br />
<br />
<br />
<div class="div-2"></div>
<div class="div-2 text-center">
    Denpasar, {{ date('d F Y') }}
    <br />
    <br />
    <br />
    <br />
    <br />
    <u>{{ Session::get('user')['karyawan']['nama_karyawan'] }}</u><br />
    Kepala Bengkel
</div>
<div class="clearfix"></div>