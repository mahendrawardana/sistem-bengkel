@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/produksi.js') }}"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('produksiInsert') }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('produksiPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('orderValidationJasaPage') }}">
                            <h5>1. Biaya Jasa</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('orderValidationPartsPage') }}">
                            <h5>2. Biaya Parts</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('orderValidationSubletPage') }}">
                            <h5>3. Sublet</h5>
                        </a>
                    </li>
                </ul>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Nomer Dokumen
                                    </label>
                                    <div class="col-8">
                                        100-DOK-01
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        100-ORD-01
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Nomer Slip Part
                                    </label>
                                    <div class="col-8">
                                        100-PRT-01
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Nama
                                    </label>
                                    <div class="col-8">
                                        DWI APRIANTO W.Y.
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Alamat
                                    </label>
                                    <div class="col-8">
                                        PERUM GRAHA DEWATA IRAWADI NO. 6 JL. TUKAD IRAWADI, PANJER, DENPASAR
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        No Telp
                                    </label>
                                    <div class="col-8">
                                        +6281 5791 4082
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        DK 1234 ABC
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        S-CROSS
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        Putih
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        2014
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        PAG
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        No Polis
                                    </label>
                                    <div class="col-8">
                                        918-28371
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Tipe
                                    </label>
                                    <div class="col-8">
                                        BTC
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label ">
                                        Expire Date
                                    </label>
                                    <div class="col-8">
                                        2021
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                        <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Nama Parts</th>
                                <th>Nomer Parts</th>
                                <th>Jumlah</th>
                                <th>Price List</th>
                                <th>Discount</th>
                                <th>Harga</th>
                                <th width="50">Valid</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Kopling Set Kampas</td>
                                <td>PRT-10001</td>
                                <td class="text-right">1</td>
                                <td class="text-right">620,000</td>
                                <td class="text-right">20,000</td>
                                <td class="text-right">600,000</td>
                                <td>
                                    <label class="m-checkbox m-checkbox--success">
                                        <input type="checkbox">
                                        <span></span>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Remote Steer</td>
                                <td>PRT-10002</td>
                                <td class="text-right">1</td>
                                <td class="text-right">750,000</td>
                                <td class="text-right">30,000</td>
                                <td class="text-right">720,000</td>
                                <td>
                                    <label class="m-checkbox m-checkbox--success">
                                        <input type="checkbox">
                                        <span></span>
                                    </label>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" class="text-center">Total Parts</th>
                                <th class="text-right">2</th>
                                <th class="text-right">1,370,000</th>
                                <th class="text-right">50,000</th>
                                <th class="text-right">1,320,000</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-4 offset-md-8">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-5 col-form-label ">
                                                Total Biaya
                                            </label>
                                            <div class="col-7">
                                                3,860,000
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <a href="{{ route('orderValidationPartsPage') }}"
                       class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </a>

                    <a href="{{ route("orderValidationPartsPrint") }}" target="_blank"
                       class="btn-produk-add btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Slip Parts</span>
                        </span>
                    </a>

                    <a href="{{ route("orderValidationPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection