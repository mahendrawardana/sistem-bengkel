@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <style type="text/css">
        .select2-selection__clear {
            display: none !important;
        }
    </style>

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                orientation: "bottom left",
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

            $('.datepicker').change(function () {
                var date = $(this).val();
                var step = $(this).data('step');
                $.ajax({
                    url: "{{ route('workPlanFormatDay') }}",
                    type: 'post',
                    data: {
                        _token: "{{ csrf_token() }}",
                        date: date
                    },
                    success: function (data) {
                        $(".day-" + step).html(data);
                    }
                })
            });
        });
    </script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('qcProcessUpdate', ['id_order' => Main::encrypt($order->id_order)]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('qcProcessPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <input type="hidden" name="qc_process_cetak" value="no">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Estimasi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->esti_number_label }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        {{ $order->order_number_label }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $order->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->insu_name }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_color_name }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td rowspan="2"></td>
                                    <td class="text-center font-weight" colspan="3"><strong>SURFACER</strong></td>
                                    <td class="text-center font-weight" colspan="3"><strong>SPRAYING</strong></td>
                                    <td class="text-center font-weight" colspan="3"><strong>FINISHING</strong></td>
                                    <td rowspan="2" class="text-center font-weight" valign="middle">
                                        <strong>CATATAN</strong></td>
                                </tr>
                                <tr>
                                    @foreach($step as $step_row)
                                        <td class="text-center">
                                            <strong>{{ strtoupper($step_row) }}</strong>
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <strong>Hari</strong>
                                    </td>
                                    @foreach($step as $step_row)
                                        <td class="text-center day-{{$step_row}}">{{ $qc_process[$step_row]['qc_date'] ? Main::format_day($qc_process[$step_row]['qc_date']) : '' }}</td>
                                    @endforeach
                                    <td rowspan="4"><textarea class="form-control" rows="11" name="qc_notes">{{ $qc_process[$step[0]]['qc_notes'] }}</textarea></td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <strong>Tanggal</strong>
                                    </td>
                                    @foreach($step as $step_row)
                                        <td>
                                            <input type="text" class="form-control datepicker"
                                                   name="qc_date[{{$step_row}}]" style="width: 120px"
                                                   value="{{ $qc_process[$step_row]['qc_date'] ? date('d-m-Y', strtotime($qc_process[$step_row]['qc_date'])) : '' }}"
                                                   readonly
                                                   data-step="{{ $step_row }}"
                                                   placeholder="Pilih Tanggal"/>
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <strong>Jam</strong>
                                    </td>
                                    @foreach($step as $step_row)
                                        <td>
                                            <input class="form-control m-timepicker"
                                                   name="qc_time[{{ $step_row }}]" style="width: 120px"
                                                   readonly
                                                   value="{{ $qc_process[$step_row]['qc_time'] }}"
                                                   placeholder="Pilih Jam" type="text"/>
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td class="text-center"><strong>Mekanik</strong></td>
                                    @foreach($step as $step_row)
                                        <td>
                                            <select class="form-control m-select2" name="id_mechanic[{{ $step_row }}]">
                                                @foreach($mechanic as $row)
                                                    <option value="{{ $row->id_mechanic }}" {{ $qc_process[$step_row]['id_mechanic'] == $row->id_mechanic ? 'selected':'' }}>{{ $row->mechanic_name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                       class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </button>

                    <button type="submit"
                       class="btn-submit-cetak-qc-process btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak QC Process</span>
                        </span>
                    </button>

                    <a href="{{ route("qcProcessPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection