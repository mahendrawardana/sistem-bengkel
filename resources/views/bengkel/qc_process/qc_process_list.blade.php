@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="post" action="{{ route('qcProcessSearch') }}"
                      class="form-send m-form m-form--fit m-form--label-align-right">
                    {{ csrf_field() }}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-1 col-sm-12">Nomor Polisi</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="car_police_number" value="{{ $car_police_number }}">
                            </div>
                            <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Nomor Estimasi</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="esti_number_label"
                                       value="{{ $esti_number_label }}">
                            </div>
                            <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Nomor Order</label>
                            <div class="col-lg-3 col-md-9 col-sm-12">
                                <input type="text" class="form-control" name="order_number_label"
                                       value="{{ $order_number_label }}">
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <button type="submit" class="btn btn-accent btn-sm btn-search">
                            <i class="la la-search"></i> Cari Data
                        </button>
                        <a href="{{ route('qcProcessPage') }}" class="btn btn-info btn-sm btn-search">
                            <i class="la la-random"></i> Reset
                        </a>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="table table-bordered datatable-new"
                               data-url="{{ route('qcProcessDataTable',['id_order' => $id_order]) }}"
                               data-column="{{ json_encode($datatable_column) }}">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Nomer Order</th>
                                <th>No Polisi</th>
                                <th>Merk</th>
                                <th>Type</th>
                                <th>Warna</th>
                                <th>Nama Pelanggan</th>
                                <th>Jumlah Panel</th>
                                <th width="60">Menu</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
