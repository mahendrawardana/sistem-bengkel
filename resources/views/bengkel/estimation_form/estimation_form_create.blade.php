@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/estimation.js') }}"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('estimationFormInsert') }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('estimationFormPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <input type="hidden" name="id_car" value="{{ $id_car }}">
                <input type="hidden" name="id_claim" value="{{ $id_claim }}">
                <input type="hidden" name="esti_price_total_services" value="0">
                <input type="hidden" name="esti_price_total_parts" value="0">
                <input type="hidden" name="esti_price_total_sublet" value="0">
                <input type="hidden" name="esti_price_total" value="0">
                <input type="hidden" name="estimate_cetak" value="no">
                <input type="hidden" name="order_cetak" value="no">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                @if($id_claim)
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label ">
                                            Nomer Dokumen
                                        </label>
                                        <div class="col-8">
                                            {{ $claim->claim_number_label }}
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-xs-12 col-md-4">
                                {{--                                <div class="form-group m-form__group row">--}}
                                {{--                                    <label for="example-text-input" class="col-4 col-form-label ">--}}
                                {{--                                        Nomer Order--}}
                                {{--                                    </label>--}}
                                {{--                                    <div class="col-8">--}}
                                {{--                                        100-ORD-01--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                            </div>
                            <div class="col-xs-12 col-md-2 offset-md-2">
                                <div class="m-radio-list">
                                    <label class="m-radio m-radio--check-bold m-radio--state-success">
                                        <input type="radio" name="esti_status" value="add"
                                               {{ $claim && $claim->claim_status == 'add' ? 'checked':'' }}> Tambahan
                                        Klaim
                                        <span></span>
                                    </label>
                                    <label class="m-radio m-radio--check-bold m-radio--state-brand">
                                        <input type="radio" name="esti_status" value="revision"
                                               {{ $claim && $claim->claim_status == 'revision' ? 'checked':'' }}> Revisi
                                        Klaim
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Alamat Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_address }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Telp Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_phone }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_color_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polis
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_policy_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tipe
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Expire Date
                                    </label>
                                    <div class="col-8">
                                        {{ Main::format_date($row->insurance->insu_expire_date) }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#" data-target="#kt_tabs_1_1"><h5>1. Biaya
                                Jasa</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_1_2"><h5>2. Biaya
                                Parts</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3"><h5>3. Sublet</h5></a>
                    </li>
                </ul>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-jasa">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Panel</th>
                                        <th>Repaire/Replace</th>
                                        <th>Price List</th>
                                        <th>Diskon</th>
                                        <th>Harga</th>
                                        <th width="50">Menu</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="7">
                                            <button type="button" class="btn btn-success btn-jasa-add"><i
                                                        class="la la-plus"></i> Tambah Biaya Jasa
                                            </button>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="3" class="text-center">Total Jasa</th>
                                        <th class="eser-price-item-total">0</th>
                                        <th class="eser-discount-total">0</th>
                                        <th class="esti-price-total-services">0</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
                                <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-parts">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Parts</th>
                                        <th>Nomer Parts</th>
                                        <th>Jumlah</th>
                                        <th>Price List</th>
                                        <th>Discount</th>
                                        <th>Harga</th>
                                        <th width="50">Menu</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="8">
                                            <button type="button" class="btn btn-success btn-parts-add">
                                                <i class="la la-plus"></i> Tambah Biaya Parts
                                            </button>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="3" class="text-center">Total Parts</th>
                                        <th class="espa-qty-total">0</th>
                                        <th class="espa-price-item-total1"></th>
                                        <th class="espa-discount-total1"></th>
                                        <th class="esti-price-total-parts">0</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_3" role="tabpanel">
                                <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-sublet">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Nama Pekerjaan</th>
                                        <th>Jumlah</th>
                                        <th>Price List</th>
                                        <th>Discount</th>
                                        <th>Harga</th>
                                        <th width="50">Menu</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th colspan="7">
                                            <button type="button" class="btn btn-success btn-sublet-add"><i
                                                        class="la la-plus"></i> Tambah Biaya Sublet
                                            </button>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="text-center">Total Sublet</th>
                                        <th class="esub-qty-total">0</th>
                                        <th class="esub-price-item-total1"></th>
                                        <th class="esub-discount-total1"></th>
                                        <th class="esti-price-total-sublet">0</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-4 offset-md-8">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-5 col-form-label ">
                                                Total Estimasi Biaya
                                            </label>
                                            <div class="col-7 esti-price-total">
                                                0
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-5 col-form-label ">
                                                Estimasi Selesai
                                            </label>
                                            <div class="col-7">
                                                <div class="input-group">
                                                    <input type="text" class="form-control m-input" value="3"
                                                           placeholder="esti_day" aria-describedby="basic-addon2">
                                                    <div class="input-group-append"><span class="input-group-text"
                                                                                          id="basic-addon2">Hari</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                            class="btn-submit-simpan-estimasi btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan Estimasi</span>
                        </span>
                    </button>

                    <button type="submit"
                       class="btn-submit-cetak-estimasi btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Estimasi</span>
                        </span>
                    </button>

                    <button type="submit"
                       class="btn-submit-cetak-order btn btn-info btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Order</span>
                        </span>
                    </button>

                    <a href="{{ route("estimationFormPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar Data</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>


    <table class="m--hide table-jasa-row">
        <tbody>
        <tr class="jasa-row">
            <td class="td-jasa-data m--hide">
                <input type="hidden" name="eser_price_end[]" value="0">
            </td>
            <td class="jasa-no">1</td>
            <td><input type="text" class="form-control" name="eser_name[]" required></td>
            <td class="jasa-eser-status">
                <div class="m-radio-list">
                    <label class="m-radio m-radio--state-success">
                        <input type="radio" class="eser-status" name="eser_status[]" value="repair" checked>
                        Repair
                        <span></span>
                    </label>
                    <label class="m-radio m-radio--state-brand">
                        <input type="radio" class="eser-status" name="eser_status[]" value="replace">
                        Replace
                        <span></span>
                    </label>
                </div>
            </td>
            <td class="eser-price-item">
                <input type="text" class="form-control input-numeral" name="eser_price_item[]"
                       value="0" required>
            </td>
            <td class="eser-discount">
                <input type="text" class="form-control input-numeral" name="eser_discount[]"
                       value="0" required>
            </td>
            <td class="eser-price-end">0</td>
            <td>
                <button type="button" class="btn btn-danger btn-sm btn-jasa-hapus">Hapus</button>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="m--hide table-parts-row">
        <tbody>
        <tr class="parts-row">
            <td class="td-parts-data m--hide">
                <input type="hidden" name="espa_price_end[]" value="0">
            </td>
            <td class="parts-no">1</td>
            <td>
                <textarea class="form-control" name="espa_name[]" required></textarea>
            </td>
            <td class="espa-number"><input type="text" class="form-control input-numeral" name="espa_number[]" required>
            </td>
            <td class="espa-qty"><input type="text" class="form-control input-numeral" name="espa_qty[]" value="0"></td>
            <td class="espa-price-item"><input type="text" class="form-control input-numeral" name="espa_price_item[]"
                                               value="0"></td>
            <td class="espa-discount"><input type="text" class="form-control input-numeral" name="espa_discount[]"
                                             value="0"></td>
            <td class="espa-price-end">0</td>
            <td>
                <button type="button" class="btn btn-danger btn-sm btn-parts-hapus">Hapus</button>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="m--hide table-sublet-row">
        <tbody>
        <tr class="sublet-row">
            <td class="td-sublet-data m--hide">
                <input type="hidden" name="esub_price_end[]" value="0">
            </td>
            <td class="sublet-no">1</td>
            <td>
                <textarea class="form-control" name="esub_name[]"></textarea>
            </td>
            <td class="esub-qty"><input type="text" class="form-control input-numeral" name="esub_qty[]" value="0"></td>
            <td class="esub-price-item"><input type="text" class="form-control input-numeral" name="esub_price_item[]"
                                               value="0"></td>
            <td class="esub-discount"><input type="text" class="form-control input-numeral" name="esub_discount[]"
                                             value="0"></td>
            <td class="esub-price-end">0</td>
            <td>
                <button type="button" class="btn btn-danger btn-sm btn-sublet-hapus">Hapus</button>
            </td>
        </tr>
        </tbody>
    </table>

@endsection