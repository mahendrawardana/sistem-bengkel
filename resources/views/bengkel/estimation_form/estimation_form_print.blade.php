<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }

        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }


    </style>
    <?php
    $width_label = '64';
    ?>

</head>
<body>
<h1 align="center">FORM ESTIMASI</h1>
<hr/>

<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">No Klaim</td>
            <td width="4">:</td>
            <td>{{ $estimate->claim_number_label }}</td>
        </tr>
        <tr>
            <td>Nama Plg</td>
            <td>:</td>
            <td>{{ $row->customer->cust_name }}</td>
        </tr>
        <tr>
            <td>Alamat Plg</td>
            <td>:</td>
            <td>{{ $row->customer->cust_address }}</td>
        </tr>
        <tr>
            <td>No. Telp</td>
            <td>:</td>
            <td>{{ $row->customer->cust_phone }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Estimasi</td>
            <td width="4">:</td>
            <td>{{ $estimate->esti_number_label }}</td>
        </tr>
        <tr>
            <td>No. Polisi</td>
            <td>:</td>
            <td>{{ $row->car_police_number }}</td>
        </tr>
        <tr>
            <td>Type</td>
            <td>:</td>
            <td>{{ $row->car_type }}</td>
        </tr>
        <tr>
            <td>Warna</td>
            <td>:</td>
            <td>{{ $row->car_color_name }}</td>
        </tr>
        <tr>
            <td>Tahun Produksi</td>
            <td>:</td>
            <td>{{ $row->car_production_year }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Status</td>
            <td width="4">:</td>
            <td>{{ $estimate->claim_status == 'add' ? 'Tambahan Klaim':'Revisi Klaim' }}</td>
        </tr>
        <tr>
            <td>Nama Asuransi</td>
            <td>:</td>
            <td>{{ $row->insurance->insu_name }}</td>
        </tr>
        <tr>
            <td>No Polis</td>
            <td>:</td>
            <td>{{ $row->insurance->insu_policy_number }}</td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td>:</td>
            <td>{{ $row->insurance->insu_type }}</td>
        </tr>
        <tr>
            <td>Expire Date</td>
            <td>:</td>
            <td>{{ Main::format_date($row->insurance->insu_expire_date) }}</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>

<br/>
<br/>

<h2>1. Biaya Jasa</h2>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="10">No</th>
        <th>Nama Panel</th>
        <th>Repair/Service</th>
        <th>Price List</th>
        <th>Disc</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @php
        $eser_price_item_total = 0;
        $eser_discount_total = 0;
    @endphp
    @foreach($estimate->estimate_services as $key => $row_data)
        @php
            $eser_price_item_total += $row_data->eser_price_item;
            $eser_discount_total += $row_data->eser_discount;
        @endphp
        <tr>
            <td class="number">{{ ++$key }}.</td>
            <td>{{ $row_data->eser_name }}</td>
            <td>{{ ucwords($row_data->eser_status) }}</td>
            <td class="number">{{ Main::format_number($row_data->eser_price_item) }}</td>
            <td class="number">{{ Main::format_number($row_data->eser_discount) }}</td>
            <td class="number">{{ Main::format_number($row_data->eser_price_end) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="3">Total Jasa</th>
        <th class="number">{{ Main::format_number($eser_price_item_total) }}</th>
        <th class="number">{{ Main::format_number($eser_discount_total) }}</th>
        <th class="number">{{ Main::format_number($estimate->esti_price_total_services) }}</th>
    </tr>
    </tfoot>
</table>

<h2>2. Biaya Parts</h2>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="10">No</th>
        <th>Nama Parts</th>
        <th>Nomor Parts</th>
        <th>Jumlah</th>
        <th>Price List</th>
        <th>Disc</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @php
        $espa_qty_total = 0;
    @endphp
    @foreach($estimate->estimate_parts as $key => $row_data)
        @php
            $espa_qty_total += $row_data->espa_qty;
        @endphp
        <tr>
            <td class="number">{{ ++$key }}.</td>
            <td>{{ $row_data->espa_name }}</td>
            <td>{{ $row_data->espa_number }}</td>
            <td class="number">{{ Main::format_number($row_data->espa_qty) }}</td>
            <td class="number">{{ Main::format_number($row_data->espa_prite_item) }}</td>
            <td class="number">{{ Main::format_number($row_data->espa_discount) }}</td>
            <td class="number">{{ Main::format_number($row_data->espa_price_end) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="3">Total Parts</th>
        <th class="number">{{ Main::format_number($espa_qty_total) }}</th>
        <th class="number"></th>
        <th class="number"></th>
        <th class="number">{{ Main::format_number($estimate->esti_price_total_parts) }}</th>
    </tr>
    </tfoot>
</table>

<h2>3. Sublet</h2>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="10">No</th>
        <th>Nama Pekerjaan</th>
        <th>Jumlah</th>
        <th>Price List</th>
        <th>Disc</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @foreach($estimate->estimate_sublet as $key => $row_data)
        <tr>
            <td class="number">{{ ++$key }}.</td>
            <td>{{ $row_data->esub_name }}</td>
            <td class="number">{{ Main::format_number($row_data->esub_qty) }}</td>
            <td class="number">{{ Main::format_number($row_data->esub_price_item) }}</td>
            <td class="number">{{ Main::format_number($row_data->esub_discount) }}</td>
            <td class="number">{{ Main::format_number($row_data->esub_price_end) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="5">Total Sublet</th>
        <th class="number">{{ Main::format_number($estimate->esti_price_total_sublet) }}</th>
    </tr>
    </tfoot>
</table>

<h2>4. Total</h2>
<table class="table table-bordered">
    <tr>
        <td width="20%">Total Estimasi Biaya</td>
        <td width="25%" class="number">{{ Main::format_number($estimate->esti_price_total) }}</td>
        <td width="10%"></td>
        <td width="20%">Estimasi Selesai</td>
        <td width="25%" class="number">{{ $estimate->esti_day }} Hari</td>
    </tr>
</table>
<br/>
<br/>
<br/>
<div class="div-2 text-center">
    Menyetujui
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <u>{{ $row->customer->cust_name }}</u><br/>
    Pelanggan
</div>
<div class="div-2 text-center">
    Denpasar, <?php echo date('d F Y') ?>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <u>{{ Session::get('user')['karyawan']['nama_karyawan'] }}</u><br/>
    Service Advisor
</div>
<div class="clearfix"></div>