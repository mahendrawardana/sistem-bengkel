<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }

        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }


    </style>
    <?php
    $width_label = '64';
    ?>

</head>
<body>
<h1 align="center"><strong>PERNYATAAN KEPUASAN PELANGGAN</strong></h1>
<h1 align="center"><i>CUSTOMER SATISFACTION STATEMENT</i></h1>
<hr/>

<strong>Dengan ini Saya/Kami, yang bertanda tangan dibawah ini:</strong><br/>
<i>I/We hereby undersigned:</i>

<table width="100%">
    <tbody>
    <tr>
        <td width="30%">
            <div><strong>Nama</strong></div>
            <div><i>Name</i></div>
        </td>
        <td valign="top" width="10">:</td>
        <td align="left" valign="top">{{ $order->cust_name }}</td>
    </tr>
    <tr>
        <td width="30%">
            <div><strong>Nomor Polis</strong></div>
            <div><i>Policy No.</i></div>
        </td>
        <td valign="top">:</td>
        <td align="left" valign="top">{{ $order->insu_policy_number }}</td>
    </tr>
    <tr>
        <td width="30%">
            <div><strong>Merk Kendaraan</strong></div>
            <div><i>Vehicle Brand</i></div>
        </td>
        <td valign="top">:</td>
        <td align="left" valign="top">{{ $order->car_brand }}</td>
    </tr>
    <tr>
        <td width="30%">
            <div><strong>Nomor Kendaraan</strong></div>
            <div><i>Vehicle No.</i></div>
        </td>
        <td valign="top">:</td>
        <td align="left" valign="top">{{ $order->car_police_number }}</td>
    </tr>
    </tbody>
</table>
<br/>
<strong>Telah mengajukan klaim kepada PT Asuransi Jasindo sebagai berikut:</strong><br/>
<i>Has filed a claim to PT Asuransi Jasindo, as follow:</i>
<table width="100%">
    <tbody>
    <tr>
        <td width="30%">
            <div><strong>Nomor Klaim</strong></div>
            <div><i>Claim No.</i></div>
        </td>
        <td valign="top" width="10">:</td>
        <td align="left" valign="top">{{ $order->claim_number_label }}</td>
    </tr>
    <tr>
        <td width="30%">
            <div><strong>No. Order :</strong></div>
            <div><i>Order No.</i></div>
        </td>
        <td valign="top">:</td>
        <td align="left" valign="top">{{ $order->order_number_label }}</td>
    </tr>
    </tbody>
</table>
<br/>
<strong>yang telah diperbaiki sesuai dengan kerusakan pada klaim yang saya ajukan di :</strong><br/>
<i>Has filed a claim to {{ $order->insu_name }}, as follow:</i>
<table width="100%">
    <tbody>
    <tr>
        <td width="30%">
            <div><strong>Nama Bengkel</strong></div>
            <div><i>Refair Shop Name</i></div>
        </td>
        <td valign="top" width="10">:</td>
        <td align="left" valign="top"></td>
    </tr>
    <tr>
        <td width="30%">
            <div><strong>Alamat Bengkel</strong></div>
            <div><i>Refair Shop Address</i></div>
        </td>
        <td valign="top">:</td>
        <td align="left" valign="top"></td>
    </tr>
    </tbody>
</table>
<br />
<strong>Saya/Kami menyatakan puas terhadap hasil pengerjaan perbaikan bengkel yang telah mengembalikan kondisi kendaraan saya.
    Seperti sesaat sebelum terjadinya kerugian yang telah saya laporkan.</strong><br />
<i>I/We hereby declare satisfied with the refaur work result which has restored my vehicle Condition as it was prior to the loss tha I reported.</i>


<br/>
<br/>

<br/>
<br/>
<br/>
<div class="div-2 text-center">
    <div style="color: white">Denpasar ?></div>
    <div><strong>Tanda Tangan Tertanggung,</strong></div>
    <i>Insured Tertanggung</i>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <u>{{ $order->cust_name }}</u><br/>
    <strong>Pelanggan</strong>
</div>
<div class="div-2 text-center">
    Denpasar, <?php echo date('d F Y') ?>
    <div><strong>Saksi,</strong></div>
    <i>Witnessed by</i>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <u>{{ Session::get('user')['karyawan']['nama_karyawan'] }}</u><br/>
    <strong>Bengkel</strong><br />
    <i>(Refair Shop)</i>
</div>
<div class="clearfix"></div>