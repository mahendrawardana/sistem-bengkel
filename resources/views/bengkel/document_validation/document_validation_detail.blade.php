@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.min.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <?php
    $select_date = '
            <input type="text" class="form-control" id="m_datepicker_1" readonly placeholder="Pilih Tanggal" />
';

    $select_time = '
        <input class="form-control m-timepicker" readonly placeholder="Pilih Jam" type="text" />
    ';

    $select_mechanic = '
                <select class="form-control m-select2" name="">
                    <option value="1">Mahendra</option>
                    <option value="2">Adi</option>
                    <option value="3">Wardana</option>
                    <option value="4">Kusuma</option>
                </select>
            ';

    ?>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('produksiInsert') }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('produksiPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}


                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        {{ $order->order_number_label }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $order->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Alamat Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $order->cust_address }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Telepon Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $order->cust_phone }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tipe Mobil
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna Mobil
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_color_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->insu_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polis
                                    </label>
                                    <div class="col-8">
                                        {{ $order->insu_policy_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tipe
                                    </label>
                                    <div class="col-8">
                                        {{ $order->insu_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Expire Date
                                    </label>
                                    <div class="col-8">
                                        {{ Main::format_date($order->insu_expire_date) }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 offset-lg-411">

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th>Judul</th>
                                            <th>Tanggal Upload</th>
                                            <th width="70">Menu</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox"
                                                           disabled {{ $police_insurance ? 'checked' : '' }}> 1.
                                                    Fotocopy Polis Asuransi
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $police_insurance[0]->updated_at }}</td>
                                            <td>
                                                @if($police_insurance)
                                                    <button type="button" class="btn btn-info btn-sm"
                                                            data-toggle="modal" data-target="#modal-police-insurance">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox" disabled {{ $damage ? 'checked' : '' }}> 2.
                                                    Foto Kerusakan
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $damage[0]->updated_at }}</td>
                                            <td>
                                                @if($damage)
                                                    <button type="button" class="btn btn-info btn-sm"
                                                            data-toggle="modal" data-target="#modal-damage">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox"
                                                           disabled {{ $drive_license ? 'checked' : '' }}> 3. Fotocopy
                                                    SIM
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $drive_license[0]->updated_at }}</td>
                                            <td>
                                                @if($drive_license)
                                                    <button type="button" class="btn btn-info btn-sm"
                                                            data-toggle="modal" data-target="#modal-drive-license">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox"
                                                           disabled {{ $police_report ? 'checked' : '' }}> 4. Laporan
                                                    Kepolisian
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $police_report[0]->updated_at }}</td>
                                            <td>
                                                @if($police_report)
                                                    <button type="button" class="btn btn-info btn-sm"
                                                            data-toggle="modal" data-target="#modal-police-report">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>5.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox"
                                                           disabled {{ $events_chronology ? 'checked' : '' }}> 5.
                                                    Kronologi Kejadian
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $events_chronology[0]->updated_at }}</td>
                                            <td>
                                                @if($events_chronology)
                                                    <button type="button" class="btn btn-info btn-sm"
                                                            data-toggle="modal" data-target="#modal-events-chronology">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>6.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox" disabled {{ $loss_report ? 'checked' : '' }}>
                                                    6. Form Laporan Kerugian
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $loss_report[0]->updated_at }}</td>
                                            <td>
                                                @if($loss_report)
                                                    <button type="button" class="btn btn-info btn-sm"
                                                            data-toggle="modal" data-target="#modal-loss-report">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>7.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox"
                                                           disabled {{ $work_progress_file && $work_progress_file->wopf_epoxy ? 'checked' : '' }}>
                                                    7. Fotocopy Epoxy
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $work_progress_file->updated_at }}</td>
                                            <td>
                                                @if($work_progress_file && $work_progress_file->wopf_epoxy)
                                                    <a href="{{ asset('upload/wopf_epoxy/'.$work_progress_file->wopf_epoxy) }}" target="_blank" class="btn btn-info btn-sm">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>8.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox"
                                                           disabled {{ $work_progress_file && $work_progress_file->wopf_parts_lama ? 'checked' : '' }}>
                                                    8. Foto Parts Lama
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $work_progress_file->updated_at }}</td>
                                            <td>
                                                @if($work_progress_file && $work_progress_file->wopf_parts_lama)
                                                    <a href="{{ asset('upload/wopf_parts_lama/'.$work_progress_file->wopf_parts_lama) }}" target="_blank" class="btn btn-info btn-sm">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>9.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox"
                                                           disabled {{ $work_progress_file && $work_progress_file->wopf_parts_baru ? 'checked' : '' }}>
                                                    9. Foto Parts Baru
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $work_progress_file->updated_at }}</td>
                                            <td>
                                                @if($work_progress_file && $work_progress_file->wopf_parts_baru)
                                                    <a href="{{ asset('upload/wopf_parts_baru/'.$work_progress_file->wopf_parts_baru) }}" target="_blank" class="btn btn-info btn-sm">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>10.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox"
                                                           disabled {{ $work_progress_file && $work_progress_file->wopf_hasil_pekerjaan ? 'checked' : '' }}>
                                                    10. Foto Hasil Pekerjaan
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $work_progress_file->updated_at }}</td>
                                            <td>
                                                @if($work_progress_file && $work_progress_file->wopf_hasil_pekerjaan)
                                                    <a href="{{ asset('upload/wopf_hasil_pekerjaan/'.$work_progress_file->wopf_hasil_pekerjaan) }}" target="_blank" class="btn btn-info btn-sm">
                                                        <i class="la la-print"></i> Mulai Cetak
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>11.</td>
                                            <td>
                                                <label class="m-checkbox m-checkbox--bold m-checkbox--success">
                                                    <input type="checkbox" checked disabled> 11. Surat Puas Pelanggan
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td></td>
                                            <td>
                                                <a href="{{ route('documentValidationSuratPuas', ['id_order' => Main::encrypt($order->id_order)]) }}" target="_blank" class="btn btn-info btn-sm">
                                                    <i class="la la-print"></i> Mulai Cetak
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                <!--                    <a href="{{ route('documentValidationPage') }}"
                       class="btn btn-primary btn-lg m-btn m-btn&#45;&#45;custom m-btn&#45;&#45;pill m-btn&#45;&#45;icon m-btn&#45;&#45;air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </a>-->

                    <a href="{{ route("documentValidationPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="modal" id="modal-police-insurance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        1. Fotocopy Polis Asuransi
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @foreach($police_insurance as $row)
                            <div class="col-lg-4 text-center">
                                <img src="{{ asset('upload/claim_policy_insurance/'.$row->cfile_filename) }}" class="img-thumbnail img-responsive img-fluid">
                                <br />
                                <br />
                                <a href="{{ asset('upload/claim_policy_insurance/'.$row->cfile_filename) }}" target="_blank" class="btn btn-success">Cetak File</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal-damage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        2. Foto Kerusakan
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @foreach($damage as $row)
                            <div class="col-lg-4 text-center">
                                <img src="{{ asset('upload/claim_damage/'.$row->cfile_filename) }}" class="img-thumbnail img-responsive img-fluid">
                                <br />
                                <br />
                                <a href="{{ asset('upload/claim_damage/'.$row->cfile_filename) }}" target="_blank" class="btn btn-success">Cetak File</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal-drive-license" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        3. Fotocopy SIM
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @foreach($drive_license as $row)
                            <div class="col-lg-4 text-center">
                                <img src="{{ asset('upload/claim_drive_license/'.$row->cfile_filename) }}" class="img-thumbnail img-responsive img-fluid">
                                <br />
                                <br />
                                <a href="{{ asset('upload/claim_drive_license/'.$row->cfile_filename) }}" target="_blank" class="btn btn-success">Cetak File</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal-police-report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        4. Laporan Kepolisian
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @foreach($police_report as $row)
                            <div class="col-lg-4 text-center">
                                <img src="{{ asset('upload/claim_police_report/'.$row->cfile_filename) }}" class="img-thumbnail img-responsive img-fluid">
                                <br />
                                <br />
                                <a href="{{ asset('upload/claim_police_report/'.$row->cfile_filename) }}" target="_blank" class="btn btn-success">Cetak File</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal-events-chronology" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        5. Kronologi Kejadian
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @foreach($events_chronology as $row)
                            <div class="col-lg-4 text-center">
                                <img src="{{ asset('upload/claim_events_chronology/'.$row->cfile_filename) }}" class="img-thumbnail img-responsive img-fluid">
                                <br />
                                <br />
                                <a href="{{ asset('upload/claim_events_chronology/'.$row->cfile_filename) }}" target="_blank" class="btn btn-success">Cetak File</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal-loss-report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        6. Form Laporan Kerugian
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @foreach($loss_report as $row)
                            <div class="col-lg-4 text-center">
                                <img src="{{ asset('upload/claim_loss_report/'.$row->cfile_filename) }}" class="img-thumbnail img-responsive img-fluid">
                                <br />
                                <br />
                                <a href="{{ asset('upload/claim_loss_report/'.$row->cfile_filename) }}" target="_blank" class="btn btn-success">Cetak File</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection