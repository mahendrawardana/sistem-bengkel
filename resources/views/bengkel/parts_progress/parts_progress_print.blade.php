<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }
        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }
        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }


    </style>
    <?php
        $width_label = '64';
    ?>

</head>
<body>
<h1 align="center">PARTS ORDER</h1>
<hr/>

<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Estimasi</td>
            <td width="4">:</td>
            <td>192381-BTC-001</td>
        </tr>
        <tr>
            <td>Nama Pemilik</td>
            <td>:</td>
            <td>I Putu Mahendra Adi Wardana</td>
        </tr>
        <tr>
            <td>Nama Asuransi</td>
            <td>:</td>
            <td>Bank Central Asia</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Order</td>
            <td width="4">:</td>
            <td>192381-ORDER-002</td>
        </tr>
        <tr>
            <td>No. Polisi</td>
            <td>:</td>
            <td>DK 123 ABC</td>
        </tr>
        <tr>
            <td>Tahun Produksi</td>
            <td>:</td>
            <td>2015</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">&nbsp;</td>
            <td width="4"></td>
            <td></td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td>:</td>
            <td>S-CROSS</td>
        </tr>
        <tr>
            <td>Warna</td>
            <td>:</td>
            <td>Putih</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>

<br />
<br />

<h2>Parts Order</h2>

<table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Nama Parts</th>
        <th>Kode Parts</th>
        <th>Jumlah</th>
        <th>Harga Jual</th>
        <th>Harga Beli</th>
        <th>% Margin</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Fabric Paint</td>
        <td>10912</td>
        <td>1</td>
        <td>80000</td>
        <td>72000</td>
        <td>8000</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Styloz Fabric</td>
        <td>10913</td>
        <td>1</td>
        <td>60000</td>
        <td>52000</td>
        <td>8000</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Tesla Paints</td>
        <td>10915</td>
        <td>1</td>
        <td>60000</td>
        <td>55000</td>
        <td>5000</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Cat Acrylic</td>
        <td>10916</td>
        <td>1</td>
        <td>75000</td>
        <td>69000</td>
        <td>6000</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Mont Marte Fabric</td>
        <td>10917</td>
        <td>1</td>
        <td>130000</td>
        <td>125000</td>
        <td>5000</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="3" class="text-center">Total</th>
        <th>5</th>
        <th>405,000</th>
        <th>373,000</th>
        <th>32,000</th>
    </tr>
    </tfoot>
</table>
<br />
<br />
<br />
<div class="div-2 text-center">
    Menyetujui
    <br />
    <br />
    <br />
    <br />
    <br />
    <u>Dwi Aprianto</u><br />
    Pelanggan
</div>
<div class="div-2 text-center">
    Denpasar, <?php echo date('d') ?> Agustus 2020
    <br />
    <br />
    <br />
    <br />
    <br />
    <u>{{ Session::get('user')['karyawan']['nama_karyawan'] }}</u><br />
    Service Advisor
</div>
<div class="clearfix"></div>