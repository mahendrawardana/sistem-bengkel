@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".m-datatable").mDatatable({
                data: {saveState: {cookie: !1}},
                search: {input: $("#generalSearch")},
                columns: [
                    {field: "nomer", width: "30"},
                    {field: "menu", width: "110"},
                    {field: "no-polisi", width: "80"},
                    {field: "merek", width: "80"},
                    {field: "tipe", width: "80"},
                    {field: "alamat", width: "250"},
                ],
            });

            $('.btn-search').click(function () {
                var ada = $('[name="ada"]:checked').val();
                if (ada == 'yes') {
                    window.location.href = '<?php echo route('partsProgressEditPage') ?>';
                } else {
                    swal({
                        title: "Perhatian ...",
                        text: "Data Work Plan Tidak Ditemukan",
                        type: "warning",
                        showCancelButton: !0,
                        showConfirmButton: false,
                        confirmButtonText: "Lihat Daftar Pelanggan",
                        cancelButtonText: "Cari Lagi",
                    }).then(function (e) {
                        if (e.value) {
                            window.location.href = '<?php echo route('partsOrderPage') ?>';
                        }
                    });
                }
            });
        });
    </script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">


            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" action="#" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Nomor Polisi</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" value="DK 1234 DK">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Nomer Estimasi</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" value="1231019-EST-01">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Nomer Order</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" value="323123-ORD-01">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Ada Data ? (test)</label>
                            <div class="col-lg-3">
                                <label class="radio-inline">
                                    <input type="radio" name="ada" value="yes"> Ada
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="ada" value="no" checked> Tidak Ada
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group btn-group-sm">
                            <button type="button" class="btn btn-accent btn-search">
                                <i class="la la-search"></i> Cari Data
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Nama Part</th>
                                <th>Jumlah</th>
                                <th>No Part</th>
                                <th>No Polisi</th>
                                <th>Estimasi Selesai</th>
                                <th>Tanggal Order</th>
                                <th>E.T.A</th>
                                <th>Suplier</th>
                                <th>Status</th>
                                <th width="60">Menu</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php for($i = 1; $i <= 10; $i++) { ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td>Bumper Depan</td>
                                <td>1</td>
                                <td>1093-129423</td>
                                <td>DK 1242 SJ</td>
                                <td>24-10-2020</td>
                                <td>10-10-2020</td>
                                <td>14-10-2020</td>
                                <td>Suzuki Indotrada</td>
                                <td>Datang</td>
                                <td>
                                    <a class="btn btn-success btn-sm" href="{{ route('partsProgressEditPage') }}"><i
                                                class="la la-pencil"></i> Edit</a>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
