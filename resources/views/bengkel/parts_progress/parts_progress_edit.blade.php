@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.min.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <?php
    $select_date = '
            <input type="text" class="form-control" id="m_datepicker_1" readonly placeholder="Pilih Tanggal" />
';

    $select_time = '
        <input class="form-control m-timepicker" readonly placeholder="Pilih Jam" type="text" />
    ';

    $select_mechanic = '
                <select class="form-control m-select2" name="">
                    <option value="1">Mahendra</option>
                    <option value="2">Adi</option>
                    <option value="3">Wardana</option>
                    <option value="4">Kusuma</option>
                </select>
            ';

    ?>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('produksiInsert') }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('produksiPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Estimasi
                                    </label>
                                    <div class="col-8">
                                        111-EST-01
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        111-ORD-01
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pemilik
                                    </label>
                                    <div class="col-8">
                                        DWI APRIANTO W.Y.
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        Bank Central Asia
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        DK 1234 ABC
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        2015
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        S-CROSS
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        Putih
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">


                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">
                                        Nama Part
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" value="Bumper Depan">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">
                                        Jumlah Part
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" value="1">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">
                                        No Part
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" value="1029-192911">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">
                                        Estimasi Selesai
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" value="22-10-2020">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">

                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">
                                        Tanggal Order
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" value="10-10-2020">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">
                                        E.T.A
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" value="14-10-2020">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">
                                        Supplier
                                    </label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" value="Suzuki Indotrada">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">
                                        Status
                                    </label>
                                    <div class="col-9">
                                        <div class="m-radio-inline">
                                            <label class="m-radio m-radio--success">
                                                <input type="radio" name="example_3" value="1"> Order
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--brand">
                                                <input type="radio" name="example_3" value="2" checked> On The Way
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--primary">
                                                <input type="radio" name="example_3" value="3"> Datang
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <a href="{{ route('partsProgressPage') }}"
                       class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </a>

                    <a href="{{ route("partsProgressPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Data Parts Order</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection