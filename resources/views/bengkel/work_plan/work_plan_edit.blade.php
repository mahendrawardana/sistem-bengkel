@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.min.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                orientation: "bottom left",
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

            $('.datepicker').change(function () {
                var date = $(this).val();
                var day_order = $(this).data('key');
                $.ajax({
                    url: "{{ route('workPlanFormatDay') }}",
                    type: 'post',
                    data: {
                        _token: "{{ csrf_token() }}",
                        date: date
                    },
                    success: function(data) {
                        $(".day-"+day_order).html(data);
                    }
                })
            });
        });
    </script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('workPlanUpdate', ['id_order' => Main::encrypt($order->id_order)]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('workPlanPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <input type="hidden" name="work_plan_cetak" value="no">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Estimasi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->esti_number_label }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        {{ $order->order_number_label }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $order->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->insu_name }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_color_name }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Work Plan
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-center">Panel Repair</th>
                                            <th class="text-center">Pendempulan</th>
                                            <th class="text-center">Surfacer</th>
                                            <th class="text-center">Spraying</th>
                                            <th class="text-center">Polishing</th>
                                            <th class="text-center">Finishing</th>
                                            <th class="text-center">Delivery</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Hari</td>
                                            @foreach($work_plan_type_arr as $key => $type)
                                                <td class="day-{{ $key }}">
                                                    {{ Main::format_day($work_plan[$type]['work_plan_date']) }}
                                                </td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td>Tanggal</td>
                                            @foreach($work_plan_type_arr as $key => $type)
                                                <td>
                                                    <input type="text" class="form-control datepicker"
                                                           name="work_plan_date[{{ $type }}]" style="width: 150px"
                                                           value="{{ $work_plan[$type]['work_plan_date'] ? date('d-m-Y', strtotime($work_plan[$type]['work_plan_date'])) : '' }}"
                                                           data-key="{{ $key }}"
                                                           readonly
                                                           required
                                                           placeholder="Pilih Tanggal"/>
                                                </td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td>Jam</td>
                                            @foreach($work_plan_type_arr as $type)
                                                <td>
                                                    <input class="form-control m-timepicker"
                                                           name="work_plan_time[{{ $type }}]" style="width: 150px"
                                                           readonly
                                                           value="{{ $work_plan[$type]['work_plan_time'] }}"
                                                           required
                                                           placeholder="Pilih Jam" type="text"/>
                                                </td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td>Mekanik</td>
                                            @foreach($work_plan_type_arr as $type)
                                                <td>
                                                    <select class="form-control m-select2"
                                                            name="id_mechanic[{{ $type }}]"
                                                            required>
                                                        @foreach($mechanic as $row)
                                                            <option value="{{ $row->id_mechanic }}" {{ $work_plan[$type]['id_mechanic'] == $row->id_mechanic ? 'selected':'' }}>{{ $row->mechanic_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            @endforeach
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </button>

                    <button type="submit"
                            class="btn-submit-cetak-work-plan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Work Plan</span>
                        </span>
                    </button>

                    <a href="{{ route("workPlanPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection