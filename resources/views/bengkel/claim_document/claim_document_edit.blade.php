@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/kajree_preview_browse_file/css/fileinput.css') }}" media="all"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link href="{{ asset('assets/vendors/custom/kajree_preview_browse_file/themes/explorer-fas/theme.css') }}"
          media="all" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        .kv-file-upload {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/kajree_preview_browse_file/js/plugins/piexif.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/kajree_preview_browse_file/js/plugins/sortable.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/kajree_preview_browse_file/js/fileinput.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/kajree_preview_browse_file/js/locales/fr.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/kajree_preview_browse_file/js/locales/es.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/kajree_preview_browse_file/themes/fas/theme.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/kajree_preview_browse_file/themes/explorer-fas/theme.js') }}"
            type="text/javascript"></script>

    <script>
        $(document).ready(function () {

            $('[name="cfile_filename_policy_insurance[]"]').fileinput({
                initialPreview: [
                    @foreach($claim_file_policy_insurance as $row_file)
                        "{{ asset('upload/claim_policy_insurance/'.$row_file->cfile_filename) }}",
                    @endforeach
                ],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                        @foreach($claim_file_policy_insurance as $key => $row_file)
                    {
                        caption: "{{ $row_file->cfile_filename }}",
                        downloadUrl: "{{ asset('upload/claim_policy_insurance/'.$row_file->cfile_filename) }}",
                        width: "120px",
                        key: {{ $key }}
                    },
                    @endforeach
                ],
            });

            $('[name="cfile_filename_damage[]"]').fileinput({
                initialPreview: [
                    @foreach($claim_file_damage as $row_file)
                        "{{ asset('upload/claim_damage/'.$row_file->cfile_filename) }}",
                    @endforeach
                ],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                        @foreach($claim_file_damage as $key => $row_file)
                    {
                        caption: "{{ $row_file->cfile_filename }}",
                        downloadUrl: "{{ asset('upload/claim_damage/'.$row_file->cfile_filename) }}",
                        width: "120px",
                        key: {{ $key }}
                    },
                    @endforeach
                ],
            });

            $('[name="cfile_filename_drive_license[]"]').fileinput({
                initialPreview: [
                    @foreach($claim_file_drive_license as $row_file)
                        "{{ asset('upload/claim_drive_license/'.$row_file->cfile_filename) }}",
                    @endforeach
                ],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                        @foreach($claim_file_drive_license as $key => $row_file)
                    {
                        caption: "{{ $row_file->cfile_filename }}",
                        downloadUrl: "{{ asset('upload/claim_drive_license/'.$row_file->cfile_filename) }}",
                        width: "120px",
                        key: {{ $key }}
                    },
                    @endforeach
                ],
            });

            $('[name="cfile_filename_police_report[]"]').fileinput({
                initialPreview: [
                    @foreach($claim_file_police_report as $row_file)
                        "{{ asset('upload/claim_police_report/'.$row_file->cfile_filename) }}",
                    @endforeach
                ],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                        @foreach($claim_file_police_report as $key => $row_file)
                    {
                        caption: "{{ $row_file->cfile_filename }}",
                        downloadUrl: "{{ asset('upload/claim_police_report/'.$row_file->cfile_filename) }}",
                        width: "120px",
                        key: {{ $key }}
                    },
                    @endforeach
                ],
            });

            $('[name="cfile_filename_events_chronology[]"]').fileinput({
                initialPreview: [
                    @foreach($claim_file_events_chronology as $row_file)
                        "{{ asset('upload/claim_events_chronology/'.$row_file->cfile_filename) }}",
                    @endforeach
                ],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                        @foreach($claim_file_events_chronology as $key => $row_file)
                    {
                        caption: "{{ $row_file->cfile_filename }}",
                        downloadUrl: "{{ asset('upload/claim_events_chronology/'.$row_file->cfile_filename) }}",
                        width: "120px",
                        key: {{ $key }}
                    },
                    @endforeach
                ],
            });

            $('[name="cfile_filename_loss_report[]"]').fileinput({
                initialPreview: [
                    @foreach($claim_file_loss_report as $row_file)
                        "{{ asset('upload/claim_loss_report/'.$row_file->cfile_filename) }}",
                    @endforeach
                ],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                        @foreach($claim_file_loss_report as $key => $row_file)
                    {
                        caption: "{{ $row_file->cfile_filename }}",
                        downloadUrl: "{{ asset('upload/claim_loss_report/'.$row_file->cfile_filename) }}",
                        width: "120px",
                        key: {{ $key }}
                    },
                    @endforeach
                ],
            });


        });
    </script>

@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('claimDocumentUpdate', ['id_claim' => Main::encrypt($claim->id_claim)]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('claimDocumentPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true"
                  enctype="multipart/form-data">
                {{ csrf_field() }}

                <input type="hidden" name="id_car" value="{{ $id_car }}">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                {{--                                <div class="form-group m-form__group row">
                                                                    <label for="example-text-input" class="col-4 col-form-label required">
                                                                        Nomer Dokumen
                                                                    </label>
                                                                    <div class="col-8">
                                                                        100-DOK-01
                                                                    </div>
                                                                </div>--}}
                            </div>
                            <div class="col-xs-12 col-md-2 offset-md-6">
                                <div class="m-radio-list">
                                    <label class="m-radio m-radio--check-bold m-radio--state-success">
                                        <input type="radio" name="claim_status"
                                               value="add" {{ $claim->claim_status == 'add' ? 'checked':'' }}> Tambahan
                                        Klaim
                                        <span></span>
                                    </label>
                                    <label class="m-radio m-radio--check-bold m-radio--state-brand">
                                        <input type="radio" name="claim_status"
                                               value="revision" {{ $claim->claim_status == 'revision' ? 'checked':'' }}>
                                        Revisi Klaim
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Alamat Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_address }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Telp Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_phone }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_color_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polis
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_policy_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tipe
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Expire Date
                                    </label>
                                    <div class="col-8">
                                        {{ Main::format_date($row->insurance->insu_expire_date) }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <label class="m-checkbox m-checkbox--success">
                                                <input type="checkbox" checked> <h5>1. Fotocopy Polis Asuransi</h5>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <div class="file-loading">
                                                    <input name="cfile_filename_policy_insurance[]"
                                                           type="file" multiple
                                                           data-show-upload="false" data-show-caption="true"
                                                           data-msg-placeholder="Select {files} for upload...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <label class="m-checkbox m-checkbox--success">
                                                <input type="checkbox" checked> <h5>2. Foto Kerusakan</h5>
                                                <span></span>
                                            </label>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <div class="file-loading">
                                                    <input name="cfile_filename_damage[]"
                                                           type="file" multiple
                                                           data-show-upload="false" data-show-caption="true"
                                                           data-msg-placeholder="Select {files} for upload...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <label class="m-checkbox m-checkbox--success">
                                                <input type="checkbox" checked> <h5>3. Fotocopy SIM</h5>
                                                <span></span>
                                            </label>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <div class="file-loading">
                                                    <input name="cfile_filename_drive_license[]"
                                                           type="file" multiple
                                                           data-show-upload="false" data-show-caption="true"
                                                           data-msg-placeholder="Select {files} for upload...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <label class="m-checkbox m-checkbox--success">
                                                <input type="checkbox" checked> <h5>4. Laporan Kepolisian</h5>
                                                <span></span>
                                            </label>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <div class="file-loading">
                                                    <input name="cfile_filename_police_report[]"
                                                           type="file" multiple
                                                           data-show-upload="false" data-show-caption="true"
                                                           data-msg-placeholder="Select {files} for upload...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <label class="m-checkbox m-checkbox--success">
                                                <input type="checkbox" checked> <h5>5. Kronologi Kerjadian</h5>
                                                <span></span>
                                            </label>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <div class="file-loading">
                                                    <input name="cfile_filename_events_chronology[]"
                                                           type="file" multiple
                                                           data-show-upload="false" data-show-caption="true"
                                                           data-msg-placeholder="Select {files} for upload...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <label class="m-checkbox m-checkbox--success">
                                                <input type="checkbox" checked> <h5>6. Form Laporan Kerugian</h5>
                                                <span></span>
                                            </label>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <div class="file-loading">
                                                    <input name="cfile_filename_loss_report[]"
                                                           type="file" multiple
                                                           data-show-upload="false" data-show-caption="true"
                                                           data-msg-placeholder="Select {files} for upload...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Perbarui Klaim</span>
                        </span>
                    </button>

                    <a href="{{ route("claimDocumentPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Data Klaim</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection