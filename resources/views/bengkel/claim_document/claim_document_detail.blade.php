@extends('../general/index')

@section('css')
    <link href="{{ asset('plugin/lightbox/css/lightbox.min.css') }}" rel="stylesheet"/>
@endsection

@section('js')
    <script src="{{ asset('plugin/lightbox/js/lightbox.min.js') }}"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('claimDocumentInsert') }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('claimDocumentPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true"
                  enctype="multipart/form-data">
                {{ csrf_field() }}

                <input type="hidden" name="id_car" value="{{ $id_car }}">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                {{--                                <div class="form-group m-form__group row">
                                                                    <label for="example-text-input" class="col-4 col-form-label">
                                                                        Nomer Dokumen
                                                                    </label>
                                                                    <div class="col-8">
                                                                        100-DOK-01
                                                                    </div>
                                                                </div>--}}
                            </div>
                            <div class="col-xs-12 col-md-2 offset-md-6">
                                <div class="m-radio-list">
                                    <label class="m-radio m-radio--check-bold m-radio--state-success">
                                        <input type="radio" name="claim_status" value="add" {{ $claim->claim_status == 'add' ? 'checked':'' }} disabled> Tambahan
                                        Klaim
                                        <span></span>
                                    </label>
                                    <label class="m-radio m-radio--check-bold m-radio--state-brand">
                                        <input type="radio" name="claim_status" value="revision" {{ $claim->claim_status == 'revision' ? 'checked':'' }} disabled> Revisi Klaim
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Alamat Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_address }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Telp Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $row->customer->cust_phone }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_color_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polis
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_policy_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tipe
                                    </label>
                                    <div class="col-8">
                                        {{ $row->insurance->insu_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Expire Date
                                    </label>
                                    <div class="col-8">
                                        {{ Main::format_date($row->insurance->insu_expire_date) }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <h5>1. Fotocopy Polis Asuransi</h5>
                                        </div>
                                        <div class="col-12 col-md-12">
                                            @foreach($claim_file_policy_insurance as $row)
                                                <a href="{{ asset('upload/claim_policy_insurance/'.$row->cfile_filename) }}" data-lightbox="policy_insurance">
                                                    <img src="{{ asset('upload/claim_policy_insurance/'.$row->cfile_filename) }}" class="img-view img-thumbnail">
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <h5>2. Foto Kerusakan</h5>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                @foreach($claim_file_damage as $row)
                                                    <a href="{{ asset('upload/claim_damage/'.$row->cfile_filename) }}" data-lightbox="damage">
                                                        <img src="{{ asset('upload/claim_damage/'.$row->cfile_filename) }}" class="img-view img-thumbnail">
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <h5>3. Fotocopy SIM</h5>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                @foreach($claim_file_drive_license as $row)
                                                    <a href="{{ asset('upload/claim_drive_license/'.$row->cfile_filename) }}" data-lightbox="drive_license">
                                                        <img src="{{ asset('upload/claim_drive_license/'.$row->cfile_filename) }}" class="img-view img-thumbnail">
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <h5>4. Laporan Kepolisian</h5>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                @foreach($claim_file_police_report as $row)
                                                    <a href="{{ asset('upload/claim_police_report/'.$row->cfile_filename) }}" data-lightbox="police_report">
                                                        <img src="{{ asset('upload/claim_police_report/'.$row->cfile_filename) }}" class="img-view img-thumbnail">
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <h5>5. Kronologi Kerjadian</h5>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                @foreach($claim_file_events_chronology as $row)
                                                    <a href="{{ asset('upload/claim_events_chronology/'.$row->cfile_filename) }}" data-lightbox="events_cronology">
                                                        <img src="{{ asset('upload/claim_events_chronology/'.$row->cfile_filename) }}" class="img-view img-thumbnail">
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="border-foto">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <h5>6. Form Laporan Kerugian</h5>
                                        </div>

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                @foreach($claim_file_loss_report as $row)
                                                    <a href="{{ asset('upload/claim_loss_report/'.$row->cfile_filename) }}" data-lightbox="loss_report">
                                                        <img src="{{ asset('upload/claim_loss_report/'.$row->cfile_filename) }}" class="img-view img-thumbnail">
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">


                    <a href="{{ route('estimationFormCreate', ['id_car' => Main::encrypt($claim->id_car), 'id_claim' => Main::encrypt($claim->id_claim)]) }}"
                       class="btn-produk-add btn btn-info btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Buat Estimasi</span>
                        </span>
                    </a>

                    <a href="{{ route("claimDocumentPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Data Klaim</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection