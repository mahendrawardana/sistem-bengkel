@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('js/material_plan.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('materialPlanUpdate', ['id_order' => Main::encrypt($order->id_order)]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('materialPlanPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <input type="hidden" name="material_plan_cetak" value="no">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Estimasi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->esti_number_label }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        {{ $order->order_number_label }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $order->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->insu_name }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_color_name }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#" data-target="#kt_tabs_1_1"><h5>1. Material
                                Non Paint</h5></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" data-target="#kt_tabs_1_2"><h5>2. Material
                                Paint</h5></a>
                    </li>
                </ul>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-non-paint">
                                        <thead>
                                        <tr>
                                            <th width="20">No</th>
                                            <th>Nama Barang</th>
                                            <th>Kode Barang</th>
                                            <th>Plan</th>
                                            <th>Actual</th>
                                            <th>Harga Satuan</th>
                                            <th>Harga</th>
                                            <th width="50">Menu</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $mapl_plan_total = 0;
                                            $mapl_actual_total = 0;
                                            $mapl_item_price_total = 0;
                                            $mapl_price_total_non_paint_all = 0;
                                        @endphp
                                        @foreach($work_plan_non_paint as $key => $row)
                                            @php
                                                $no = $key + 1;
                                                $mapl_plan_total += $row->mapl_plan;
                                                $mapl_actual_total += $row->mapl_actual;
                                                $mapl_item_price_total += $row->mapl_item_price;
                                                $mapl_price_total_non_paint_all += $row->mapl_price_total;
                                            @endphp

                                            <tr class="non-paint-row">
                                                <td class="td-non-paint-data m--hide">
                                                    <input type="hidden" name="mapl_price_total[non_paint][]" value="{{ $row->mapl_price_total }}">
                                                </td>
                                                <td class="non-paint-no">{{ $no }}.</td>
                                                <td class="mapl-item-code">
                                                    <textarea class="form-control" name="mapl_item_name[non_paint][]">{{ $row->mapl_item_name }}</textarea>
                                                </td>
                                                <td class="mapl-item-code"><input type="text" class="form-control" name="mapl_item_code[non_paint][]" value="{{ $row->mapl_item_code }}"></td>
                                                <td class="mapl-plan"><input type="text" class="form-control input-numeral" name="mapl_plan[non_paint][]" value="{{ Main::format_number($row->mapl_plan) }}"></td>
                                                <td class="mapl-actual"><input type="text" class="form-control input-numeral" name="mapl_actual[non_paint][]" value="{{ Main::format_number($row->mapl_actual) }}"></td>
                                                <td class="mapl-item-price"><input type="text" class="form-control input-numeral" name="mapl_item_price[non_paint][]" value="{{ Main::format_number($row->mapl_item_price) }}"></td>
                                                <td class="mapl-price-total">{{ Main::format_number($row->mapl_price_total) }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm btn-non-paint-hapus">Hapus</button>
                                                </td>
                                            </tr>

                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="8">
                                                <button type="button" class="btn btn-success btn-non-paint-add">
                                                    <i class="la la-plus"></i> Tambah Material Non Paint
                                                </button>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="3" class="text-center">Total</th>
                                            <th class="mapl-plan-total">{{ Main::format_number($mapl_plan_total) }}</th>
                                            <th class="mapl-actual-total">{{ Main::format_number($mapl_actual_total) }}</th>
                                            <th class="mapl-item-price-total">{{ Main::format_number($mapl_item_price_total) }}</th>
                                            <th class="mapl-price-total-all">{{ Main::format_number($mapl_price_total_non_paint_all) }}</th>
                                            <th></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general table-paint">
                                        <thead>
                                        <tr>
                                            <th width="20">No</th>
                                            <th>Nama Barang</th>
                                            <th>Kode Barang</th>
                                            <th>Plan</th>
                                            <th>Actual</th>
                                            <th>Harga Satuan</th>
                                            <th>Harga</th>
                                            <th width="50">Menu</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $mapl_plan_total = 0;
                                            $mapl_actual_total = 0;
                                            $mapl_item_price_total = 0;
                                            $mapl_price_total_paint_all = 0;
                                        @endphp
                                        @foreach($work_plan_paint as $key => $row)
                                            @php
                                                $no = $key + 1;
                                                $mapl_plan_total += $row->mapl_plan;
                                                $mapl_actual_total += $row->mapl_actual;
                                                $mapl_item_price_total += $row->mapl_item_price;
                                                $mapl_price_total_paint_all += $row->mapl_price_total;
                                            @endphp

                                            <tr class="paint-row">
                                                <td class="td-paint-data m--hide">
                                                    <input type="hidden" name="mapl_price_total[paint][]" value="{{ $row->mapl_price_total }}">
                                                </td>
                                                <td class="paint-no">{{ $no }}.</td>
                                                <td class="mapl-item-code">
                                                    <textarea class="form-control" name="mapl_item_name[paint][]">{{ $row->mapl_item_name }}</textarea>
                                                </td>
                                                <td class="mapl-item-code"><input type="text" class="form-control" name="mapl_item_code[paint][]" value="{{ $row->mapl_item_code }}"></td>
                                                <td class="mapl-plan"><input type="text" class="form-control input-numeral" name="mapl_plan[paint][]" value="{{ Main::format_number($row->mapl_plan) }}"></td>
                                                <td class="mapl-actual"><input type="text" class="form-control input-numeral" name="mapl_actual[paint][]" value="{{ Main::format_number($row->mapl_actual) }}"></td>
                                                <td class="mapl-item-price"><input type="text" class="form-control input-numeral" name="mapl_item_price[paint][]" value="{{ Main::format_number($row->mapl_item_price) }}"></td>
                                                <td class="mapl-price-total">{{ Main::format_number($row->mapl_price_total) }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm btn-paint-hapus">Hapus</button>
                                                </td>
                                            </tr>

                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="8">
                                                <button type="button" class="btn btn-success btn-paint-add"><i
                                                            class="la la-plus"></i> Tambah Material Paint
                                                </button>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="3" class="text-center">Total</th>
                                            <th class="mapl-plan-total">{{ Main::format_number($mapl_plan_total) }}</th>
                                            <th class="mapl-actual-total">{{ Main::format_number($mapl_actual_total) }}</th>
                                            <th class="mapl-item-price-total">{{ Main::format_number($mapl_item_price_total) }}</th>
                                            <th class="mapl-price-total-all">{{ Main::format_number($mapl_price_total_paint_all) }}</th>
                                            <th></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-4 offset-md-8">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <div class="row detail-info">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-5 col-form-label">
                                                Total Harga
                                            </label>
                                            <div class="col-7 price-material-total">
                                                {{ Main::format_number($mapl_price_total_non_paint_all + $mapl_price_total_paint_all) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                       class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </button>

                    <button type="submit"
                       class="btn-submit-cetak-material-plan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Material Plan</span>
                        </span>
                    </button>

                    <a href="{{ route("materialPlanPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

    <table class="m--hide table-non-paint-row">
        <tbody>
        <tr class="non-paint-row">
            <td class="td-non-paint-data m--hide">
                <input type="hidden" name="mapl_price_total[non_paint][]" value="0">
            </td>
            <td class="non-paint-no">1</td>
            <td class="mapl-item-code">
                <textarea class="form-control" name="mapl_item_name[non_paint][]"></textarea>
            </td>
            <td class="mapl-item-code"><input type="text" class="form-control" name="mapl_item_code[non_paint][]" value=""></td>
            <td class="mapl-plan"><input type="text" class="form-control input-numeral" name="mapl_plan[non_paint][]" value="0"></td>
            <td class="mapl-actual"><input type="text" class="form-control input-numeral" name="mapl_actual[non_paint][]" value="0"></td>
            <td class="mapl-item-price"><input type="text" class="form-control input-numeral" name="mapl_item_price[non_paint][]" value="0"></td>
            <td class="mapl-price-total">0</td>
            <td>
                <button type="button" class="btn btn-danger btn-sm btn-non-paint-hapus">Hapus</button>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="m--hide table-paint-row">
        <tbody>
        <tr class="paint-row">
            <td class="td-paint-data m--hide">
                <input type="hidden" name="mapl_price_total[paint][]" value="0">
            </td>
            <td class="paint-no">1</td>
            <td class="mapl-item-code">
                <textarea class="form-control" name="mapl_item_name[paint][]"></textarea>
            </td>
            <td class="mapl-item-code"><input type="text" class="form-control" name="mapl_item_code[paint][]" value=""></td>
            <td class="mapl-plan"><input type="text" class="form-control input-numeral" name="mapl_plan[paint][]" value="0"></td>
            <td class="mapl-actual"><input type="text" class="form-control input-numeral" name="mapl_actual[paint][]" value="0"></td>
            <td class="mapl-item-price"><input type="text" class="form-control input-numeral" name="mapl_item_price[paint][]" value="0"></td>
            <td class="mapl-price-total">0</td>
            <td>
                <button type="button" class="btn btn-danger btn-sm btn-paint-hapus">Hapus</button>
            </td>
        </tr>
        </tbody>
    </table>

@endsection