<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }
        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }
        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }


    </style>
    <?php
        $width_label = '64';
    ?>

</head>
<body>
<h1 align="center">MATERIAL PLAN</h1>
<hr/>

<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Estimasi</td>
            <td width="4">:</td>
            <td>{{ $order->esti_number_label }}</td>
        </tr>
        <tr>
            <td>Nama Pelanggan</td>
            <td>:</td>
            <td>{{ $order->cust_name }}</td>
        </tr>
        <tr>
            <td>Nama Asuransi</td>
            <td>:</td>
            <td>{{ $order->insu_name }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Order</td>
            <td width="4">:</td>
            <td>{{ $order->order_number_label }}</td>
        </tr>
        <tr>
            <td>No. Polisi</td>
            <td>:</td>
            <td>{{ $order->car_police_number }}</td>
        </tr>
        <tr>
            <td>Tahun Produksi</td>
            <td>:</td>
            <td>{{ $order->car_production_year }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">&nbsp;</td>
            <td width="4"></td>
            <td></td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td>:</td>
            <td>{{ $order->car_type }}</td>
        </tr>
        <tr>
            <td>Warna</td>
            <td>:</td>
            <td>{{ $order->car_color_name }}</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>

<br />
<br />

<h2>1. Material Non Paint</h2>
<table class="table table-hover table-bordered table-striped">
    <thead>
    <tr>
        <th class="text-center" width="10">No</th>
        <th class="text-center">Nama Barang</th>
        <th class="text-center">Kode Barang</th>
        <th class="text-center">Plan</th>
        <th class="text-center">Actual</th>
        <th class="text-center">Harga Satuan</th>
        <th class="text-center">Harga</th>
    </tr>
    </thead>
    <tbody>
    @php
        $mapl_plan_total = 0;
        $mapl_actual_total = 0;
        $mapl_item_price_total = 0;
        $mapl_price_total_non_paint_all = 0;
    @endphp
    @foreach($material_plan_non_paint as $key => $row)
        @php
            $mapl_plan_total += $row->mapl_plan;
            $mapl_actual_total += $row->mapl_actual;
            $mapl_item_price_total += $row->mapl_item_price;
            $mapl_price_total_non_paint_all += $row->mapl_price_total;
        @endphp
    <tr>
        <td>{{ ++$key }}.</td>
        <td>{{ $row->mapl_item_name }}</td>
        <td>{{ $row->mapl_item_code }}</td>
        <td class="text-right">{{ Main::format_number($row->mapl_plan) }}</td>
        <td class="text-right">{{ Main::format_number($row->mapl_actual) }}</td>
        <td class="text-right">{{ Main::format_number($row->mapl_item_price) }}</td>
        <td class="text-right">{{ Main::format_number($row->mapl_price_total) }}</td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="3">Total</th>
            <th class="text-right">{{ Main::format_number($mapl_plan_total) }}</th>
            <th class="text-right">{{ Main::format_number($mapl_actual_total) }}</th>
            <th class="text-right">{{ Main::format_number($mapl_item_price_total) }}</th>
            <th class="text-right">{{ Main::format_number($mapl_price_total_non_paint_all) }}</th>
        </tr>
    </tfoot>
</table>
<br />

<h2>2. Material Paint</h2>
<table class="table table-hover table-bordered table-striped">
    <thead>
    <tr>
        <th class="text-center" width="10">No</th>
        <th class="text-center">Nama Barang</th>
        <th class="text-center">Kode Barang</th>
        <th class="text-center">Plan</th>
        <th class="text-center">Actual</th>
        <th class="text-center">Harga Satuan</th>
        <th class="text-center">Harga</th>
    </tr>
    </thead>
    <tbody>
    @php
        $mapl_plan_total = 0;
        $mapl_actual_total = 0;
        $mapl_item_price_total = 0;
        $mapl_price_total_paint_all = 0;
    @endphp
    @foreach($material_plan_paint as $key => $row)
        @php
            $mapl_plan_total += $row->mapl_plan;
            $mapl_actual_total += $row->mapl_actual;
            $mapl_item_price_total += $row->mapl_item_price;
            $mapl_price_total_paint_all += $row->mapl_price_total;
        @endphp
        <tr>
            <td>{{ ++$key }}.</td>
            <td>{{ $row->mapl_item_name }}</td>
            <td>{{ $row->mapl_item_code }}</td>
            <td class="text-right">{{ Main::format_number($row->mapl_plan) }}</td>
            <td class="text-right">{{ Main::format_number($row->mapl_actual) }}</td>
            <td class="text-right">{{ Main::format_number($row->mapl_item_price) }}</td>
            <td class="text-right">{{ Main::format_number($row->mapl_price_total) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="3">Total</th>
        <th class="text-right">{{ Main::format_number($mapl_plan_total) }}</th>
        <th class="text-right">{{ Main::format_number($mapl_actual_total) }}</th>
        <th class="text-right">{{ Main::format_number($mapl_item_price_total) }}</th>
        <th class="text-right">{{ Main::format_number($mapl_price_total_paint_all) }}</th>
    </tr>
    </tfoot>
</table>
<br />
<div class="div-2">
    <h2>3. Total</h2>
</div>
<div class="div-2 text-right">
    <h2>{{ Main::format_money($mapl_price_total_paint_all + $mapl_price_total_non_paint_all) }}</h2>
</div>
<div class="clearfix"></div>
<br />
<br />
<br />
<div class="div-2 text-center">
    Menyetujui
    <br />
    <br />
    <br />
    <br />
    <br />
    <u>{{ $order->cust_name }}</u><br />
    Pelanggan
</div>
<div class="div-2 text-center">
    Denpasar, {{ date('d F Y') }}
    <br />
    <br />
    <br />
    <br />
    <br />
    <u>{{ Session::get('user')['karyawan']['nama_karyawan'] }}</u><br />
    Foreman
</div>
<div class="clearfix"></div>