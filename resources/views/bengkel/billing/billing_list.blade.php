@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Filter Data
                                </h3>
                            </div>
                        </div>
                    </div>
                    <form method="post" action="{{ route('billingSearch') }}"
                          class="form-send m-form m-form--fit m-form--label-align-right">
                        {{ csrf_field() }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row text-center">
                                <label class="col-form-label col-lg-1 col-sm-12">Nomor Polisi</label>
                                <div class="col-lg-3 col-md-9 col-sm-12">
                                    <input type="text" class="form-control" name="car_police_number" value="{{ $car_police_number }}">
                                </div>
                                <label class="col-form-label col-lg-1 col-sm-12 font-weight-bold">Nomor Invoice</label>
                                <div class="col-lg-3 col-md-9 col-sm-12">
                                    <input type="text" class="form-control" name="invoice_number_label"
                                           value="{{ $invoice_number_label }}">
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot text-center">
                            <button type="submit" class="btn btn-accent btn-sm btn-search">
                                <i class="la la-search"></i> Cari Data
                            </button>
                            <a href="{{ route('billingPage') }}" class="btn btn-info btn-sm btn-search">
                                <i class="la la-random"></i> Reset
                            </a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="table table-bordered datatable-new"
                               data-url="{{ route('billingDataTable',['id_order' => $id_order]) }}"
                               data-column="{{ json_encode($datatable_column) }}">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>No Invoice</th>
                                <th>No Polisi</th>
                                <th>Type</th>
                                <th>Nama Pelanggan</th>
                                <th>Asuransi</th>
                                <th>Total Tagihan</th>
                                <th>Tanggal Invoice</th>
                                <th>Tanggal Kirim</th>
                                <th>Tanggal Bayar</th>
                                <th width="60">Progress</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>


    <form method="post" action="{{ route('billingUpdate') }}" class="form-send m-form m-form--fit m-form--label-align-right">

        {{ csrf_field() }}

        <input type="hidden" name="id_order">

        <div class="modal fade" id="modal-billing-update" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Perbarui Data Tanggal
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tanggal Kirim</label>
                            <div class="col-lg-8">
                                <input type='text' class="form-control m_datetimepicker_1_modal" name="send_date" readonly  data-z-index="1100" placeholder="Pilih Tanggal & Waktu"/>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Tanggal Bayar</label>
                            <div class="col-lg-8">
                                <input type='text' class="form-control m_datetimepicker_1_modal" name="payment_date" readonly  data-z-index="1100" placeholder="Pilih Tanggal & Waktu"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Kembali
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Perbarui Data
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection
