@extends('../general/index')

@section('css')

@endsection

@section('js')

@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('userList')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__body">
                    <form action="{{ route('reprintInvoice') }}"
                          method="post"
                          class="form-send"
                          data-alert-show="true"
                          data-alert-field-message="true">
                        {{csrf_field()}}
                        <div class="form-group m-form__group row">

                            <label class="col-lg-2 col-form-label">Nomor Invoice</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="invoice">
                            </div>
                            <div class="col-lg-4">
                                <button
                                        type="submit"
                                        class="btn btn-success">
                                    <i class="la la-print"></i> Cetak
                                </button>
                            </div>
                        </div>
                    </form>
                    <form action="{{ route('reprintParts') }}"
                          method="post"
                          class="form-send"
                          data-alert-show="true"
                          data-alert-field-message="true">
                        {{csrf_field()}}
                        <div class="form-group m-form__group row">

                            <label class="col-lg-2 col-form-label">Nomor Slip Parts</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="parts">
                            </div>
                            <div class="col-lg-4">
                                <button
                                        type="submit"
                                        class="btn btn-info">
                                    <i class="la la-print"></i> Cetak
                                </button>
                            </div>
                        </div>
                    </form>
                    <form action="{{ route('reprintSublet') }}"
                          method="post"
                          class="form-send"
                          data-alert-show="true"
                          data-alert-field-message="true">
                        {{csrf_field()}}
                        <div class="form-group m-form__group row">

                            <label class="col-lg-2 col-form-label">Nomor Slip Sublet</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="sublet">
                            </div>
                            <div class="col-lg-4">
                                <button
                                        type="submit"
                                        class="btn btn-primary">
                                    <i class="la la-print"></i> Cetak
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
@endsection
