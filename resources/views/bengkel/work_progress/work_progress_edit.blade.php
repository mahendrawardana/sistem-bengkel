@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugin/lightbox/css/lightbox.min.css') }}" rel="stylesheet"/>
    <style type="text/css">
        .select2-selection__clear {
            display: none !important;
        }
    </style>

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/work_progress.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('plugin/lightbox/js/lightbox.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                orientation: "bottom left",
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

            $('.datepicker').change(function () {
                var date = $(this).val();
                var day_order = $(this).data('key');
                $.ajax({
                    url: "{{ route('workPlanFormatDay') }}",
                    type: 'post',
                    data: {
                        _token: "{{ csrf_token() }}",
                        date: date
                    },
                    success: function(data) {
                        $(".day-"+day_order).html(data);
                    }
                })
            });
        });
    </script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('workProgressUpdate', ['id_order' => Main::encrypt($order->id_order)]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('workProgressPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <input type="hidden" name="work_progress_cetak" value="no">
                <input type="hidden" name="id_car" value="{{ $order->id_car }}">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Estimasi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->esti_number_label }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        {{ $order->order_number_label }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $order->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->insu_name }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_color_name }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Work Plan
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th colspan="2"></th>
                                            <th class="text-center text-uppercase font-weight-bold">Panel Repair</th>
                                            <th class="text-center text-uppercase font-weight-bold">Pendempulan</th>
                                            <th class="text-center text-uppercase font-weight-bold">Surfacer</th>
                                            <th class="text-center text-uppercase font-weight-bold">Spraying</th>
                                            <th class="text-center text-uppercase font-weight-bold">Polishing</th>
                                            <th class="text-center text-uppercase font-weight-bold">Finishing</th>
                                            <th class="text-center text-uppercase font-weight-bold">Delivery</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td rowspan="2" class="text-center text-uppercase font-weight-bold">Hari
                                            </td>
                                            <td class="text-center text-uppercase font-weight-bold">Plan</td>
                                            @foreach($type_list as $type)
                                                <td class="text-center">{{ Main::format_day($work_plan[$type]->work_plan_date) }}</td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td class="text-center text-uppercase font-weight-bold">Act</td>
                                            @foreach($type_list as $type)
                                                <td class="text-center day-{{$type}}">
                                                    {{ Main::format_day($work_progress[$type]['work_progress_date']) }}
                                                </td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td rowspan="2" class="text-center text-uppercase font-weight-bold">
                                                Tanggal
                                            </td>
                                            <td class="text-center text-uppercase font-weight-bold">Plan</td>
                                            @foreach($type_list as $type)
                                                <td class="text-center">{{ Main::format_date($work_plan[$type]['work_plan_date']) }}</td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td class="text-center text-uppercase font-weight-bold">Act</td>
                                            @foreach($type_list as $type)
                                                <td class="text-center">
                                                    <input
                                                            type="text"
                                                            class="form-control datepicker text-center"
                                                            data-key="{{ $type }}" style="width: 174px"
                                                            readonly
                                                            name="work_progress_date[{{ $type }}]"
                                                            value="{{ $work_progress[$type]['work_progress_date'] ? date('d-m-Y', strtotime($work_progress[$type]['work_progress_date'])) : '' }}"
                                                            placeholder="Pilih Tanggal" />
                                                </td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td rowspan="2" class="text-center text-uppercase font-weight-bold">Jam</td>
                                            <td class="text-center text-uppercase font-weight-bold">Plan</td>
                                            @foreach($type_list as $type)
                                                <td class="text-center">{{ $work_plan[$type]['work_plan_time'] }}</td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td class="text-center text-uppercase font-weight-bold">Act</td>
                                            @foreach($type_list as $type)
                                                <td>
                                                    <input
                                                            class="form-control m-timepicker text-center"
                                                            name="work_progress_time[{{ $type }}]"
                                                            value="{{ $work_progress[$type]['work_progress_time'] }}"
                                                            readonly
                                                            placeholder="Pilih Jam"
                                                            type="text" />
                                                </td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td rowspan="2" class="text-center text-uppercase font-weight-bold">
                                                Mekanik
                                            </td>
                                            <td class="text-center text-uppercase font-weight-bold">Plan</td>
                                            @foreach($type_list as $type)
                                                <td class="text-center">{{ $work_plan[$type]['mechanic_name'] }}</td>
                                            @endforeach

                                        </tr>
                                        <tr>
                                            <td class="text-center text-uppercase font-weight-bold">Act</td>
                                            @foreach($type_list as $type)
                                                <td>
                                                    <select class="form-control m-select2" name="id_mechanic[{{ $type }}]">
                                                        @foreach($mechanic as $row_mechanic)
                                                        <option value="{{ $row_mechanic->id_mechanic }}" {{ $work_progress[$type]['id_mechanic'] == $row_mechanic->id_mechanic ? 'selected':'' }}>{{ $row_mechanic->mechanic_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td class="text-center text-uppercase font-weight-bold">Gambar Upload</td>
                                            <td></td>

                                            @foreach($type_list as $type)
                                                @if($type == 'pendempulan')
                                                    <td class="text-center">
                                                        <div style="position:relative; margin-bottom: 4px">


                                                            <a class='btn btn-success btn-sm' href='javascript:;'>
                                                                <i class="la la-upload"></i> Upload Parts Lama
                                                                <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="wopf_parts_lama" class="btn-image-browse-parts-lama" >
                                                            </a>
                                                        </div>

                                                        <a href="{{ asset('upload/wopf_parts_lama/'.$work_progress_file['wopf_parts_lama']) }}" class="img-link-parts-lama" data-lightbox="damage">
                                                            <img src="{{ asset('upload/wopf_parts_lama/'.$work_progress_file['wopf_parts_lama']) }}" class="img-view img-thumbnail img-preview-parts-lama img-thumbnail">
                                                        </a>
                                                        <br />
                                                        <hr />
                                                        <div style="position:relative; margin-bottom: 4px">
                                                            <a class='btn btn-info btn-sm' href='javascript:;'>
                                                                <i class="la la-upload"></i> Upload Parts Baru
                                                                <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="wopf_parts_baru" class="btn-image-browse-parts-baru" >
                                                            </a>
                                                        </div>

                                                        <a href="{{ asset('upload/wopf_parts_baru/'.$work_progress_file['wopf_parts_baru']) }}" class="img-link-parts-baru" data-lightbox="damage">
                                                            <img src="{{ asset('upload/wopf_parts_baru/'.$work_progress_file['wopf_parts_baru']) }}" class="img-view img-thumbnail img-preview-parts-baru img-thumbnail">
                                                        </a>

                                                    </td>
                                                @elseif($type == 'surfacer')
                                                    <td class="text-center">
                                                        <div style="position:relative; margin-bottom: 4px">
                                                            <a class='btn btn-info btn-sm' href='javascript:;'>
                                                                <i class="la la-upload"></i> Upload Expoxy
                                                                <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="wopf_epoxy" class="btn-image-browse-epoxy" >
                                                            </a>
                                                        </div>

                                                        <a href="{{ asset('upload/wopf_epoxy/'.$work_progress_file['wopf_epoxy']) }}" class="img-link-epoxy" data-lightbox="damage">
                                                            <img src="{{ asset('upload/wopf_epoxy/'.$work_progress_file['wopf_epoxy']) }}" class="img-view img-thumbnail img-preview-epoxy img-thumbnail">
                                                        </a>
                                                    </td>
                                                @elseif($type == 'finishing')
                                                    <td class="text-center">
                                                        <div style="position:relative; margin-bottom: 4px">
                                                            <a class='btn btn-info btn-sm' href='javascript:;'>
                                                                <i class="la la-upload"></i> Upload Hasil Pekerjaan
                                                                <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="wopf_hasil_pekerjaan" class="btn-image-browse-hasil" >
                                                            </a>
                                                        </div>

                                                        <a href="{{ asset('upload/wopf_hasil_pekerjaan/'.$work_progress_file['wopf_hasil_pekerjaan']) }}" class="img-link-hasil" data-lightbox="damage">
                                                            <img src="{{ asset('upload/wopf_hasil_pekerjaan/'.$work_progress_file['wopf_hasil_pekerjaan']) }}" class="img-view img-thumbnail img-preview-hasil img-thumbnail">
                                                        </a>
                                                    </td>
                                                @else
                                                    <td></td>
                                                @endif
                                            @endforeach
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                       class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </button>

                    <button type="submit"
                       class="btn-submit-cetak-work-progress btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Progress Pekerjaan</span>
                        </span>
                    </button>

                    <a href="{{ route("workProgressPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection