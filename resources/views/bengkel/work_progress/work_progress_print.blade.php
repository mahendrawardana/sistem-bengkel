<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }

        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }


    </style>
    <?php
    $width_label = '64';
    ?>

</head>
<body>
<h1 align="center">Progress Pekerjaan</h1>
<hr/>

<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Estimasi</td>
            <td width="4">:</td>
            <td>{{ $order->esti_number_label }}</td>
        </tr>
        <tr>
            <td>Nama Pelanggan</td>
            <td>:</td>
            <td>{{ $order->cust_name }}</td>
        </tr>
        <tr>
            <td>Nama Asuransi</td>
            <td>:</td>
            <td>{{ $order->insu_name }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Order</td>
            <td width="4">:</td>
            <td>{{ $order->order_number_label }}</td>
        </tr>
        <tr>
            <td>No. Polisi</td>
            <td>:</td>
            <td>{{ $order->car_police_number }}</td>
        </tr>
        <tr>
            <td>Tahun Produksi</td>
            <td>:</td>
            <td>{{ $order->car_production_year }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">&nbsp;</td>
            <td width="4"></td>
            <td></td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td>:</td>
            <td>{{ $order->car_type }}</td>
        </tr>
        <tr>
            <td>Warna</td>
            <td>:</td>
            <td>{{ $order->car_color_name }}</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>

<br/>
<br/>

<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th colspan="2"></th>
        <th class="text-center text-uppercase font-weight-bold">Panel Repair</th>
        <th class="text-center text-uppercase font-weight-bold">Pendempulan</th>
        <th class="text-center text-uppercase font-weight-bold">Surfacer</th>
        <th class="text-center text-uppercase font-weight-bold">Spraying</th>
        <th class="text-center text-uppercase font-weight-bold">Polishing</th>
        <th class="text-center text-uppercase font-weight-bold">Finishing</th>
        <th class="text-center text-uppercase font-weight-bold">Delivery</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td rowspan="2" class="text-center text-uppercase font-weight-bold">Hari
        </td>
        <td class="text-center text-uppercase font-weight-bold">Plan</td>
        @foreach($type_list as $type)
            <td class="text-center">{{ Main::format_day($work_plan[$type]->work_plan_date) }}</td>
        @endforeach
    </tr>
    <tr>
        <td class="text-center text-uppercase font-weight-bold">Act</td>
        @foreach($type_list as $type)
            <td class="text-center day-{{$type}}">
                {{ Main::format_day($work_progress[$type]['work_progress_date']) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td rowspan="2" class="text-center text-uppercase font-weight-bold">
            Tanggal
        </td>
        <td class="text-center text-uppercase font-weight-bold">Plan</td>
        @foreach($type_list as $type)
            <td class="text-center">{{ Main::format_date($work_plan[$type]['work_plan_date']) }}</td>
        @endforeach
    </tr>
    <tr>
        <td class="text-center text-uppercase font-weight-bold">Act</td>
        @foreach($type_list as $type)
            <td class="text-center">
                {{ date('d-m-Y', strtotime($work_progress[$type]['work_progress_date'])) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td rowspan="2" class="text-center text-uppercase font-weight-bold">Jam</td>
        <td class="text-center text-uppercase font-weight-bold">Plan</td>
        @foreach($type_list as $type)
            <td class="text-center">{{ $work_plan[$type]['work_plan_time'] }}</td>
        @endforeach
    </tr>
    <tr>
        <td class="text-center text-uppercase font-weight-bold">Act</td>
        @foreach($type_list as $type)
            <td class="text-center">
                {{ $work_progress[$type]['work_progress_time'] }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td rowspan="2" class="text-center text-uppercase font-weight-bold">
            Mekanik
        </td>
        <td class="text-center text-uppercase font-weight-bold">Plan</td>
        @foreach($type_list as $type)
            <td class="text-center">{{ $work_plan[$type]['mechanic_name'] }}</td>
        @endforeach

    </tr>
    <tr>
        <td class="text-center text-uppercase font-weight-bold">Act</td>
        @foreach($type_list as $type)
            <td class="text-center">
                {{ $work_progress[$type]['mechanic_name'] }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td class="text-center text-uppercase font-weight-bold">Gambar Upload</td>
        <td></td>

        @foreach($type_list as $type)
            @if($type == 'pendempulan')
                <td class="text-center">
                    <strong>Gambar Parts Lama : </strong><br/>
                    <br/>
                    <img src="{{ asset('upload/wopf_parts_lama/'.$work_progress_file['wopf_parts_lama']) }}"
                         style="width: 100px">
                    <br/>
                    <hr/>
                    <strong>Gambar Parts Baru : </strong><br/>
                    <br/>
                    <img src="{{ asset('upload/wopf_parts_baru/'.$work_progress_file['wopf_parts_baru']) }}"
                         style="width: 100px">
                </td>
            @elseif($type == 'surfacer')
                <td class="text-center">
                    <strong>Gambar Epoxy :</strong><br/>
                    <br/>
                    <img src="{{ asset('upload/wopf_epoxy/'.$work_progress_file['wopf_epoxy']) }}" style="width: 100px">
                </td>
            @elseif($type == 'finishing')
                <td class="text-center">
                    <strong>Gambar Hasil Pekerjaan :</strong><br/>
                    <br/>
                    <img src="{{ asset('upload/wopf_hasil_pekerjaan/'.$work_progress_file['wopf_hasil_pekerjaan']) }}"
                         style="width: 100px">
                </td>
            @else
                <td></td>
            @endif
        @endforeach
    </tr>
    </tbody>
</table>
<br/>
<br/>
<br/>
<div class="div-2 text-center">
    Menyetujui
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <u>{{ $order->cust_name }}</u><br/>
    Pelanggan
</div>
<div class="div-2 text-center">
    Denpasar, {{ date('d F Y') }}
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <u>{{ Session::get('user')['karyawan']['nama_karyawan'] }}</u><br/>
    Foreman
</div>
<div class="clearfix"></div>