@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.min.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('js/parts_order.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    <?php
    $select_date = '
            <input type="text" class="form-control" id="m_datepicker_1" readonly placeholder="Pilih Tanggal" />
';

    $select_time = '
        <input class="form-control m-timepicker" readonly placeholder="Pilih Jam" type="text" />
    ';

    $select_mechanic = '
                <select class="form-control m-select2" name="">
                    <option value="1">Mahendra</option>
                    <option value="2">Adi</option>
                    <option value="3">Wardana</option>
                    <option value="4">Kusuma</option>
                </select>
            ';

    ?>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('partsOrderUpdate', ['id_order' => Main::encrypt($order->id_order)]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('partsOrderPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}

                <input type="hidden" name="parts_order_cetak" value="no">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Estimasi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->esti_number_label }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nomer Order
                                    </label>
                                    <div class="col-8">
                                        {{ $order->order_number_label }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <br/>

                        <div class="row detail-info">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Pelanggan
                                    </label>
                                    <div class="col-8">
                                        {{ $order->cust_name }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Nama Asuransi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->insu_name }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        No Polisi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_police_number }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Tahun Produksi
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_production_year }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_type }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Warna
                                    </label>
                                    <div class="col-8">
                                        {{ $order->car_color_name }}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="20">No</th>
                                    <th>Nama Parts</th>
                                    <th>Kode Parts</th>
                                    <th>Jumlah</th>
                                    <th>Harga Jual</th>
                                    <th width="200">Harga Beli</th>
                                    <th>% Margin</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $espa_qty_total = 0;
                                    $espa_price_item_total = 0;
                                    $paor_price_buy_total = 0;
                                    $margin = 0;
                                @endphp
                                @foreach($parts as $key => $row)
                                    @php
                                        $no = $key + 1;
                                        $espa_qty_total += $row->espa_qty;
                                        $espa_price_item_total += $row->espa_price_item;
                                        $paor_price_buy_total += $row->paor_price_buy;
                                    @endphp
                                    <tr>
                                        <td class="m--hide row-data">
                                            <input type="hidden" class="paor-margin-input"
                                                   name="paor_margin[{{ $row->id_estimate_parts_from }}]" value="{{ $row->paor_margin }}">
                                        </td>
                                        <td class="text-center">{{ $no }}.</td>
                                        <td class="text-center">{{ $row->espa_name }}</td>
                                        <td class="text-center">{{ $row->espa_number }}</td>
                                        <td class="text-right">{{ Main::format_number($row->espa_qty) }}</td>
                                        <td class="text-right espa-price-item">
                                            {{ Main::format_number($row->espa_price_item) }}
                                        </td>
                                        <td>
                                            <input type="text"
                                                   class="form-control input-numeral text-right paor-price-buy"
                                                   name="paor_price_buy[{{ $row->id_estimate_parts_from }}]"
                                                   value="{{ Main::format_number($row->paor_price_buy) }}">
                                        </td>
                                        <td class="text-center paor-margin">
                                            {{ round($row->paor_margin, 2) }}%
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="3" class="text-center">Total</th>
                                    <th class="text-right">{{ Main::format_number($espa_qty_total) }}</th>
                                    <th class="text-right espa-price-item-total">{{ Main::format_number($espa_price_item_total) }}</th>
                                    <th class="text-right paor-price-buy-total">{{ Main::format_number($paor_price_buy_total) }}</th>
                                    <th class="text-center paor-margin-total">{{ (($espa_price_item_total - $paor_price_buy_total) / $espa_price_item_total) * 100 }}
                                        %
                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan</span>
                        </span>
                    </button>

                    <button type="submit"
                            class="btn-submit-cetak-parts-order btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-print"></i>
                            <span>Cetak Parts Order</span>
                        </span>
                    </button>

                    <a href="{{ route("partsOrderPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali </span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection