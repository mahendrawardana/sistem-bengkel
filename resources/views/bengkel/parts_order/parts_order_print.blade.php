<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 12px;
            font-weight: bold;
        }

        .div-2 {
            width: 50%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 {
            width: 33.3%;
            float: left;
            padding: 0 4px 0 0;
        }

        .div-3 tr td {
            vertical-align: top;
            padding-top: 0;
        }

        .label-1 {
            margin-top: -10px;
            margin-bottom: -14px;
            padding-left: 30%;
        }
        .label-1 label {
            padding-top: 12px;
        }

        .label-2 {
            margin-top: -5px;
            margin-bottom: -6px;
            padding-left: 30%;
        }
        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }


    </style>
    <?php
        $width_label = '64';
    ?>

</head>
<body>
<h1 align="center">PARTS ORDER</h1>
<hr/>

<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Estimasi</td>
            <td width="4">:</td>
            <td>{{ $order->esti_number_label }}</td>
        </tr>
        <tr>
            <td>Nama Pelanggan</td>
            <td>:</td>
            <td>{{ $order->cust_name }}</td>
        </tr>
        <tr>
            <td>Nama Asuransi</td>
            <td>:</td>
            <td>{{ $order->insu_name }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">Nomor Order</td>
            <td width="4">:</td>
            <td>{{ $order->order_number_label }}</td>
        </tr>
        <tr>
            <td>No. Polisi</td>
            <td>:</td>
            <td>{{ $order->car_police_number }}</td>
        </tr>
        <tr>
            <td>Tahun Produksi</td>
            <td>:</td>
            <td>{{ $order->car_production_year }}</td>
        </tr>
    </table>
</div>
<div class="div-3">
    <table width="100%">
        <tr>
            <td width="<?php echo $width_label ?>">&nbsp;</td>
            <td width="4"></td>
            <td></td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td>:</td>
            <td>{{ $order->car_type }}</td>
        </tr>
        <tr>
            <td>Warna</td>
            <td>:</td>
            <td>{{ $order->car_color_name }}</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>

<br />
<br />

<h2>Parts Order</h2>

<table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Nama Parts</th>
        <th>Kode Parts</th>
        <th>Jumlah</th>
        <th>Harga Jual</th>
        <th>Harga Beli</th>
        <th>% Margin</th>
    </tr>
    </thead>
    <tbody>
    @php
        $espa_qty_total = 0;
        $espa_price_item_total = 0;
        $paor_price_buy_total = 0;
    @endphp
    @foreach($parts as $key => $row)
        @php
            $no = $key + 1;
            $espa_qty_total += $row->espa_qty;
            $espa_price_item_total += $row->espa_price_item;
            $paor_price_buy_total += $row->paor_price_buy;
        @endphp
    <tr>
        <td class="text-center">{{ $no }}.</td>
        <td>{{ $row->espa_name }}</td>
        <td>{{ $row->espa_number }}</td>
        <td class="text-right">{{ Main::format_number($row->espa_qty) }}</td>
        <td class="text-right">{{ Main::format_number($row->espa_price_item) }}</td>
        <td class="text-right">{{ Main::format_number($row->paor_price_buy) }}</td>
        <td class="text-center">{{ $row->paor_margin }}%</td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="3" class="text-center">Total</th>
        <th class="text-right">{{ Main::format_number($espa_qty_total) }}</th>
        <th class="text-right">{{ Main::format_number($espa_price_item_total) }}</th>
        <th class="text-right">{{ Main::format_number($paor_price_buy_total) }}</th>
        <th class="text-center">{{ (($espa_price_item_total - $paor_price_buy_total) / $espa_price_item_total) * 100 }}%</th>
    </tr>
    </tfoot>
</table>
<br />
<br />
<br />
<div class="div-2 text-center">
    Menyetujui
    <br />
    <br />
    <br />
    <br />
    <br />
    <u>{{ $order->cust_name }}</u><br />
    Pelanggan
</div>
<div class="div-2 text-center">
    Denpasar, {{ date('d F Y') }}
    <br />
    <br />
    <br />
    <br />
    <br />
    <u>{{ Session::get('user')['karyawan']['nama_karyawan'] }}</u><br />
    Foreman
</div>
<div class="clearfix"></div>