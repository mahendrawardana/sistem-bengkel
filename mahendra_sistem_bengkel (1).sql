-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 21, 2021 at 04:37 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mahendra_sistem_bengkel`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `analisa_penjualan_termasuk_ppn`
-- (See below for the actual view)
--
CREATE TABLE `analisa_penjualan_termasuk_ppn` (
);

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id_billing` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `billing_invoice` date DEFAULT NULL,
  `billing_send` date DEFAULT NULL,
  `billing_pay` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id_brand` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `brand_description` text DEFAULT NULL,
  `brand_logo` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id_car` int(11) NOT NULL,
  `id_car_owner` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_brand` int(11) DEFAULT NULL,
  `id_insurance` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `car_type` varchar(50) DEFAULT NULL,
  `car_color_name` varchar(50) DEFAULT NULL,
  `car_color_code` varchar(10) DEFAULT NULL,
  `car_production_year` year(4) DEFAULT NULL,
  `car_machine_number` varchar(40) DEFAULT NULL,
  `car_chassis_number` varchar(40) DEFAULT NULL,
  `car_stnk_picutre` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `car_owner`
--

CREATE TABLE `car_owner` (
  `id_car_owner` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `caro_name` varchar(255) DEFAULT NULL,
  `caro_address` text DEFAULT NULL,
  `caro_city` varchar(100) DEFAULT NULL,
  `caro_postal_code` char(8) DEFAULT NULL,
  `caro_phone` varchar(20) DEFAULT NULL,
  `caro_birthday` date DEFAULT NULL,
  `caro_job` varchar(70) DEFAULT NULL,
  `caro_hobby` varchar(100) DEFAULT NULL,
  `caro_ktp_number` varchar(20) DEFAULT NULL,
  `caro_ktp_picture` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `claim`
--

CREATE TABLE `claim` (
  `id_claim` int(11) NOT NULL,
  `id_car` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `claim_number` int(11) DEFAULT NULL,
  `claim_number_label` varchar(50) DEFAULT NULL,
  `claim_status` enum('add','revision') DEFAULT NULL,
  `claim_progress` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `claim_file`
--

CREATE TABLE `claim_file` (
  `id_claim_file` int(11) NOT NULL,
  `id_claim` int(11) DEFAULT NULL,
  `cfile_filename` varchar(255) DEFAULT NULL,
  `cfile_type` enum('policy_insurance','damage','drive_license','police_report','events_chronology','loss_report') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `cust_name` varchar(255) DEFAULT NULL,
  `cust_address` text DEFAULT NULL,
  `cust_city` varchar(50) DEFAULT NULL,
  `cust_postal_code` char(10) DEFAULT NULL,
  `cust_phone` varchar(20) DEFAULT NULL,
  `cust_birthday` date DEFAULT NULL,
  `cust_job` varchar(100) DEFAULT NULL,
  `cust_hobby` varchar(100) DEFAULT NULL,
  `cust_ktp_number` varchar(30) DEFAULT NULL,
  `cust_ktp_picture` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `id_user`, `cust_name`, `cust_address`, `cust_city`, `cust_postal_code`, `cust_phone`, `cust_birthday`, `cust_job`, `cust_hobby`, `cust_ktp_number`, `cust_ktp_picture`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2', '3', '4', '5', '6', '2021-01-21', '8', '9', '0', '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `estimate`
--

CREATE TABLE `estimate` (
  `id_estimate` int(11) NOT NULL,
  `id_claim` int(11) DEFAULT NULL,
  `id_car` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `esti_number` int(11) DEFAULT NULL,
  `esti_number_label` varchar(50) DEFAULT NULL,
  `esti_status` enum('add','revision') DEFAULT NULL,
  `esti_price_total` float DEFAULT NULL,
  `esti_finish` int(11) DEFAULT NULL,
  `esti_print_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `estimate_parts`
--

CREATE TABLE `estimate_parts` (
  `id_estimate_parts` int(11) NOT NULL,
  `id_estimate` int(11) DEFAULT NULL,
  `espa_name` varchar(255) DEFAULT NULL,
  `espa_number` varchar(255) DEFAULT NULL,
  `espa_qty` float DEFAULT NULL,
  `espa_price_item` float DEFAULT NULL,
  `espa_discount` float DEFAULT NULL,
  `espa_price_end` float DEFAULT NULL,
  `espa_valid` enum('true','false') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `estimate_services`
--

CREATE TABLE `estimate_services` (
  `id_estimate_services` int(11) NOT NULL,
  `id_estimate` int(11) DEFAULT NULL,
  `id_panel` int(11) DEFAULT NULL,
  `eser_status` enum('repaire','replace') DEFAULT NULL,
  `eser_price_item` float DEFAULT NULL,
  `eser_discount` float DEFAULT NULL,
  `eser_price_end` float DEFAULT NULL,
  `eser_valid` enum('true','false') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `estimate_sublet`
--

CREATE TABLE `estimate_sublet` (
  `id_estimate_sublet` int(11) NOT NULL,
  `id_estimate` int(11) DEFAULT NULL,
  `esub_name` varchar(255) DEFAULT NULL,
  `esub_qty` float DEFAULT NULL,
  `esub_price_item` float DEFAULT NULL,
  `esub_discount` float DEFAULT NULL,
  `esub_price_end` float DEFAULT NULL,
  `esub_valid` enum('true','false') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE `insurance` (
  `id_insurance` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `insu_name` varchar(255) DEFAULT NULL,
  `insu_address` text DEFAULT NULL,
  `insu_city` varchar(50) DEFAULT NULL,
  `insu_postal_code` varchar(10) DEFAULT NULL,
  `insu_policy_number` varchar(50) DEFAULT NULL,
  `insu_type` varchar(50) DEFAULT NULL,
  `insu_expire_date` datetime DEFAULT NULL,
  `insu_or_value` varchar(100) DEFAULT NULL,
  `insu_ktp_picture` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `job_progress`
--

CREATE TABLE `job_progress` (
  `id_job_progress` int(11) NOT NULL,
  `id_work_plan` int(11) DEFAULT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_mechanic` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `jobp_actual_time` datetime DEFAULT NULL,
  `jobp_filename_1` varchar(255) DEFAULT NULL,
  `jobp_filename_2` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in structure for view `laba_penjualan`
-- (See below for the actual view)
--
CREATE TABLE `laba_penjualan` (
);

-- --------------------------------------------------------

--
-- Table structure for table `material_plan`
--

CREATE TABLE `material_plan` (
  `id_material_plan` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `mapl_item_name` varchar(255) DEFAULT NULL,
  `mapl_item_code` varchar(100) DEFAULT NULL,
  `mapl_plan` float DEFAULT NULL,
  `mapl_actual` float DEFAULT NULL,
  `mapl_item_price` float DEFAULT NULL,
  `mapl_price_total` float DEFAULT NULL,
  `mapl_type` enum('non_paint','paint') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mechanic`
--

CREATE TABLE `mechanic` (
  `id_mechanic` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `mechanic_name` varchar(255) DEFAULT NULL,
  `mechanic_description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in structure for view `mutasilangganan`
-- (See below for the actual view)
--
CREATE TABLE `mutasilangganan` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `mutasilanggananlanjut`
-- (See below for the actual view)
--
CREATE TABLE `mutasilanggananlanjut` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `mutasisupplier`
-- (See below for the actual view)
--
CREATE TABLE `mutasisupplier` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `mutasisupplier2`
-- (See below for the actual view)
--
CREATE TABLE `mutasisupplier2` (
);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id_order` int(11) NOT NULL,
  `id_estimate` int(11) DEFAULT NULL,
  `id_car` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL,
  `order_number_label` varchar(255) DEFAULT NULL,
  `order_print_date` datetime DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `panel`
--

CREATE TABLE `panel` (
  `id_panel` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `panel_name` varchar(255) DEFAULT NULL,
  `panel_description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `parts_order`
--

CREATE TABLE `parts_order` (
  `id_parts_order` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `paor_item_name` varchar(255) DEFAULT NULL,
  `paor_item_code` varchar(100) DEFAULT NULL,
  `paor_item_price` float DEFAULT NULL,
  `paor_qty` float DEFAULT NULL,
  `paor_price_sell` float DEFAULT NULL,
  `paor_price_buy` float DEFAULT NULL,
  `paor_margin` float DEFAULT NULL,
  `paor_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `parts_progress`
--

CREATE TABLE `parts_progress` (
  `id_parts_progress` int(11) NOT NULL,
  `id_parts_order` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `papr_done_estimate` date DEFAULT NULL,
  `papr_eta` date DEFAULT NULL,
  `papr_supplier` varchar(255) DEFAULT NULL,
  `papr_status` enum('order','on_the_way','arrive') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in structure for view `pembelian_termasuk_ppn_perbarang`
-- (See below for the actual view)
--
CREATE TABLE `pembelian_termasuk_ppn_perbarang` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `perhitungan`
-- (See below for the actual view)
--
CREATE TABLE `perhitungan` (
);

-- --------------------------------------------------------

--
-- Table structure for table `qc_process`
--

CREATE TABLE `qc_process` (
  `id_qc_process` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_mechanic` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `qc_order` varchar(255) DEFAULT NULL,
  `qc_notes` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `id` int(11) NOT NULL,
  `kode_karyawan` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `nama_karyawan` varchar(255) DEFAULT NULL,
  `alamat_karyawan` text CHARACTER SET latin1 DEFAULT NULL,
  `telp_karyawan` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `posisi_karyawan` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `email_karyawan` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `foto_karyawan` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`id`, `kode_karyawan`, `nama_karyawan`, `alamat_karyawan`, `telp_karyawan`, `posisi_karyawan`, `email_karyawan`, `foto_karyawan`, `created_at`, `updated_at`) VALUES
(5, '2', 'Mahendra Wardana', 'Siibangkaja', '081934364063', 'Programmer', 'mahendra.adi.wardana@gmail.com', 'avatartahilalats.jpg', '2019-02-08 07:38:08', '2019-03-01 16:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) DEFAULT NULL,
  `id_user_role` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `inisial_karyawan` varchar(10) DEFAULT NULL,
  `id_lokasi` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `id_karyawan`, `id_user_role`, `username`, `password`, `inisial_karyawan`, `id_lokasi`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 'mahendrawardana', '$2y$10$3/dnn5l4R2C1BKnAOSThw.DA14SiQPcO1Vuh.JsUSzfxyfm.iNxoy', 'MD', '3', '2019-02-08 08:20:40', '2020-08-02 14:35:03'),
(23, 5, 17, 'billing', '$2y$10$OHGvyGYz8TiBzmRf6ue/NO3drsHLnJX3bjAJ6XSr6KujPv5b6cocu', 'billing', '6', '2020-08-02 18:18:43', '2020-08-15 05:33:32'),
(24, 5, 16, 'foreman', '$2y$10$8NNuiOLYUksBUoK7ScaP/.F11uxV/AWC5I.JAdlLrj0kLtUqTv9I6', 'foreman', '6', '2020-08-02 18:19:05', '2020-08-02 18:19:05'),
(25, 5, 15, 'sa', '$2y$10$UOzfr21unpUQUW3Lj.ZlA.Phh/B6P/fN8fo5iZJz5XbCCVqHCf8/u', 'sa', '6', '2020-08-02 18:19:22', '2020-08-02 18:19:52'),
(26, 5, 2, 'admin', '$2y$10$pwJTU5KKyUSJx4T0xusRiuUhgZ8ot0TMBe0zU0XY0dkvYy.20t0Fq', 'admin', '6', '2020-08-15 15:37:25', '2020-08-15 15:37:25');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_role`
--

CREATE TABLE `tb_user_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `role_keterangan` text DEFAULT NULL,
  `role_akses` text DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user_role`
--

INSERT INTO `tb_user_role` (`id`, `role_name`, `role_keterangan`, `role_akses`, `updated_at`, `created_at`) VALUES
(2, 'ADMINISTRATOR', NULL, '{\"dashboard\":{\"akses_menu\":true},\"dashboard_sa\":{\"akses_menu\":true},\"data_pelanggan\":{\"akses_menu\":true},\"dokumen_klaim\":{\"akses_menu\":true},\"form_estimasi\":{\"akses_menu\":true},\"form_order\":{\"akses_menu\":true},\"progress_klaim\":{\"akses_menu\":true},\"dashboard_foreman\":{\"akses_menu\":true},\"work_plan\":{\"akses_menu\":true},\"material_plan\":{\"akses_menu\":true},\"parts_order\":{\"akses_menu\":true},\"parts_progress\":{\"akses_menu\":true},\"qc_process\":{\"akses_menu\":true},\"progress_pekerjaan\":{\"akses_menu\":true},\"job_progress_board\":{\"akses_menu\":true},\"dashboard_billing\":{\"akses_menu\":true},\"validasi_dokumen\":{\"akses_menu\":true},\"validasi_order\":{\"akses_menu\":true},\"cetak_ulang\":{\"akses_menu\":true},\"penagihan\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":true,\"staff\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"role_user\":{\"akses_menu\":true,\"list\":true,\"menu_akses\":true,\"create\":true,\"edit\":true,\"delete\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}}}', '2020-10-03 04:14:05', '2019-05-14 22:20:00'),
(15, 'SERVICE ADVICER', NULL, '{\"dashboard\":{\"akses_menu\":false},\"dashboard_sa\":{\"akses_menu\":true},\"data_pelanggan\":{\"akses_menu\":true},\"dokumen_klaim\":{\"akses_menu\":true},\"form_estimasi\":{\"akses_menu\":true},\"form_order\":{\"akses_menu\":true},\"progress_klaim\":{\"akses_menu\":true},\"dashboard_foreman\":{\"akses_menu\":false},\"work_plan\":{\"akses_menu\":false},\"material_plan\":{\"akses_menu\":false},\"parts_order\":{\"akses_menu\":false},\"parts_progress\":{\"akses_menu\":false},\"qc_process\":{\"akses_menu\":false},\"progress_pekerjaan\":{\"akses_menu\":false},\"job_progress_board\":{\"akses_menu\":true},\"dashboard_billing\":{\"akses_menu\":false},\"validasi_dokumen\":{\"akses_menu\":false},\"validasi_order\":{\"akses_menu\":false},\"cetak_ulang\":{\"akses_menu\":false},\"penagihan\":{\"akses_menu\":false},\"master_data\":{\"akses_menu\":false,\"staff\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"role_user\":{\"akses_menu\":false,\"list\":false,\"menu_akses\":false,\"create\":false,\"edit\":false,\"delete\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}}}', '2020-10-03 04:14:53', '2020-08-02 18:16:33'),
(16, 'FOREMAN', NULL, '{\"dashboard\":{\"akses_menu\":false},\"dashboard_sa\":{\"akses_menu\":false},\"data_pelanggan\":{\"akses_menu\":false},\"dokumen_klaim\":{\"akses_menu\":false},\"form_estimasi\":{\"akses_menu\":false},\"form_order\":{\"akses_menu\":false},\"progress_klaim\":{\"akses_menu\":false},\"dashboard_foreman\":{\"akses_menu\":true},\"work_plan\":{\"akses_menu\":true},\"material_plan\":{\"akses_menu\":true},\"parts_order\":{\"akses_menu\":true},\"parts_progress\":{\"akses_menu\":true},\"qc_process\":{\"akses_menu\":true},\"progress_pekerjaan\":{\"akses_menu\":true},\"job_progress_board\":{\"akses_menu\":true},\"dashboard_billing\":{\"akses_menu\":false},\"validasi_dokumen\":{\"akses_menu\":false},\"validasi_order\":{\"akses_menu\":false},\"cetak_ulang\":{\"akses_menu\":false},\"penagihan\":{\"akses_menu\":false},\"master_data\":{\"akses_menu\":false,\"staff\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"role_user\":{\"akses_menu\":false,\"list\":false,\"menu_akses\":false,\"create\":false,\"edit\":false,\"delete\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}}}', '2020-10-03 04:14:40', '2020-08-02 18:16:40'),
(17, 'BILLING', NULL, '{\"dashboard\":{\"akses_menu\":false},\"dashboard_sa\":{\"akses_menu\":false},\"data_pelanggan\":{\"akses_menu\":false},\"dokumen_klaim\":{\"akses_menu\":false},\"form_estimasi\":{\"akses_menu\":false},\"form_order\":{\"akses_menu\":false},\"progress_klaim\":{\"akses_menu\":false},\"dashboard_foreman\":{\"akses_menu\":false},\"work_plan\":{\"akses_menu\":false},\"material_plan\":{\"akses_menu\":false},\"parts_order\":{\"akses_menu\":false},\"parts_progress\":{\"akses_menu\":false},\"qc_process\":{\"akses_menu\":false},\"progress_pekerjaan\":{\"akses_menu\":false},\"job_progress_board\":{\"akses_menu\":false},\"dashboard_billing\":{\"akses_menu\":true},\"validasi_dokumen\":{\"akses_menu\":true},\"validasi_order\":{\"akses_menu\":true},\"cetak_ulang\":{\"akses_menu\":true},\"penagihan\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":false,\"staff\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"role_user\":{\"akses_menu\":false,\"list\":false,\"menu_akses\":false,\"create\":false,\"edit\":false,\"delete\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}}}', '2020-10-03 04:14:21', '2020-08-02 18:16:45');

-- --------------------------------------------------------

--
-- Stand-in structure for view `totalpenjualanperbarang`
-- (See below for the actual view)
--
CREATE TABLE `totalpenjualanperbarang` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `totalstokbarang2`
-- (See below for the actual view)
--
CREATE TABLE `totalstokbarang2` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `totalstokbarang3`
-- (See below for the actual view)
--
CREATE TABLE `totalstokbarang3` (
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `totalstokbarang4`
-- (See below for the actual view)
--
CREATE TABLE `totalstokbarang4` (
);

-- --------------------------------------------------------

--
-- Table structure for table `work_plan`
--

CREATE TABLE `work_plan` (
  `id_work_plan` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_mechanic` int(11) DEFAULT NULL,
  `work_plan_time` datetime DEFAULT NULL,
  `work_plan_type` enum('panel_repair','pendempulan','surfacer','spraying','polishing','finishing','delivery') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure for view `analisa_penjualan_termasuk_ppn`
--
DROP TABLE IF EXISTS `analisa_penjualan_termasuk_ppn`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `analisa_penjualan_termasuk_ppn`  AS  (select `tb_item_penjualan`.`id` AS `id_item_penjualan`,`tb_item_penjualan`.`id_penjualan` AS `id_penjualan`,`tb_item_penjualan`.`id_barang` AS `id_barang`,`tb_item_penjualan`.`id_stok` AS `id_stok`,`tb_barang`.`id_golongan` AS `id_golongan`,`tb_golongan`.`golongan` AS `golongan`,`tb_penjualan`.`id_langganan` AS `id_langganan`,`tb_penjualan`.`id_salesman` AS `id_salesman`,`tb_salesman`.`salesman` AS `salesman`,`tb_penjualan`.`id_lokasi` AS `id_lokasi`,`tb_lokasi`.`id_wilayah` AS `id_wilayah`,`tb_penjualan`.`tgl_penjualan` AS `tgl_penjualan`,`tb_barang`.`kode_barang` AS `kode_barang`,`tb_barang`.`nama_barang` AS `nama_barang`,`tb_item_penjualan`.`jml_barang_penjualan` AS `qty`,`tb_item_penjualan`.`subtotal` AS `subtotal`,`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100 AS `setelah_diskon_persen`,`tb_penjualan`.`disc_nominal_penjualan` / (`tb_penjualan`.`total_penjualan` - `tb_penjualan`.`total_penjualan` * `tb_penjualan`.`disc_persen_penjualan` / 100) * 100 AS `persen_nominal`,(`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100) * (`tb_penjualan`.`disc_nominal_penjualan` / (`tb_penjualan`.`total_penjualan` - `tb_penjualan`.`total_penjualan` * `tb_penjualan`.`disc_persen_penjualan` / 100)) AS `nominal`,`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100 - (`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100) * (`tb_penjualan`.`disc_nominal_penjualan` / (`tb_penjualan`.`total_penjualan` - `tb_penjualan`.`total_penjualan` * `tb_penjualan`.`disc_persen_penjualan` / 100)) AS `dpp`,(`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100 - (`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100) * (`tb_penjualan`.`disc_nominal_penjualan` / (`tb_penjualan`.`total_penjualan` - `tb_penjualan`.`total_penjualan` * `tb_penjualan`.`disc_persen_penjualan` / 100))) / `tb_item_penjualan`.`jml_barang_penjualan` AS `harga_net`,`tb_penjualan`.`pajak_persen_penjualan` * (`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100 - (`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100) * (`tb_penjualan`.`disc_nominal_penjualan` / (`tb_penjualan`.`total_penjualan` - `tb_penjualan`.`total_penjualan` * `tb_penjualan`.`disc_persen_penjualan` / 100))) / 100 AS `ppn`,`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100 - (`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100) * (`tb_penjualan`.`disc_nominal_penjualan` / (`tb_penjualan`.`total_penjualan` - `tb_penjualan`.`total_penjualan` * `tb_penjualan`.`disc_persen_penjualan` / 100)) + `tb_penjualan`.`pajak_persen_penjualan` * (`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100 - (`tb_item_penjualan`.`subtotal` - `tb_item_penjualan`.`subtotal` * `tb_penjualan`.`disc_persen_penjualan` / 100) * (`tb_penjualan`.`disc_nominal_penjualan` / (`tb_penjualan`.`total_penjualan` - `tb_penjualan`.`total_penjualan` * `tb_penjualan`.`disc_persen_penjualan` / 100))) / 100 AS `total` from ((((((`tb_item_penjualan` left join `tb_penjualan` on(`tb_penjualan`.`id` = `tb_item_penjualan`.`id_penjualan`)) left join `tb_barang` on(`tb_item_penjualan`.`id_barang` = `tb_barang`.`id`)) left join `tb_lokasi` on(`tb_lokasi`.`id` = `tb_penjualan`.`id_lokasi`)) left join `tb_salesman` on(`tb_penjualan`.`id_salesman` = `tb_salesman`.`id`)) left join `tb_wilayah` on(`tb_lokasi`.`id_wilayah` = `tb_wilayah`.`id`)) left join `tb_golongan` on(`tb_barang`.`id_golongan` = `tb_golongan`.`id`)) group by `tb_item_penjualan`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `laba_penjualan`
--
DROP TABLE IF EXISTS `laba_penjualan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `laba_penjualan`  AS  select `tb_penjualan`.`id` AS `id`,`tb_penjualan`.`no_penjualan` AS `no_penjualan`,`tb_penjualan`.`tgl_penjualan` AS `tgl_penjualan`,`tb_piutang_langganan`.`jatuh_tempo` AS `jatuh_tempo`,`tb_lokasi`.`lokasi` AS `lokasi`,`tb_langganan`.`langganan` AS `langganan`,`tb_salesman`.`salesman` AS `salesman`,sum(`tb_item_penjualan`.`jml_barang_penjualan` * `tb_item_penjualan`.`harga_net`) AS `netto`,sum(`tb_item_penjualan`.`jml_barang_penjualan` * `tb_barang`.`hpp`) AS `hpp_penjualan` from ((((((`tb_item_penjualan` left join `tb_penjualan` on(`tb_item_penjualan`.`id_penjualan` = `tb_penjualan`.`id`)) left join `tb_lokasi` on(`tb_penjualan`.`id_lokasi` = `tb_lokasi`.`id`)) left join `tb_langganan` on(`tb_penjualan`.`id_langganan` = `tb_langganan`.`id`)) left join `tb_piutang_langganan` on(`tb_penjualan`.`id` = `tb_piutang_langganan`.`id_penjualan`)) left join `tb_salesman` on(`tb_penjualan`.`id_salesman` = `tb_salesman`.`id`)) left join `tb_barang` on(`tb_item_penjualan`.`id_barang` = `tb_barang`.`id`)) group by `tb_item_penjualan`.`id_penjualan` ;

-- --------------------------------------------------------

--
-- Structure for view `mutasilangganan`
--
DROP TABLE IF EXISTS `mutasilangganan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mutasilangganan`  AS  select distinct `tb_piutang_langganan`.`id` AS `id_piutang_langganan`,`tb_payment_piutang_langganan`.`id` AS `id_payment`,`tb_piutang_langganan`.`id_langganan` AS `id_langganan`,`tb_langganan`.`kode_langganan` AS `kode_langganan`,`tb_langganan`.`langganan` AS `langganan`,`tb_lokasi`.`id` AS `id_lokasi`,`tb_piutang_langganan`.`created_at` AS `tanggal_piutang_langganan`,`tb_payment_piutang_langganan`.`tgl_pembayaran` AS `tgl_payment_piutang_langganan`,`tb_piutang_langganan`.`total_piutang` AS `sisa_piutang`,`tb_piutang_langganan`.`tgl_faktur_penjualan` AS `tgl_penjualan`,`tb_payment_piutang_langganan`.`jumlah` AS `jumlah_payment`,`tb_payment_piutang_langganan`.`jenis_pembayaran` AS `jenis_pembayaran`,`tb_piutang_langganan`.`total_piutang` AS `total_piutang`,`tb_piutang_langganan`.`status` AS `status_piutang_langganan` from ((((`tb_piutang_langganan` left join `tb_langganan` on(`tb_piutang_langganan`.`id_langganan` = `tb_langganan`.`id`)) left join `tb_payment_piutang_langganan` on(`tb_piutang_langganan`.`id` = `tb_payment_piutang_langganan`.`id_piutang`)) left join `tb_penjualan` on(`tb_piutang_langganan`.`id_penjualan` = `tb_penjualan`.`id`)) left join `tb_lokasi` on(`tb_penjualan`.`id_lokasi` = `tb_lokasi`.`id`)) order by `tb_piutang_langganan`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `mutasilanggananlanjut`
--
DROP TABLE IF EXISTS `mutasilanggananlanjut`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mutasilanggananlanjut`  AS  select `mutasilangganan`.`id_piutang_langganan` AS `id_piutang_langganan`,count(0) AS `hitung`,`mutasilangganan`.`sisa_piutang` / count(0) AS `rata` from `mutasilangganan` group by `mutasilangganan`.`id_piutang_langganan` ;

-- --------------------------------------------------------

--
-- Structure for view `mutasisupplier`
--
DROP TABLE IF EXISTS `mutasisupplier`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mutasisupplier`  AS  (select distinct `tb_hutang_supplier`.`id` AS `id_hutang`,`tb_payment_hutang_supplier`.`id` AS `id_payment`,`tb_hutang_supplier`.`id_supplier` AS `id_supplier`,`tb_supplier`.`kode_supplier` AS `kode_supplier`,`tb_supplier`.`supplier` AS `supplier`,`tb_pembelian_return`.`id` AS `id_return`,`tb_wilayah`.`id` AS `wilayah`,`tb_hutang_supplier`.`created_at` AS `tanggal_hutang`,`tb_payment_hutang_supplier`.`created_at` AS `tgl_payment_hutang`,`tb_po`.`sisa_pembayaran_po` AS `original_pembayaran`,`tb_po`.`created_at` AS `tgl_po`,`tb_payment_hutang_supplier`.`jumlah` AS `jumlah_payment`,`tb_pembelian_return_barang`.`qty_harga_return` AS `harga_return`,`tb_pembelian_return`.`tanggal_return` AS `tanggal_return`,`tb_hutang_supplier`.`sisa_hutang_supplier` AS `sisa_hutang`,`tb_hutang_supplier`.`status_hutang_supplier` AS `status_hutang` from (((((((`tb_hutang_supplier` left join `tb_supplier` on(`tb_hutang_supplier`.`id_supplier` = `tb_supplier`.`id`)) left join `tb_wilayah` on(`tb_supplier`.`id_wilayah` = `tb_wilayah`.`id`)) left join `tb_po` on(`tb_hutang_supplier`.`id_po` = `tb_po`.`id`)) left join `tb_payment_hutang_supplier` on(`tb_hutang_supplier`.`id` = `tb_payment_hutang_supplier`.`id_hutang`)) left join `tb_pembelian` on(`tb_hutang_supplier`.`id_pembelian` = `tb_pembelian`.`id`)) left join `tb_pembelian_return` on(`tb_pembelian_return`.`id_pembelian` = `tb_pembelian`.`id`)) left join `tb_pembelian_return_barang` on(`tb_pembelian_return_barang`.`id_pembelian_return` = `tb_pembelian_return`.`id`)) order by `tb_hutang_supplier`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `mutasisupplier2`
--
DROP TABLE IF EXISTS `mutasisupplier2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mutasisupplier2`  AS  (select distinct `mutasisupplier`.`id_hutang` AS `id_hutang`,`mutasisupplier`.`id_payment` AS `id_payment`,`mutasisupplier`.`id_supplier` AS `id_supplier`,`mutasisupplier`.`wilayah` AS `id_wilayah`,count(0) AS `count_hutang`,`mutasisupplier`.`original_pembayaran` / count(0) AS `rata` from `mutasisupplier` group by `mutasisupplier`.`id_hutang` order by `mutasisupplier`.`id_supplier`) ;

-- --------------------------------------------------------

--
-- Structure for view `pembelian_termasuk_ppn_perbarang`
--
DROP TABLE IF EXISTS `pembelian_termasuk_ppn_perbarang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pembelian_termasuk_ppn_perbarang`  AS  (select `tb_po_barang`.`id` AS `id_po_barang`,`tb_po_barang`.`id_barang` AS `id_barang`,`tb_po_barang`.`id_po` AS `id_po`,`tb_po_barang`.`id_satuan` AS `id_satuan`,`tb_po_barang`.`id_stok` AS `id_stok`,`tb_barang`.`id_golongan` AS `id_golongan`,`tb_po`.`id_lokasi` AS `id_lokasi`,`tb_po`.`id_supplier` AS `id_supplier`,`tb_po`.`tgl_po` AS `tgl_po`,`tb_pembelian`.`tanggal_pembelian` AS `tgl_pembelian`,`tb_barang`.`kode_barang` AS `kode_barang`,`tb_barang`.`nama_barang` AS `nama_barang`,`tb_po_barang`.`qty` AS `qty`,`tb_po`.`discount_persen_po` AS `disc_persen_global`,`tb_po`.`discount_nominal_po` AS `disc_nominal_global`,`tb_po_barang`.`sub_total` AS `subtotal`,`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100 AS `setelah_diskon`,`tb_po`.`discount_nominal_po` / (`tb_po`.`total_po` - `tb_po`.`total_po` * `tb_po`.`discount_persen_po` / 100) * 100 AS `persen_nominal`,(`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100) * (`tb_po`.`discount_nominal_po` / (`tb_po`.`total_po` - `tb_po`.`total_po` * `tb_po`.`discount_persen_po` / 100)) AS `nominal`,`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100 - (`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100) * (`tb_po`.`discount_nominal_po` / (`tb_po`.`total_po` - `tb_po`.`total_po` * `tb_po`.`discount_persen_po` / 100)) AS `dpp`,(`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100 - (`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100) * (`tb_po`.`discount_nominal_po` / (`tb_po`.`total_po` - `tb_po`.`total_po` * `tb_po`.`discount_persen_po` / 100))) / `tb_po_barang`.`qty` AS `harga_net`,`tb_po`.`pajak_persen_po` * (`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100 - (`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100) * (`tb_po`.`discount_nominal_po` / (`tb_po`.`total_po` - `tb_po`.`total_po` * `tb_po`.`discount_persen_po` / 100))) / 100 AS `ppn`,`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100 - (`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100) * (`tb_po`.`discount_nominal_po` / (`tb_po`.`total_po` - `tb_po`.`total_po` * `tb_po`.`discount_persen_po` / 100)) + `tb_po`.`pajak_persen_po` * (`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100 - (`tb_po_barang`.`sub_total` - `tb_po_barang`.`sub_total` * `tb_po`.`discount_persen_po` / 100) * (`tb_po`.`discount_nominal_po` / (`tb_po`.`total_po` - `tb_po`.`total_po` * `tb_po`.`discount_persen_po` / 100))) / 100 AS `total` from (((((((`tb_po_barang` left join `tb_po` on(`tb_po_barang`.`id_po` = `tb_po`.`id`)) left join `tb_pembelian` on(`tb_po`.`id` = `tb_pembelian`.`id_po`)) left join `tb_barang` on(`tb_po_barang`.`id_barang` = `tb_barang`.`id`)) left join `tb_lokasi` on(`tb_po`.`id_lokasi` = `tb_lokasi`.`id`)) left join `tb_supplier` on(`tb_po`.`id_supplier` = `tb_supplier`.`id`)) left join `tb_satuan` on(`tb_po_barang`.`id_satuan` = `tb_satuan`.`id`)) left join `tb_golongan` on(`tb_barang`.`id_golongan` = `tb_golongan`.`id`)) group by `tb_po_barang`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `perhitungan`
--
DROP TABLE IF EXISTS `perhitungan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `perhitungan`  AS  select `tb_stok`.`id_barang` AS `id_barang`,sum(`tb_stok`.`jml_barang`) AS `total_stok`,avg(`tb_stok`.`hpp`) AS `rata_rata_hpp` from `tb_stok` group by `tb_stok`.`id_barang` ;

-- --------------------------------------------------------

--
-- Structure for view `totalpenjualanperbarang`
--
DROP TABLE IF EXISTS `totalpenjualanperbarang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `totalpenjualanperbarang`  AS  (select `tb_barang`.`kode_barang` AS `kode_barang`,`tb_barang`.`nama_barang` AS `nama_barang`,`tb_item_penjualan`.`nama_barang_penjualan` AS `nama_barang2`,`tb_barang`.`id_golongan` AS `id_golongan`,`tb_golongan`.`golongan` AS `golongan`,`tb_item_penjualan`.`golongan_penjualan` AS `golongan2`,sum(`tb_item_penjualan`.`jml_barang_penjualan`) AS `total_jml_barang_penjualan`,sum(`tb_item_penjualan`.`subtotal`) AS `total_subtotal` from ((`tb_item_penjualan` left join `tb_barang` on(`tb_item_penjualan`.`id_barang` = `tb_barang`.`id`)) left join `tb_golongan` on(`tb_barang`.`id_golongan` = `tb_golongan`.`id`)) group by `tb_item_penjualan`.`id_barang`) ;

-- --------------------------------------------------------

--
-- Structure for view `totalstokbarang2`
--
DROP TABLE IF EXISTS `totalstokbarang2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `totalstokbarang2`  AS  (select `tb_barang`.`id` AS `id_barang`,`tb_barang`.`kode_barang` AS `kode_barang`,`tb_barang`.`nama_barang` AS `nama_barang`,`tb_barang`.`id_golongan` AS `id_golongan`,`tb_barang`.`hpp` AS `hpp`,`tb_barang`.`harga_beli_terakhir_barang` AS `harga_beli_terakhir_barang`,`tb_barang`.`harga_jual_barang` AS `harga_jual_barang`,`tb_stok`.`id_satuan` AS `id_satuan`,`tb_stok`.`satuan` AS `satuan`,`tb_stok`.`id` AS `id_stok`,`tb_golongan`.`golongan` AS `golongan`,round(sum(`tb_stok`.`jml_barang`),2) AS `total_stok` from ((`tb_stok` join `tb_barang` on(`tb_stok`.`id_barang` = `tb_barang`.`id`)) join `tb_golongan` on(`tb_barang`.`id_golongan` = `tb_golongan`.`id`)) where `tb_stok`.`satuan` is not null and `tb_stok`.`id_satuan` is not null group by `tb_barang`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `totalstokbarang3`
--
DROP TABLE IF EXISTS `totalstokbarang3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `totalstokbarang3`  AS  (select `tb_barang`.`id` AS `id_barang`,`tb_barang`.`kode_barang` AS `kode_barang`,`tb_barang`.`nama_barang` AS `nama_barang`,`tb_barang`.`id_golongan` AS `id_golongan`,`tb_barang`.`hpp` AS `hpp`,`tb_barang`.`harga_beli_terakhir_barang` AS `harga_beli_terakhir_barang`,`tb_barang`.`harga_jual_barang` AS `harga_jual_barang`,`tb_stok`.`id_satuan` AS `id_satuan`,`tb_stok`.`satuan` AS `satuan`,`tb_stok`.`id` AS `id_stok`,`tb_golongan`.`golongan` AS `golongan`,`tb_lokasi`.`lokasi` AS `lokasi`,round(sum(`tb_stok`.`jml_barang`),2) AS `total_stok` from (((`tb_stok` join `tb_barang` on(`tb_stok`.`id_barang` = `tb_barang`.`id`)) join `tb_golongan` on(`tb_barang`.`id_golongan` = `tb_golongan`.`id`)) join `tb_lokasi` on(`tb_stok`.`id_lokasi` = `tb_lokasi`.`id`)) where `tb_stok`.`satuan` is not null and `tb_stok`.`id_satuan` is not null group by `tb_barang`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `totalstokbarang4`
--
DROP TABLE IF EXISTS `totalstokbarang4`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `totalstokbarang4`  AS  (select `tb_barang`.`id` AS `id_barang`,`tb_barang`.`kode_barang` AS `kode_barang`,`tb_barang`.`nama_barang` AS `nama_barang`,`tb_barang`.`id_golongan` AS `id_golongan`,`tb_barang`.`hpp` AS `hpp`,`tb_barang`.`harga_beli_terakhir_barang` AS `harga_beli_terakhir_barang`,`tb_barang`.`harga_jual_barang` AS `harga_jual_barang`,`tb_stok`.`id_satuan` AS `id_satuan`,`tb_stok`.`satuan` AS `satuan`,`tb_stok`.`id` AS `id_stok`,`tb_golongan`.`golongan` AS `golongan`,round(sum(`tb_stok`.`jml_barang`),2) AS `total_stok` from ((`tb_stok` join `tb_barang` on(`tb_stok`.`id_barang` = `tb_barang`.`id`)) join `tb_golongan` on(`tb_barang`.`id_golongan` = `tb_golongan`.`id`)) where `tb_stok`.`satuan` is not null and `tb_stok`.`id_satuan` is not null and `tb_barang`.`status_barang` = 1 group by `tb_barang`.`id`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id_billing`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id_brand`);

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id_car`);

--
-- Indexes for table `car_owner`
--
ALTER TABLE `car_owner`
  ADD PRIMARY KEY (`id_car_owner`);

--
-- Indexes for table `claim`
--
ALTER TABLE `claim`
  ADD PRIMARY KEY (`id_claim`);

--
-- Indexes for table `claim_file`
--
ALTER TABLE `claim_file`
  ADD PRIMARY KEY (`id_claim_file`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `estimate`
--
ALTER TABLE `estimate`
  ADD PRIMARY KEY (`id_estimate`);

--
-- Indexes for table `estimate_parts`
--
ALTER TABLE `estimate_parts`
  ADD PRIMARY KEY (`id_estimate_parts`);

--
-- Indexes for table `estimate_services`
--
ALTER TABLE `estimate_services`
  ADD PRIMARY KEY (`id_estimate_services`);

--
-- Indexes for table `estimate_sublet`
--
ALTER TABLE `estimate_sublet`
  ADD PRIMARY KEY (`id_estimate_sublet`);

--
-- Indexes for table `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`id_insurance`);

--
-- Indexes for table `job_progress`
--
ALTER TABLE `job_progress`
  ADD PRIMARY KEY (`id_job_progress`);

--
-- Indexes for table `material_plan`
--
ALTER TABLE `material_plan`
  ADD PRIMARY KEY (`id_material_plan`);

--
-- Indexes for table `mechanic`
--
ALTER TABLE `mechanic`
  ADD PRIMARY KEY (`id_mechanic`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `panel`
--
ALTER TABLE `panel`
  ADD PRIMARY KEY (`id_panel`);

--
-- Indexes for table `parts_order`
--
ALTER TABLE `parts_order`
  ADD PRIMARY KEY (`id_parts_order`);

--
-- Indexes for table `parts_progress`
--
ALTER TABLE `parts_progress`
  ADD PRIMARY KEY (`id_parts_progress`);

--
-- Indexes for table `qc_process`
--
ALTER TABLE `qc_process`
  ADD PRIMARY KEY (`id_qc_process`);

--
-- Indexes for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_karyawan` (`id_karyawan`) USING BTREE,
  ADD KEY `id_user_role` (`id_user_role`) USING BTREE;

--
-- Indexes for table `tb_user_role`
--
ALTER TABLE `tb_user_role`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `work_plan`
--
ALTER TABLE `work_plan`
  ADD PRIMARY KEY (`id_work_plan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id_billing` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id_brand` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `id_car` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_owner`
--
ALTER TABLE `car_owner`
  MODIFY `id_car_owner` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `claim`
--
ALTER TABLE `claim`
  MODIFY `id_claim` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `claim_file`
--
ALTER TABLE `claim_file`
  MODIFY `id_claim_file` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `estimate`
--
ALTER TABLE `estimate`
  MODIFY `id_estimate` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estimate_parts`
--
ALTER TABLE `estimate_parts`
  MODIFY `id_estimate_parts` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estimate_services`
--
ALTER TABLE `estimate_services`
  MODIFY `id_estimate_services` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estimate_sublet`
--
ALTER TABLE `estimate_sublet`
  MODIFY `id_estimate_sublet` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `insurance`
--
ALTER TABLE `insurance`
  MODIFY `id_insurance` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_progress`
--
ALTER TABLE `job_progress`
  MODIFY `id_job_progress` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `material_plan`
--
ALTER TABLE `material_plan`
  MODIFY `id_material_plan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mechanic`
--
ALTER TABLE `mechanic`
  MODIFY `id_mechanic` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `panel`
--
ALTER TABLE `panel`
  MODIFY `id_panel` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `parts_order`
--
ALTER TABLE `parts_order`
  MODIFY `id_parts_order` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `parts_progress`
--
ALTER TABLE `parts_progress`
  MODIFY `id_parts_progress` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qc_process`
--
ALTER TABLE `qc_process`
  MODIFY `id_qc_process` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tb_user_role`
--
ALTER TABLE `tb_user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `work_plan`
--
ALTER TABLE `work_plan`
  MODIFY `id_work_plan` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`id_karyawan`) REFERENCES `tb_karyawan` (`id`),
  ADD CONSTRAINT `tb_user_ibfk_2` FOREIGN KEY (`id_user_role`) REFERENCES `tb_user_role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
