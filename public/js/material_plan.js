$(document).ready(function () {
    $('.btn-non-paint-add').click(function () {
        var non_paint_row = $('.table-non-paint-row tbody').html();
        $('.table-non-paint tbody').append(non_paint_row);

        input_numeral();
        non_paint_hapus();
        non_paint_row_number();
        non_paint_mapl_price_total();
    });


    $('.btn-paint-add').click(function () {
        var paint_row = $('.table-paint-row tbody').html();
        $('.table-paint tbody').append(paint_row);

        input_numeral();
        paint_hapus();
        paint_row_number();
        paint_mapl_price_total();
    });

    /**
     * Saat edit Material Plan
     */

    non_paint_hapus();
    non_paint_row_number();
    non_paint_mapl_price_total();

    paint_hapus();
    paint_row_number();
    paint_mapl_price_total();
});


/**
 * Function untuk Material Non Paint
 */
function non_paint_hapus() {
    $('.btn-non-paint-hapus').click(function () {
        $(this).parents('tr').remove();

        non_paint_total();
        price_material_total();
    });
}

function non_paint_row_number() {
    $('.table-non-paint .non-paint-no').each(function (key) {
        var number = parseInt(key) + 1;
        $(this).text(number + '.');
    });
}

function non_paint_mapl_price_total() {
    $('.table-non-paint [name="mapl_plan[non_paint][]"]').on('keyup change', function () {

        non_paint_total();
        price_material_total();
    });

    $('.table-non-paint [name="mapl_actual[non_paint][]"]').on('keyup change', function () {
        var mapl_actual = $(this).val();
        mapl_actual = strtonumber(mapl_actual);

        var mapl_item_price = $(this).parents('td').siblings('td.mapl-item-price').children('input').val();
        mapl_item_price = strtonumber(mapl_item_price);

        var mapl_price_total = parseFloat(mapl_actual) * parseFloat(mapl_item_price);

        $(this).parents('td').siblings('td.td-non-paint-data').children('[name="mapl_price_total[non_paint][]"]').val(mapl_price_total);
        $(this).parents('td').siblings('td.mapl-price-total').html(format_number(mapl_price_total));

        non_paint_total();
        price_material_total();
    });

    $('.table-non-paint [name="mapl_item_price[non_paint][]"]').on('keyup change', function () {
        var mapl_actual = $(this).parents('td').siblings('td.mapl-actual').children('input').val();
        mapl_actual = strtonumber(mapl_actual);

        var mapl_item_price = $(this).val();
        mapl_item_price = strtonumber(mapl_item_price);

        var mapl_price_total = parseFloat(mapl_actual) * parseFloat(mapl_item_price);

        $(this).parents('td').siblings('td.td-non-paint-data').children('[name="mapl_price_total[non_paint][]"]').val(mapl_price_total);
        $(this).parents('td').siblings('td.mapl-price-total').html(format_number(mapl_price_total));

        non_paint_total();
        price_material_total();
    });
}

function non_paint_total() {
    var mapl_plan_total = 0,
        mapl_actual_total = 0,
        mapl_item_price_total = 0,
        mapl_price_total_all = 0;

    $('.table-non-paint [name="mapl_plan[non_paint][]"]').each(function() {
        var mapl_plan = $(this).val();
        mapl_plan = strtonumber(mapl_plan);

        mapl_plan_total = parseFloat(mapl_plan_total) + parseFloat(mapl_plan);
    });



    $('.table-non-paint [name="mapl_actual[non_paint][]"]').each(function() {
        var mapl_actual = $(this).val();
        mapl_actual = strtonumber(mapl_actual);

        mapl_actual_total = parseFloat(mapl_actual_total) + parseFloat(mapl_actual);
    });

    $('.table-non-paint [name="mapl_item_price[non_paint][]"]').each(function() {
        var mapl_item_price = $(this).val();
        mapl_item_price = strtonumber(mapl_item_price);

        mapl_item_price_total = parseFloat(mapl_item_price_total) + parseFloat(mapl_item_price);
    });

    $('.table-non-paint [name="mapl_price_total[non_paint][]"]').each(function() {
        var mapl_price_total = $(this).val();

        mapl_price_total_all = parseFloat(mapl_price_total_all) + parseFloat(mapl_price_total);
    });

    $('.table-non-paint .mapl-plan-total').html(format_number(mapl_plan_total));
    $('.table-non-paint .mapl-actual-total').html(format_number(mapl_actual_total));
    $('.table-non-paint .mapl-item-price-total').html(format_number(mapl_item_price_total));
    $('.table-non-paint .mapl-price-total-all').html(format_number(mapl_price_total_all));

}



/**
 * Function untuk Material Paint
 */
function paint_hapus() {
    $('.btn-paint-hapus').click(function () {
        $(this).parents('tr').remove();

        paint_total();
        price_material_total();
    });
}

function paint_row_number() {
    $('.table-paint .paint-no').each(function (key) {
        var number = parseInt(key) + 1;
        $(this).text(number + '.');
    });
}

function paint_mapl_price_total() {
    $('.table-paint [name="mapl_plan[paint][]"]').on('keyup change', function () {

        paint_total();
        price_material_total();
    });

    $('.table-paint [name="mapl_actual[paint][]"]').on('keyup change', function () {
        var mapl_actual = $(this).val();
        mapl_actual = strtonumber(mapl_actual);

        var mapl_item_price = $(this).parents('td').siblings('td.mapl-item-price').children('input').val();
        mapl_item_price = strtonumber(mapl_item_price);

        var mapl_price_total = parseFloat(mapl_actual) * parseFloat(mapl_item_price);

        $(this).parents('td').siblings('td.td-paint-data').children('[name="mapl_price_total[paint][]"]').val(mapl_price_total);
        $(this).parents('td').siblings('td.mapl-price-total').html(format_number(mapl_price_total));

        paint_total();
        price_material_total();
    });

    $('.table-paint [name="mapl_item_price[paint][]"]').on('keyup change', function () {
        var mapl_actual = $(this).parents('td').siblings('td.mapl-actual').children('input').val();
        mapl_actual = strtonumber(mapl_actual);

        var mapl_item_price = $(this).val();
        mapl_item_price = strtonumber(mapl_item_price);

        var mapl_price_total = parseFloat(mapl_actual) * parseFloat(mapl_item_price);

        $(this).parents('td').siblings('td.td-paint-data').children('[name="mapl_price_total[paint][]"]').val(mapl_price_total);
        $(this).parents('td').siblings('td.mapl-price-total').html(format_number(mapl_price_total));

        paint_total();
        price_material_total();
    });
}

function paint_total() {
    var mapl_plan_total = 0,
        mapl_actual_total = 0,
        mapl_item_price_total = 0,
        mapl_price_total_all = 0;

    $('.table-paint [name="mapl_plan[paint][]"]').each(function() {
        var mapl_plan = $(this).val();
        mapl_plan = strtonumber(mapl_plan);

        mapl_plan_total = parseFloat(mapl_plan_total) + parseFloat(mapl_plan);
    });



    $('.table-paint [name="mapl_actual[paint][]"]').each(function() {
        var mapl_actual = $(this).val();
        mapl_actual = strtonumber(mapl_actual);

        mapl_actual_total = parseFloat(mapl_actual_total) + parseFloat(mapl_actual);
    });

    $('.table-paint [name="mapl_item_price[paint][]"]').each(function() {
        var mapl_item_price = $(this).val();
        mapl_item_price = strtonumber(mapl_item_price);

        mapl_item_price_total = parseFloat(mapl_item_price_total) + parseFloat(mapl_item_price);
    });

    $('.table-paint [name="mapl_price_total[paint][]"]').each(function() {
        var mapl_price_total = $(this).val();

        mapl_price_total_all = parseFloat(mapl_price_total_all) + parseFloat(mapl_price_total);
    });

    $('.table-paint .mapl-plan-total').html(format_number(mapl_plan_total));
    $('.table-paint .mapl-actual-total').html(format_number(mapl_actual_total));
    $('.table-paint .mapl-item-price-total').html(format_number(mapl_item_price_total));
    $('.table-paint .mapl-price-total-all').html(format_number(mapl_price_total_all));

}

function price_material_total() {
    var non_paint_price_total = $('.table-non-paint .mapl-price-total-all').html();
    non_paint_price_total = strtonumber(non_paint_price_total);

    var paint_price_total = $('.table-paint .mapl-price-total-all').html();
    paint_price_total = strtonumber(paint_price_total);

    var price_material_total = parseFloat(non_paint_price_total) + parseFloat(paint_price_total);
    $('.price-material-total').html(format_number(price_material_total));
}