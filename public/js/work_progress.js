$(document).ready(function() {
    image_preview_parts_lama();
    image_preview_parts_baru();
    image_preview_epoxy();
    image_preview_hasil();
});



function image_preview_parts_lama() {
    function readURL(input, self) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.img-preview-parts-lama').attr('src', e.target.result);
                $('.img-link-parts-lama').attr('href', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".btn-image-browse-parts-lama").change(function () {
        readURL(this, $(this));
    });
}

function image_preview_parts_baru() {
    function readURL(input, self) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.img-preview-parts-baru').attr('src', e.target.result);
                $('.img-link-parts-baru').attr('href', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".btn-image-browse-parts-baru").change(function () {
        readURL(this, $(this));
    });
}

function image_preview_epoxy() {
    function readURL(input, self) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.img-preview-epoxy').attr('src', e.target.result);
                $('.img-link-epoxy').attr('href', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".btn-image-browse-epoxy").change(function () {
        readURL(this, $(this));
    });
}

function image_preview_hasil() {
    function readURL(input, self) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.img-preview-hasil').attr('src', e.target.result);
                $('.img-link-hasil').attr('href', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".btn-image-browse-hasil").change(function () {
        readURL(this, $(this));
    });
}