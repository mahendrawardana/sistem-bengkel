$(document).ready(function () {
    $('.btn-jasa-add').click(function () {
        var jasa_row = $('.table-jasa-row tbody').html();
        $('.table-jasa tbody').append(jasa_row);

        input_numeral();
        btn_jasa_hapus();
        jasa_row_number();
        eser_status_number();
        eser_price_end();
    });

    $('.btn-parts-add').click(function () {
        var parts_row = $('.table-parts-row tbody').html();
        $('.table-parts tbody').append(parts_row);

        input_numeral();
        btn_parts_hapus();
        parts_row_number();
        espa_price_end();
    });

    $('.btn-sublet-add').click(function () {
        var sublet_row = $('.table-sublet-row tbody').html();
        $('.table-sublet tbody').append(sublet_row);

        input_numeral();
        btn_sublet_hapus();
        sublet_row_number();
        esub_price_end();
    });

    /**
     * Saat Edit Estimate
     */

    btn_jasa_hapus();
    eser_price_end();
    btn_parts_hapus();
    espa_price_end();
    sublet_row_number();
    esub_price_end();
});

/**
 * Function untuk Services
 */
function btn_jasa_hapus() {
    $('.btn-jasa-hapus').click(function () {
        $(this).parents('tr').remove();
        jasa_row_number();
        esti_price_total_services();
        total_estimasi_biaya();
    });
}

function jasa_row_number() {
    $('.table-jasa .jasa-no').each(function (key) {
        var number = parseInt(key) + 1;
        $(this).text(number + '.');
    });
}

function eser_status_number() {
    $('.jasa-eser-status').each(function (key, val) {
        $(this).children('div').children('label').children('input').attr('name', 'eser_status[' + key + ']');
    });
}

function eser_price_end() {
    $('[name="eser_price_item[]"]').on('keyup change', function () {
        var eser_price_item = $(this).val();
        eser_price_item = strtonumber(eser_price_item);

        var eser_discount = $(this).parents('td').siblings('td.eser-discount').children('input').val();
        eser_discount = strtonumber(eser_discount);

        var eser_price_end = parseFloat(eser_price_item) - parseFloat(eser_discount);

        $(this).parents('td').siblings('td.td-jasa-data').children('[name="eser_price_end[]"]').val(eser_price_end);
        $(this).parents('td').siblings('td.eser-price-end').html(format_number(eser_price_end));


        esti_price_total_services();
        total_estimasi_biaya();
    });

    $('[name="eser_discount[]"]').on('keyup change', function () {
        var eser_price_item = $(this).parents('td').siblings('td.eser-price-item').children('input').val();
        eser_price_item = strtonumber(eser_price_item);

        var eser_discount = $(this).val();
        eser_discount = strtonumber(eser_discount);

        var eser_price_end = parseFloat(eser_price_item) - parseFloat(eser_discount);

        $(this).parents('td').siblings('td.td-jasa-data').children('[name="eser_price_end[]"]').val(eser_price_end);
        $(this).parents('td').siblings('td.eser-price-end').html(format_number(eser_price_end));


        esti_price_total_services();
        total_estimasi_biaya();
    });
}

function esti_price_total_services() {
    var eser_price_item_total = 0;
    var eser_discount_total = 0;
    var esti_price_total_services = 0;

    $('[name="eser_price_item[]"]').each(function () {
        var eser_price_item = strtonumber($(this).val());
        eser_price_item_total = parseFloat(eser_price_item) + parseFloat(eser_price_item_total);
    });
    $('[name="eser_discount[]"]').each(function () {
        var eser_discount = strtonumber($(this).val());
        eser_discount_total = parseFloat(eser_discount) + parseFloat(eser_discount_total);
    });

    esti_price_total_services = parseFloat(eser_price_item_total) - parseFloat(eser_discount_total);

    $('.eser-price-item-total').html(format_number(eser_price_item_total));
    $('.eser-discount-total').html(format_number(eser_discount_total));
    $('.esti-price-total-services').html(format_number(esti_price_total_services));

    $('[name="esti_price_total_services"]').val(esti_price_total_services);
}

/**
 * Function untuk Parts
 */
function btn_parts_hapus() {
    $('.btn-parts-hapus').click(function () {
        $(this).parents('tr').remove();
        parts_row_number();
        espa_price_total_parts();
        total_estimasi_biaya();
    });
}

function parts_row_number() {
    $('.table-parts .parts-no').each(function (key) {
        var number = parseInt(key) + 1;
        $(this).text(number + '.');
    });
}

function espa_price_end() {
    $('[name="espa_qty[]"]').on('keyup change', function () {
        var espa_qty = $(this).val();
        espa_qty = strtonumber(espa_qty);

        var espa_price_item = $(this).parents('td').siblings('td.espa-price-item').children('input').val();
        espa_price_item = strtonumber(espa_price_item);

        var espa_discount = $(this).parents('td').siblings('td.espa-discount').children('input').val();
        espa_discount = strtonumber(espa_discount);

        var espa_price_end = (parseFloat(espa_qty) * parseFloat(espa_price_item)) - parseFloat(espa_discount);

        $(this).parents('td').siblings('td.td-parts-data').children('[name="espa_price_end[]"]').val(espa_price_end);
        $(this).parents('td').siblings('td.espa-price-end').html(format_number(espa_price_end));

        espa_price_total_parts();
        total_estimasi_biaya();
    });


    $('[name="espa_price_item[]"]').on('keyup change', function () {
        var espa_qty = $(this).parents('td').siblings('td.espa-qty').children('input').val();
        espa_qty = strtonumber(espa_qty);

        var espa_price_item = $(this).val();
        espa_price_item = strtonumber(espa_price_item);

        var espa_discount = $(this).parents('td').siblings('td.espa-discount').children('input').val();
        espa_discount = strtonumber(espa_discount);

        var espa_price_end = (parseFloat(espa_qty) * parseFloat(espa_price_item)) - parseFloat(espa_discount);

        $(this).parents('td').siblings('td.td-parts-data').children('[name="espa_price_end[]"]').val(espa_price_end);
        $(this).parents('td').siblings('td.espa-price-end').html(format_number(espa_price_end));

        espa_price_total_parts();
        total_estimasi_biaya();
    });


    $('[name="espa_discount[]"]').on('keyup change', function () {
        var espa_qty = $(this).parents('td').siblings('td.espa-qty').children('input').val();
        espa_qty = strtonumber(espa_qty);

        var espa_price_item = $(this).parents('td').siblings('td.espa-price-item').children('input').val();
        espa_price_item = strtonumber(espa_price_item);

        var espa_discount = $(this).val();
        espa_discount = strtonumber(espa_discount);

        var espa_price_end = (parseFloat(espa_qty) * parseFloat(espa_price_item)) - parseFloat(espa_discount);

        $(this).parents('td').siblings('td.td-parts-data').children('[name="espa_price_end[]"]').val(espa_price_end);
        $(this).parents('td').siblings('td.espa-price-end').html(format_number(espa_price_end));

        espa_price_total_parts();
        total_estimasi_biaya();
    });
}

function espa_price_total_parts() {
    var espa_qty_total = 0;
    var espa_price_item_total = 0;
    var espa_discount_total = 0;
    var esti_price_total_parts = 0;

    $('[name="espa_qty[]"]').each(function () {
        var espa_qty = strtonumber($(this).val());
        espa_qty_total = parseFloat(espa_qty) + parseFloat(espa_qty_total);
    });
    $('[name="espa_price_item[]"]').each(function () {
        var espa_price_item = strtonumber($(this).val());
        espa_price_item_total = parseFloat(espa_price_item) + parseFloat(espa_price_item_total);
    });
    $('[name="espa_discount[]"]').each(function () {
        var espa_discount = strtonumber($(this).val());
        espa_discount_total = parseFloat(espa_discount) + parseFloat(espa_discount_total);
    });

    $('[name="espa_price_end[]"]').each(function () {
        var espa_price_end = $(this).val();
        esti_price_total_parts = parseFloat(esti_price_total_parts) + parseFloat(espa_price_end);
    });

    $('.espa-qty-total').html(format_number(espa_qty_total));
    $('.espa-price-item-total').html(format_number(espa_price_item_total));
    $('.espa-discount-total').html(format_number(espa_discount_total));
    $('.esti-price-total-parts').html(format_number(esti_price_total_parts));

    $('[name="esti_price_total_parts"]').val(esti_price_total_parts);
}


/**
 * Function untuk Sublet
 */
function btn_sublet_hapus() {
    $('.btn-sublet-hapus').click(function () {
        $(this).parents('tr').remove();
        sublet_row_number();
        esub_price_total_sublet();
        total_estimasi_biaya();
    });
}

function sublet_row_number() {
    $('.table-sublet .sublet-no').each(function (key) {
        var number = parseInt(key) + 1;
        $(this).text(number + '.');
    });
}

function esub_price_end() {
    $('[name="esub_qty[]"]').on('keyup change', function () {
        var esub_qty = $(this).val();
        esub_qty = strtonumber(esub_qty);

        var esub_price_item = $(this).parents('td').siblings('td.esub-price-item').children('input').val();
        esub_price_item = strtonumber(esub_price_item);

        var esub_discount = $(this).parents('td').siblings('td.esub-discount').children('input').val();
        esub_discount = strtonumber(esub_discount);

        var esub_price_end = (parseFloat(esub_qty) * parseFloat(esub_price_item)) - parseFloat(esub_discount);

        $(this).parents('td').siblings('td.td-sublet-data').children('[name="esub_price_end[]"]').val(esub_price_end);
        $(this).parents('td').siblings('td.esub-price-end').html(format_number(esub_price_end));

        esub_price_total_sublet();
        total_estimasi_biaya();
    });


    $('[name="esub_price_item[]"]').on('keyup change', function () {
        var esub_qty = $(this).parents('td').siblings('td.esub-qty').children('input').val();
        esub_qty = strtonumber(esub_qty);

        var esub_price_item = $(this).val();
        esub_price_item = strtonumber(esub_price_item);

        var esub_discount = $(this).parents('td').siblings('td.esub-discount').children('input').val();
        esub_discount = strtonumber(esub_discount);

        var esub_price_end = (parseFloat(esub_qty) * parseFloat(esub_price_item)) - parseFloat(esub_discount);

        $(this).parents('td').siblings('td.td-sublet-data').children('[name="esub_price_end[]"]').val(esub_price_end);
        $(this).parents('td').siblings('td.esub-price-end').html(format_number(esub_price_end));

        esub_price_total_sublet();
        total_estimasi_biaya();
    });


    $('[name="esub_discount[]"]').on('keyup change', function () {
        var esub_qty = $(this).parents('td').siblings('td.esub-qty').children('input').val();
        esub_qty = strtonumber(esub_qty);

        var esub_price_item = $(this).parents('td').siblings('td.esub-price-item').children('input').val();
        esub_price_item = strtonumber(esub_price_item);

        var esub_discount = $(this).val();
        esub_discount = strtonumber(esub_discount);

        var esub_price_end = (parseFloat(esub_qty) * parseFloat(esub_price_item)) - parseFloat(esub_discount);

        $(this).parents('td').siblings('td.td-sublet-data').children('[name="esub_price_end[]"]').val(esub_price_end);
        $(this).parents('td').siblings('td.esub-price-end').html(format_number(esub_price_end));

        esub_price_total_sublet();
        total_estimasi_biaya();
    });
}

function esub_price_total_sublet() {
    var esub_qty_total = 0;
    var esub_price_item_total = 0;
    var esub_discount_total = 0;
    var esti_price_total_sublet = 0;

    $('[name="esub_qty[]"]').each(function () {
        var esub_qty = strtonumber($(this).val());
        esub_qty_total = parseFloat(esub_qty) + parseFloat(esub_qty_total);
    });
    $('[name="esub_price_item[]"]').each(function () {
        var esub_price_item = strtonumber($(this).val());
        esub_price_item_total = parseFloat(esub_price_item) + parseFloat(esub_price_item_total);
    });
    $('[name="esub_discount[]"]').each(function () {
        var esub_discount = strtonumber($(this).val());
        esub_discount_total = parseFloat(esub_discount) + parseFloat(esub_discount_total);
    });

    $('[name="esub_price_end[]"]').each(function () {
        var esub_price_end = $(this).val();
        esti_price_total_sublet = parseFloat(esti_price_total_sublet) + parseFloat(esub_price_end);
    });

    $('.esub-qty-total').html(format_number(esub_qty_total));
    $('.esub-price-item-total').html(format_number(esub_price_item_total));
    $('.esub-discount-total').html(format_number(esub_discount_total));
    $('.esti-price-total-sublet').html(format_number(esti_price_total_sublet));

    $('[name="esti_price_total_sublet"]').val(esti_price_total_sublet);
}


/**
 * Total Estimasi Biaya
 */

function total_estimasi_biaya() {
    var esti_price_total_services = $('[name="esti_price_total_services"]').val();
    var esti_price_total_parts = $('[name="esti_price_total_parts"]').val();
    var esti_price_total_sublet = $('[name="esti_price_total_sublet"]').val();
    var esti_price_total = parseFloat(esti_price_total_services) + parseFloat(esti_price_total_parts) + parseFloat(esti_price_total_sublet);

    $('[name="esti_price_total"]').val(esti_price_total);
    $('.esti-price-total').html(format_number(esti_price_total));

}