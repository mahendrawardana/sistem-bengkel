$(document).ready(function () {
    paor_price_buy();
});

function paor_price_buy() {
    $('.paor-price-buy').on('keyup change', function () {
        var paor_price_buy = $(this).val();
        paor_price_buy = strtonumber(paor_price_buy);

        var harga_jual = $(this).parents('td').siblings('td.espa-price-item').html();
        harga_jual = strtonumber(harga_jual);

        var margin_price = parseFloat(harga_jual) - parseFloat(paor_price_buy);

        var margin_percent = (parseFloat(margin_price) / parseFloat(harga_jual)) * 100;
        var margin_percent_pembulatan = margin_percent.toFixed(2);

        $(this).parents('td').siblings('td.paor-margin').html(margin_percent_pembulatan + '%');
        $(this).parents('td').siblings('td.row-data').children('.paor-margin-input').val(margin_percent_pembulatan);

        paor_price_buy_total();
        paor_margin_total();
    });
}

function paor_price_buy_total() {
    var paor_price_buy_total = 0;

    $('.paor-price-buy').each(function () {
        var paor_price_buy = $(this).val();
        paor_price_buy = strtonumber(paor_price_buy);

        paor_price_buy_total = parseFloat(paor_price_buy_total) + parseFloat(paor_price_buy);
    });

    $('.paor-price-buy-total').html(format_number(paor_price_buy_total));
}


function paor_margin_total() {
    var espa_price_item_total = $('.espa-price-item-total').html();
    espa_price_item_total = strtonumber(espa_price_item_total);

    var paor_price_buy_total = $('.paor-price-buy-total').html();
    paor_price_buy_total = strtonumber(paor_price_buy_total);

    var paor_margin_total = ( ( parseFloat(espa_price_item_total) - parseFloat(paor_price_buy_total)) / parseFloat(espa_price_item_total) ) * 100;
    $('.paor-margin-total').html(paor_margin_total.toFixed(2) + '%');
}