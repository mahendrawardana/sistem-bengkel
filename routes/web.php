<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);


Route::group(['namespace' => 'Bengkel', 'middleware' => 'authLogin'], function () {

    /**
     * Service Advisor
     */

    Route::get('/dashboard-sa', "DashboardSa@index")->name('dashboardSaPage');

    /**
     * Customer Data
     */

    Route::get('/data-pelanggan', "CustomerData@index")->name('customerDataPage');
    Route::post('/data-pelanggan/search', "CustomerData@search")->name('customerSearch');
    Route::post('/data-pelanggan/datatable', "CustomerData@datatable")->name('customerDataTable');

    Route::get('/data-pelanggan/create', "CustomerData@create")->name('customerCreatePage');
    Route::post('/data-pelanggan/insert', "CustomerData@insert")->name('customerInsert');

    Route::get('/data-pelanggan/edit/{id_car}', "CustomerData@edit")->name('customerEditPage');
    Route::post('/data-pelanggan/update/{id_car}', "CustomerData@update")->name('customerUpdate');

    Route::get('/data-pelanggan/exist/{id_car}', "CustomerData@exist")->name('customerExistPage');
    Route::get('/data-pelanggan/detail/{id_car}', "CustomerData@detail")->name('customerDetailPage');

    Route::delete('/data-pelanggan/delete/{id_car}', "CustomerData@delete")->name('customerDelete');

    /**
     * Klaim Document
     */

    Route::get('/dokumen-klaim', "ClaimDocument@index")->name('claimDocumentPage');
    Route::post('/dokumen-klaim/search', "ClaimDocument@search")->name('claimDocumentSearch');
    Route::post('/dokumen-klaim/datatable', "ClaimDocument@datatable")->name('claimDocumentDataTable');

    Route::get('/dokumen-klaim/craete/{id_car}', "ClaimDocument@create")->name('claimDocumentCreate');
    Route::post('/dokumen-klaim/insert', "ClaimDocument@insert")->name('claimDocumentInsert');

    Route::get('/dokumen-klaim/edit/{id_claim}', "ClaimDocument@edit")->name('claimDocumentEditPage');
    Route::post('/dokumen-klaim/update/{id_claim}', "ClaimDocument@update")->name('claimDocumentUpdate');
    Route::get('/dokumen-klaim/detail/{id_claim}', "ClaimDocument@detail")->name('claimDocumentDetailPage');
    Route::delete('/dokumen-klaim/delete/{id_claim}', "ClaimDocument@delete")->name('claimDocumentDelete');

    /**
     * Form Estimasi
     */
    Route::get('/form-estimasi', "EstimationForm@index")->name('estimationFormPage');
    Route::post('/form-estimasi/search', "EstimationForm@search")->name('estimationFormSearch');
    Route::post('/form-estimasi/datatable', "EstimationForm@datatable")->name('estimationFormDataTable');
    Route::get('/form-estimasi/create/{id_car}/{id_claim?}', "EstimationForm@create")->name('estimationFormCreate');
    Route::post('/form-estimasi/insert/', "EstimationForm@insert")->name('estimationFormInsert');

    Route::get('/form-estimasi/edit/{id_estimate}', "EstimationForm@edit")->name('estimationFormEdit');
    Route::post('/form-estimasi/update/{id_estimate}', "EstimationForm@update")->name('estimationFormUpdate');

    Route::get('/form-estimasi/detail/{id_estimate}', "EstimationForm@detail")->name('estimationFormDetail');

    Route::get('/form-estimasi/cetak/{id_estimate}', "EstimationForm@print")->name('estimationFormPrint');

    Route::delete('/form-estimasi/delete/{id_estimate}', "EstimationForm@delete")->name('estimationFormDelete');

    /**
     * Form Order
     */
    Route::get('/form-order', "OrderForm@index")->name('orderFormPage');
    Route::post('/form-order/search', "OrderForm@search")->name('orderFormSearch');
    Route::post('/form-order/datatable', "OrderForm@datatable")->name('orderFormDataTable');

    Route::get('/form-order/detail/{id_order}', "OrderForm@detail")->name('orderFormDetail');
    Route::get('/form-order/cetak/estimate/{id_estimate}', "OrderForm@print_from_estimate")->name('orderFormEstimatePrint');
    Route::get('/form-order/cetak/order/{id_order}', "OrderForm@print_from_order")->name('orderFormPrint');
    Route::delete('/form-order/delete/{id_order}', "OrderForm@delete")->name('orderFormDelete');

    /**
     * Progress Claim
     */
    Route::get('/progress-klaim', "ClaimProgress@index")->name('claimProgressPage');
    Route::post('/progress-klaim/search', "ClaimProgress@search")->name('claimProgressSearch');
    Route::post('/progress-klaim/datatable', "ClaimProgress@datatable")->name('claimProgressDataTable');
    Route::post('/progress-klaim/update/progress', 'ClaimProgress@claim_progress_update')->name('claimProgressUpdate');



    /**
     * Foreman
     */
    Route::get('/dashboard-foreman', "DashboardForeman@index")->name('dashboardForemanPage');

    /**
     * Workplan
     */
    Route::get('/work-plan', "WorkPlan@index")->name('workPlanPage');
    Route::post('/work-plan/datatable', "WorkPlan@datatable")->name('workPlanDataTable');
    Route::post('/work-plan/search', "WorkPlan@search")->name('workPlanSearch');
    Route::post('/work-plan/day/format', 'WorkPlan@format_day')->name('workPlanFormatDay');

    Route::get('/work-plan/edit/{id_order}', "WorkPlan@edit")->name('workPlanEdit');
    Route::post('/work-plan/update/{id_order}', "WorkPlan@update")->name('workPlanUpdate');

    Route::get('/work-plan/cetak/{id_order}', "WorkPlan@print")->name('workPlanPrint');


    /**
     * Material Plan
     */

    Route::get('/material-plan', "MaterialPlan@index")->name('materialPlanPage');
    Route::post('/material-plan/datatable', "MaterialPlan@datatable")->name('materialPlanDataTable');
    Route::post('/material-plan/search', "MaterialPlan@search")->name('materialPlanSearch');

    Route::get('/material-plan/edit/{id_order}', "MaterialPlan@edit")->name('materialPlanEdit');
    Route::post('/material-plan/update/{id_order}', "MaterialPlan@update")->name('materialPlanUpdate');

    Route::get('/material-plan/cetak/{id_order}', "MaterialPlan@print")->name('materialPlanPrint');


    /**
     * Parts Order
     */

    Route::get('/parts-order', "PartsOrder@index")->name('partsOrderPage');
    Route::post('/parts-order/datatable', "PartsOrder@datatable")->name('partsOrderDataTable');
    Route::post('/parts-order/search', "PartsOrder@search")->name('partsOrderSearch');

    Route::get('/parts-order/edit/{id_order}', "PartsOrder@edit")->name('partsOrder');
    Route::post('/parts-order/update/{id_order}', "PartsOrder@update")->name('partsOrderUpdate');

    Route::get('/parts-order/cetak/{id_order}', "PartsOrder@print")->name('partsOrderPrint');

    /**
     * Parts Progress
     */

    Route::get('/parts-progress', "PartsProgress@index")->name('partsProgressPage');
    Route::get('/parts-progress/edit', "PartsProgress@edit")->name('partsProgressEditPage');
    Route::get('/parts-progress/cetak', "PartsProgress@print")->name('partsProgressPrint');


    /**
     * QC Process
     */
    Route::get('/qc-process', "QcProcess@index")->name('qcProcessPage');
    Route::post('/qc-process/datatable', "QcProcess@datatable")->name('qcProcessDataTable');
    Route::post('/qc-process/search', "QcProcess@search")->name('qcProcessSearch');

    Route::get('/qc-process/edit/{id_order}', "QcProcess@edit")->name('qcProcessEdit');
    Route::post('/qc-process/update/{id_order}', "QcProcess@update")->name('qcProcessUpdate');

    Route::get('/qc-process/cetak/{id_order}', "QcProcess@print")->name('qcProcessPrint');

    /**
     * Wok Progress
     */
    Route::get('/progress-pekerjaan', "WorkProgress@index")->name('workProgressPage');
    Route::post('/progress-pekerjaan/datatable', "WorkProgress@datatable")->name('workProgressDataTable');
    Route::post('/progress-pekerjaan/search', "WorkProgress@search")->name('workProgressSearch');

    Route::get('/progress-pekerjaan/edit/{id_order}', "WorkProgress@edit")->name('workProgressEdit');
    Route::post('/progress-pekerjaan/update/{id_order}', "WorkProgress@update")->name('workProgressUpdate');

    Route::get('/progress-pekerjaan/cetak/{id_order}', "WorkProgress@print")->name('workProgressPrint');

    /**
     * Job Progress Board
     */
    Route::get('/job-progress-board', "JobProgress@index")->name('jobProgressPage');
    Route::post('/job-progress-board/cetak', "JobProgress@print")->name('jobProgressPrint');

    /**
     * Billing
     */
    Route::get('/dashboard-billing', "DashboardBilling@index")->name('dashboardBillingPage');

    /**
     *  Validasi Dokumen
     */
    Route::get('/validasi-dokumen', "DocumentValidation@index")->name('documentValidationPage');
    Route::post('/validasi-dokumen/datatable', "DocumentValidation@datatable")->name('documentValidationDataTable');
    Route::post('/validasi-dokumen/search', "DocumentValidation@search")->name('documentValidationSearch');

    Route::get('/validasi-dokumen/detail/{id_order}', "DocumentValidation@detail")->name('documentValidationDetail');
    Route::get('/validasi-dokumen/surat_puas/{id_order}', "DocumentValidation@surat_puas")->name('documentValidationSuratPuas');

    /**
     * Validasi Order
     */
    Route::get('/validasi-order', "OrderValidation@index")->name('orderValidationPage');
    Route::post('/validasi-order/datatable', "OrderValidation@datatable")->name('orderValidationDataTable');
    Route::post('/validasi-order/search', "OrderValidation@search")->name('orderValidationSearch');

    Route::get('/validasi-order/detail/{id_order}', "OrderValidation@detail")->name('orderValidationDetail');
    Route::post('/validasi-order/update/{id_order}', "OrderValidation@update")->name('orderValidationUpdate');

    Route::get('/validasi-order/cetak/{id_order}', "OrderValidation@print_invoice")->name('orderValidationPrint');


    /**
     * Reprint
     */
    Route::get('/cetak-ulang', "Reprint@index")->name('reprintPage');
    Route::post('/cetak-ulang/invoice', "Reprint@invoice")->name('reprintInvoice');
    Route::post('/cetak-ulang/parts', "Reprint@parts")->name('reprintParts');
    Route::post('/cetak-ulang/sublet', "Reprint@sublet")->name('reprintSublet');

    Route::get('/cetak-ulang/parts/print/{id_order}', "Reprint@parts_pdf")->name('reprintPartsPdf');
    Route::get('/cetak-ulang/sublet/print/{id_order}', "Reprint@sublet_pdf")->name('reprintSubletPdf');

    /**
     * Billing
     */
    Route::get('/billing', "Billing@index")->name('billingPage');
    Route::post('/billing/datatable', "Billing@datatable")->name('billingDataTable');
    Route::post('/billing/search', "Billing@search")->name('billingSearch');

    Route::post('/billing/update', "Billing@update")->name('billingUpdate');


    Route::get('/test-whatsapp', 'Test@whatsapp');

});


Route::namespace('General')->group(function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin', 'prefix' => 'admin'], function () {
        Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');
    });
});

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin', 'prefix' => 'admin'], function () {

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/{id}', "UserRole@edit")->name('userRoleEdit');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');


});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

Route::get('/logo', "logo@logo");

Route::get('#')->name('#');

